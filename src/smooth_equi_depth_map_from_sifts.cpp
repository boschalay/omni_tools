#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

//LadybugCalibration class
#include "mcs_calibration.hpp"
#include "depthmap_utils.cpp"


#include "opencv2/xfeatures2d.hpp" //SIFTS

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depth_map_txt individual_depth_map_cam1.txt pano_contribution_map.png camera1.jpg last_camera_index calibration_file dome <optional:max angle>\n",argv[0]);
}


// function for splitting image into multiple blocks. rowDivisor and colDivisor specify the number of blocks in rows and cols respectively
int subdivide(const cv::Mat &img, const int rowDivisor, const int colDivisor, std::vector<cv::Mat> &blocks)
    {
    /* Checking if the image was passed correctly */
    if(!img.data || img.empty())
        std::cerr << "Problem Loading Image" << std::endl;

    /* Cloning the image to another for visualization later, if you do not want to visualize the result just comment every line related to visualization */
    cv::Mat maskImg = img.clone();
    /* Checking if the clone image was cloned correctly */
    if(!maskImg.data || maskImg.empty())
        std::cerr << "Problem Loading Image" << std::endl;

    // check if divisors fit to image dimensions
    if(img.cols % colDivisor == 0 && img.rows % rowDivisor == 0)
    {
        for(int y = 0; y < img.cols; y += img.cols / colDivisor)
        {
            for(int x = 0; x < img.rows; x += img.rows / rowDivisor)
            {
                blocks.push_back(img(cv::Rect(y, x, (img.cols / colDivisor), (img.rows / rowDivisor))).clone());
                //rectangle(maskImg, Point(y, x), Point(y + (maskImg.cols / colDivisor) - 1, x + (maskImg.rows / rowDivisor) - 1), CV_RGB(255, 0, 0), 1); // visualization

                //imshow("Image", maskImg); // visualization
               // waitKey(0); // visualization
            }
        }
    }else if(img.cols % colDivisor != 0)
    {
        cerr << "Error: Please use another divisor for the column split." << endl;
        exit(1);
    }else if(img.rows % rowDivisor != 0)
    {
        cerr << "Error: Please use another divisor for the row split." << endl;
        exit(1);
    }
    return EXIT_SUCCESS;
}

int main(int argc, char *argv[])
{

    if(argc <8)
    {
        printhelp(argv);
        return(1);
    }


    string file_path=argv[1];
    string file_extension=file_path.substr(file_path.rfind(".")+1);

    int height_depth_map=1000;
    int width_depth_map=2000;
    cv::Mat panoramic=cv::Mat::zeros(height_depth_map,width_depth_map,CV_64F);

    int pano_width=width_depth_map;
    int pano_height=height_depth_map;

    //We resize it to make thing fasater. Anyway we are smoothing it...
    float ratio=(float)height_depth_map/(float)width_depth_map;
    float divisor=(float)width_depth_map/1000.0;
    width_depth_map=1000;
    height_depth_map=1000.0*ratio;
    cv::resize(panoramic, panoramic, cv::Size(width_depth_map, height_depth_map),0,0,cv::INTER_NEAREST );


    //READ RGB IMAGES
    int last_camera=atoi(argv[5]);

    bool dome_projection=atoi(argv[7]);
    double max_angle=3.1416;
    if(dome_projection){
      max_angle=atof(argv[8]);
    }

    //-- Load calibation matrices.
    MultiCamCalibration calibration; //Create calibration object.
    string calibration_file=string(argv[6]);
    calibration.loadCalibrationFile(calibration_file);

    string image0=string(argv[4]);
    int cameraindex = image0.rfind("camera");
    string file_part0 = image0.substr(0, cameraindex);
    string file_part1 = image0.substr(0, cameraindex+6);
    int first_camera= atoi(image0.substr(cameraindex+6,1).c_str());
    //Find now the frame. The numbering of the frame is 5 digits.
    int indexframe = image0.rfind("frame");
    string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
    string first_frame_str = image0.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    string file_part3 = image0.substr(indexframe+10);

    Mat image[last_camera+1];
    string image_path;
    stringstream ss;

    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        image_path.clear();
        ss << file_part1 << cam << file_part2 << setfill('0')<< setw(5) << first_frame<< file_part3;
        image_path=ss.str();
        image[cam] = imread(image_path, IMREAD_COLOR);
        if(image[cam].data==NULL){
            cout << "Error reading image: "<< image_path << endl;
            return 1;
        }
    }
    cout << "All images opened succesfully."<< endl;

    //READ INDIVIDUAL DEPTH MAPS
    int imageHeight=image[1].rows;
    int imageWidth=image[1].cols;

    string depthmap0=string(argv[2]);
    cameraindex = depthmap0.rfind("camera");
    string depthfile_part1 = depthmap0.substr(0, cameraindex+6);
    first_camera= atoi(depthmap0.substr(cameraindex+6,1).c_str());
    //Find now the frame. The numbering of the frame is 5 digits.
    string depthfile_part2 = depthmap0.substr(cameraindex+7);
    cv::Mat individualDepthMap[last_camera+1];
    std::vector<float> depthmapdata_individual;

    for(int cam = first_camera; cam <= last_camera; cam++) {
      cout << "cam" << cam << endl;
        ss.str("");
        string depthmap_path;
        depthmap_path.clear();
        ss << depthfile_part1 << cam << depthfile_part2;
        depthmap_path=ss.str();
        cout << "Depth map " << depthmap_path << endl;

        string file_extension=depthmap_path.substr(depthmap_path.rfind(".")+1);
        cout << "File extension " << file_extension << endl;


        individualDepthMap[cam]=cv::Mat::zeros(imageHeight,imageWidth,CV_64F);
        cout << imageHeight << ", " << imageWidth << endl;


        int width_depth_map, height_depth_map;
        cv::Mat depthMap;

        if(file_extension=="txt"){
            if(read_depth(depthmap_path,width_depth_map, height_depth_map, depthmapdata_individual)){
                depthMap=cv::Mat::zeros(width_depth_map/2,width_depth_map,CV_32F);
                memcpy(depthMap.data, depthmapdata_individual.data(), depthmapdata_individual.size()*sizeof(float));
            }
        }
        else if(file_extension=="bin"){
            if(read_depth_binary(depthmap_path,width_depth_map, height_depth_map,depthmapdata_individual)){
                depthMap=cv::Mat::zeros(height_depth_map,width_depth_map,CV_32F);
                memcpy(depthMap.data, depthmapdata_individual.data(), depthmapdata_individual.size()*sizeof(float));
                depthMap.convertTo(individualDepthMap[cam],CV_64F);
            }
        }else if(file_extension=="exr"){
          cv::Mat exrMat=cv::imread(depthmap_path,CV_LOAD_IMAGE_ANYDEPTH);
          //warning!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ONLY IF IS NOT NULL
          Mat bgr[3];   //destination array
          split(exrMat,bgr);//split source
          depthMap=bgr[0].clone();
          width_depth_map=depthMap.cols;
          depthMap.convertTo(individualDepthMap[cam],CV_64F);
        }
        else{
          cout << "Depth map extension not recognised." << endl;
          return 1;
        }

    }


    //Contribution map is not used at all for the moment!
    /*
    //READ CONTRIBUTION MAP
    cv::Mat panoramic_gradient, pano_contribution_map;
    string contribution_map_path=string(argv[3]);
    pano_contribution_map=cv::imread(contribution_map_path, IMREAD_GRAYSCALE);
    if(pano_contribution_map.data==NULL){
        cout << "Error reading image: "<< contribution_map_path << endl;
        return 1;
    }

    cv::resize(pano_contribution_map, pano_contribution_map, cv::Size(width_depth_map, width_depth_map/2));

    //Now calculate gradient image according to sobel to extract borders of image contribution.
    //originalGrayscaleImage_.convertTo(tmp_original,CV_32F);
    // Gradient X
    cv::Mat Dx;
    Sobel( pano_contribution_map, Dx, CV_32F, 1, 0, 5);
    cv::Mat Dy;
    Sobel( pano_contribution_map, Dy, CV_32F, 0, 1, 5);
    cv::addWeighted( cv::abs(Dx), 0.5, cv::abs(Dy), 0.5, 0, panoramic_gradient ); //Approximation
    panoramic_gradient.convertTo(panoramic_gradient, CV_8U);
    cv::threshold(panoramic_gradient,panoramic_gradient,0,255,cv::THRESH_BINARY); //Convert panoramic_gradient from 0 to 1


    //Image with ones where FOV is covered always
    cv::Mat covered_fov;
    cv::threshold(pano_contribution_map,covered_fov,254,255,cv::THRESH_BINARY_INV);
    covered_fov.convertTo(covered_fov, CV_8U);
    cv::Mat not_covered_fov=cv::Mat(covered_fov.rows, covered_fov.cols, CV_8U, cv::Scalar(255));
    not_covered_fov=not_covered_fov-covered_fov;
    */


    //MAIN LOOP, SIFTS AND THEN FIND SEEDS IN THE EQUIRECTANGULAR IMAGE
    cout << "About to start panoramic image" << endl;


    std::vector<cv::Point2d> seeds;

    for(int cam = first_camera; cam <= last_camera; cam++) {
        cout << "Dividing image" << endl;

        int colDivisor=4;
        int rowDivisor=4;

        std::vector<KeyPoint> all_keypoints;

        cout << "Looking for SIFTS individually" << endl;
        // check if divisors fit to image dimensions
        if(image[cam].cols % colDivisor == 0 && image[cam].rows % rowDivisor == 0){
            for(int y = 0; y < image[cam].cols; y += image[cam].cols / colDivisor){
                for(int x = 0; x < image[cam].rows; x += image[cam].rows / rowDivisor){
                    cv::Mat roi=image[cam](cv::Rect(y, x, (image[cam].cols / colDivisor), (image[cam].rows / rowDivisor)));
                    cv::Ptr<Feature2D> f2d = xfeatures2d::SIFT::create(10,3,0.02);
                    std::vector<KeyPoint> keypoints;
                    f2d->detect( roi, keypoints);
                    cout << "Detected " << keypoints.size() << "keypoints" << endl;
                    for(int i =0; i<keypoints.size();i++){
                        keypoints[i].pt=keypoints[i].pt+cv::Point2f(y,x);
                        all_keypoints.push_back(keypoints[i]);
                    }
                }
            }
        }else if(image[cam].cols % colDivisor != 0){
            cerr << "Error: Please use another divisor for the column split." << endl;
            exit(1);
        }
        else if(image[cam].rows % rowDivisor != 0){
            cerr << "Error: Please use another divisor for the row split." << endl;
            exit(1);
        }
        //cout << "Here" << endl;
        cv::Mat displayImage;
        /*
        drawKeypoints(image[cam], all_keypoints, displayImage);

        cv::namedWindow("Features", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Features",displayImage);
        cv::waitKey();
        */
        //For each feature detect it project it into equirectangular according to individual depth map
        for(int i =0; i<all_keypoints.size();i++){
            double depth=individualDepthMap[cam].at<double>(round(all_keypoints[i].pt.y),round(all_keypoints[i].pt.x));
            //Now project this point to equirectangular panorama.
            if(depth!=0){
                double localX, localY, localZ;
                calibration.RCtoCameraVector((double)all_keypoints[i].pt.y, (double)all_keypoints[i].pt.x, cam, localX, localY, localZ);
                double k=depth/sqrt(pow(localX,2)+pow(localY,2)+pow(localZ,2));
                cv::Mat point3d=(cv::Mat_<double>(4,1) << k*localX,k*localY,k*localZ,1.0);

                cv::Mat point3dGlobal=cv::Mat::zeros(4,1,CV_64F);
                calibration.camera3dPointToMultiCam3dPoint(cam,point3d,point3dGlobal);

                double R=sqrt(pow(point3dGlobal.at<double>(0,0),2)+pow(point3dGlobal.at<double>(1,0),2)+pow(point3dGlobal.at<double>(2,0),2));
                double theta=atan2(point3dGlobal.at<double>(1,0),point3dGlobal.at<double>(0,0));
                double phi=acos(point3dGlobal.at<double>(2,0)/R);
                double u,v;

                if(!dome_projection){
                  //Now look for its equirectangular pojection
                  u=(PI-theta)/(2*PI)*(pano_width);
                  v=phi/PI*(pano_height);
                }
                else{
                    //Need to project to dome as well
                    u= (max_angle-(PI-phi)*sin(theta))*(pano_width)/(2*max_angle);
                    v= (max_angle-(PI-phi)*cos(theta))*(pano_height)/(2*max_angle);
                }
                u=u/divisor;
                v=v/divisor;
                panoramic.at<double>(v,u)=R;
                //Put this coordinates into list
                seeds.push_back(cv::Point2d(u,v));
            }
        }
    }



    //We have the gradient image wihch containts the borders of the individual contributions
    //Compute a dot grid
    ofstream myfile;
    myfile.open ("points.csv");

    for (int i=0;i<seeds.size();i++){
        cv::Point2d center=cv::Point2d(seeds[i].x,seeds[i].y);
        cout << seeds[i].x << "; " << seeds[i].y << "; " << panoramic.at<double>(seeds[i].y, seeds[i].x) << endl;
        myfile << seeds[i].x << "; " << seeds[i].y << "; " << panoramic.at<double>(seeds[i].y, seeds[i].x) << "\n";
    }
    myfile.close();
    cout << "Number of seeds used: " << seeds.size() << endl;


    //===========SAVE MASK FOR LOG==================
    double min,max;
    cv::Mat adjMap,colorDepthMap,mask;
    cv::minMaxLoc( panoramic, &min, &max);
    cv::convertScaleAbs( panoramic, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    //cv::imshow("bef",colorDepthMap);
    //cv::waitKey();

    string pano_filename;
    ss.str("");
    string file_without_extension=file_path.substr(0,file_path.rfind("."));
    ss << file_without_extension << "_filtered.png";
    pano_filename.clear();
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    bool writing_success;
    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    cv::Mat validPixels=panoramic!=0;
    cv::Mat toInterpolate=panoramic==0;

    cv::Mat filled=interpolate5(panoramic,validPixels.clone(),toInterpolate.clone());
    panoramic=smooth3(filled,validPixels.clone(),cv::Mat::ones(height_depth_map,width_depth_map,CV_8U)*255,cv::Mat(),50); //Smoothing distance
    //panoramic=filled;


    //We check if it's on the FOV of the cameras.

    for(int y = 0; y< filled.rows; y++){
        int x;
        for(x = 0; x <filled.cols ; x++) {
            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=-(2*PI*x/(filled.cols-1))+PI;// To rotate 180: -PI;
            double phi=PI*y/(filled.rows-1);

            double dist=filled.at<double>(y,x);
            //From theta and phi find the 3D coordinates.
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            int covered=0;
            int result_;
            //From the 3D coordinates, find in how many camera falls the point!
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                double dRow, dCol;
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow, dCol);
                if (result_!=-1) {
                    covered++;
                    }
                }
            if(covered==0){
                filled.at<double>(y,x)=0;
            }
        }
    }




    cv::minMaxLoc( panoramic, &min, &max);
    //cv::Mat adjMap;
    cv::convertScaleAbs( panoramic, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    if(SHOW_DEBUG_IMAGES){

        cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map Smoothed",adjMap);
        cv::waitKey(10);
    }

    ss.str("");
    ss << file_without_extension << "_sifts_smoothed.png";
    pano_filename.clear();
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;



    ss.str("");
    ss << file_without_extension << "_sifts_smoothed.exr";
    string filename=ss.str();
    cv::Mat temp;
    panoramic.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);

    cout << "Mesh file written in "<< filename << endl;
    return 0;
}
