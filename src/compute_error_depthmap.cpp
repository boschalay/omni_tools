#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

//LadybugCalibration class
#include "mcs_calibration.hpp"
#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s calibration_file ground_truth_depth_map final_depth_map first_camera last_camera last_frame <pano_contribution_map.png>\n",argv[0]);
    fprintf(stderr,"<pano_contribution.png> Can be an image or 1 or 2 contributions maps\n",argv[0]);
}


bool isFloat( string myString ) {
    std::istringstream iss(myString);
    float f;
    iss >> noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail();
}

int main(int argc, char *argv[])
{

    if(argc <8)
    {
        printhelp(argv);
        return(1);
    }

    //-- Load calibation matrices.
    MultiCamCalibration calibration; //Create calibration object.
    string calibration_file=string(argv[1]);

    int first_camera=atoi(argv[4]);
    int last_camera=atoi(argv[5]);
    int last_frame=atoi(argv[6]);

    //GT path
    int indexframe,first_frame;
    string gt_path=argv[2];
    indexframe = gt_path.rfind("frame");
    string first_frame_str = gt_path.substr(indexframe+5,5);
    first_frame=atoi(first_frame_str.c_str());
    string gt_path_part1 = gt_path.substr(0,indexframe+5);
    string gt_path_part2 = gt_path.substr(indexframe+10,gt_path.length());

    cout << gt_path_part1 << endl;
    //cout << gt_path_part2 << endl;

    //Depthmap path
    string depthmap_path_part1,depthmap_path_part2;
    string depthmap_path=argv[3];
    cv::Mat constantDepthMap;
    if(isFloat(depthmap_path)){
        constantDepthMap=cv::Mat::ones(20,20,CV_64F);
        constantDepthMap=constantDepthMap*stod(depthmap_path);
    }
    else{
      indexframe = depthmap_path.rfind("frame");
      depthmap_path_part1 = depthmap_path.substr(0,indexframe+5);
      depthmap_path_part2 = depthmap_path.substr(indexframe+10,depthmap_path.length());
    }
    //cout << depthmap_path_part1 << endl;
    //cout << depthmap_path_part2 << endl;

    //Contribution map path
    bool haveContributionMap=false;
    bool compute2ndCont=false;
    bool constantContributionMap=false;
    string contribution_map_path;
    string contribution_map_path_part1;
    string contribution_map_path_part2;

    contribution_map_path=string(argv[7]);
    if(!isFloat(contribution_map_path)){
      indexframe = contribution_map_path.rfind("frame");
      if(indexframe==-1){
        constantContributionMap=true;
      }
      else{
        first_frame_str = contribution_map_path.substr(indexframe+5,5);
        contribution_map_path_part1 = contribution_map_path.substr(0,indexframe+5);
        contribution_map_path_part2 = contribution_map_path.substr(indexframe+10,contribution_map_path.length());
      }
      haveContributionMap=true;
    }
    else if(atoi(contribution_map_path.c_str())==2){
      compute2ndCont=true;
    }


    ofstream myfile;
    char buffer [80];
    time_t t = time(0);   // get time now
    struct tm * now = localtime( & t );
    strftime (buffer,80,"%Y-%m-%d-%H-%M-%S",now);
    string filename="results"+string(buffer)+".csv";
    myfile.open (filename);

    //#pragma omp parallel for default(none) shared(calibration_file,gt_path_part1,gt_path_part2)
    for(int frame=first_frame;frame<=last_frame;frame++){
      cout << frame << endl;

      //READ Calibration

      indexframe = calibration_file.rfind("frame");

      string calibration_path_part1 = calibration_file.substr(0,indexframe+5);
      string calibration_path_part2 = calibration_file.substr(indexframe+10,calibration_file.length());

      calibration.loadCalibrationFile(calibration_path_part1+std::to_string(frame)+calibration_path_part2);


      //READ GROUND TRUTH DEPTH MAP
      gt_path= gt_path_part1 + to_string(frame) + gt_path_part2;

      cv::Mat ground_truth_depth_map;
      ground_truth_depth_map=read_depthmap(gt_path);
      if(ground_truth_depth_map.data==NULL){
        cout << "Error loading GT depthmap" << endl;
        continue;
      }

      int gt_height,gt_width;
      gt_height=ground_truth_depth_map.rows;
      gt_width=ground_truth_depth_map.cols;

      cout << "GT Pano width: " << gt_width << ", GT Pano height: " << gt_height << endl;


      //READ SMOOTHED DEPTHMAP
      depthmap_path=depthmap_path_part1 + to_string(frame) + depthmap_path_part2;
      cv::Mat depthMap;
      if(constantDepthMap.data!=NULL){
          depthMap=constantDepthMap;
      }
      else{
        depthMap=read_depthmap(depthmap_path);
        if(depthMap.data==NULL){
          cout << "Error loading depthmap" << endl;
        }
      }


      cv::resize(depthMap, depthMap, cv::Size(gt_width,gt_height),0,0,cv::INTER_NEAREST );

      int pano_height,pano_width;
      pano_height=depthMap.rows;
      pano_width=depthMap.cols;

      if(SHOW_DEBUG_IMAGES){
        //Show depthmap
        double min,max;
        cv::Mat colorDepthMap,mask;
        cv::minMaxLoc( depthMap, &min, &max);

        cv::convertScaleAbs( depthMap, colorDepthMap, 255 / (max-min), -min*255/(max-min));
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);


        cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map",colorDepthMap);
        cv::waitKey();
      }

      stringstream ss;

      //READ CONTRIBUTION MAP
      cv::Mat pano_contribution_map;
      cv::Mat pano_second_contribution_map;
      pano_second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8U);

      if(haveContributionMap){
        if (!constantContributionMap){
          contribution_map_path= contribution_map_path_part1 + to_string(frame) + contribution_map_path_part2;
        }
        pano_contribution_map=cv::imread(contribution_map_path, IMREAD_GRAYSCALE);
        if(pano_contribution_map.data==NULL){
            cout << "Error reading image: "<< contribution_map_path << endl;
            return 1;
        }
        pano_contribution_map.setTo((unsigned char) 0, pano_contribution_map == 255);
        cv::resize(pano_contribution_map, pano_contribution_map, cv::Size(pano_width,pano_height),0,0,cv::INTER_NEAREST );
        haveContributionMap=true;
      }
      else{
        //We need to generate a contribution map.
        cout << "Generating contribution map for depthmap" << endl;
        pano_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8U);
        pano_second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8U);

        int y;
        #pragma omp parallel for default(none) shared(cout,pano_height, calibration, pano_width, depthMap, last_camera, first_camera,pano_contribution_map, pano_second_contribution_map)
        for(y = 0; y< pano_height; y++){
            int x;
            for(x = 0; x <pano_width ; x++) {

                double theta,phi;
                if(pano_height!=pano_width){
                  theta=PI-x*2*PI/pano_width;// To rotate 180: -PI;
                  phi=PI*y/pano_height;
                }else{
                  theta=PI-2*PI*x/pano_width;
                  phi=y*PI/pano_height;
                  phi=PI-phi;
                }


                double dist=0;
                if(depthMap.at<double>(y,x)!=0){
                    dist=depthMap.at<double>(y,x);
                }

                //From theta and phi find the 3D coordinates.
                double globalZ=dist*cos(phi);
                double globalX=dist*sin(phi)*cos(theta);
                double globalY=dist*sin(phi)*sin(theta);

                int result_;
                //From the 3D coordinates, find where the point falls in the individual image
                int cam;
                  //This pixel is not in the contribution map. But check anyway if according to model there is somebody that can use it
                  float minDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.
                  float secondMinDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.

                  int cameraMinDistanceCenter=-1;
                  int secondCameraMinDistanceCenter=-1;
                  int covered=0;
                  int numberCameras=calibration.getNumberCameras();
                  int imageWidth=calibration.getImageWidth();
                  int imageHeight=calibration.getImageHeight();

                  double dRow[last_camera];
                  std::fill_n(dRow, numberCameras,-1);
                  double dCol[last_camera];
                  std::fill_n(dCol, numberCameras,-1);

                  //From the 3D coordinates, find in how many camera falls the point!
                  for(cam = first_camera; cam<= last_camera; cam++){
                      result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                      if (result_!=-1) {
                          covered++;
                          float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                          if (distanceCenter<minDistanceCenter){
                              secondMinDistanceCenter=minDistanceCenter;
                              secondCameraMinDistanceCenter=cameraMinDistanceCenter;

                              minDistanceCenter=distanceCenter;
                              cameraMinDistanceCenter=cam;
                          }
                          if (distanceCenter<secondMinDistanceCenter && distanceCenter!= minDistanceCenter){
                              secondMinDistanceCenter=distanceCenter;
                              secondCameraMinDistanceCenter=cam;
                          }
                      }
                  }
                  if(covered>0){
                    pano_contribution_map.at<unsigned char>(y,x)=(unsigned char) cameraMinDistanceCenter;
                    if(secondCameraMinDistanceCenter>-1){
                      pano_second_contribution_map.at<unsigned char>(y,x)=(unsigned char) secondCameraMinDistanceCenter;
                    }
                  }
              }
          }
      }
      if(!compute2ndCont){
        //we don't want to compute the second one.
        pano_second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8U);
      }


      if(SHOW_DEBUG_IMAGES){
      //Show contribution map
        double min,max;
        cv::Mat colorDepthMap,mask;
        cv::minMaxLoc( pano_contribution_map, &min, &max);

        cv::Mat colorContributionMap;
        cv::convertScaleAbs( pano_contribution_map, colorContributionMap, 255 / (max-min), -min*255/(max-min));
        applyColorMap(colorContributionMap, colorContributionMap, COLORMAP_JET);

        cv::namedWindow("Contribution map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Contribution map",colorContributionMap);
        cv::waitKey();
      }
      cout << "About to start panoramic image" << endl;
      //Panoramic image
      cv::Mat panoramic;
      panoramic=cv::Mat::ones(pano_height,pano_width,CV_64F)*-1;

      bool done[pano_height];
      std::fill_n(done, pano_height, false);

      int y;
      #pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, pano_width, depthMap, ground_truth_depth_map, pano_contribution_map, pano_second_contribution_map, calibration)
      for(y = 0; y< pano_height; y++){
          int x;
          for(x = 0; x <pano_width ; x++) {

              int final_camera=-1;
              double final_col=-1;
              double final_row=-1;
              //Find 3D position
              //Theta goes from -pi to pi
              //Phi goes from 0  to pi
              double theta,phi;
              if(pano_height!=pano_width){
                theta=PI-x*2*PI/pano_width;// To rotate 180: -PI;
                phi=PI*y/pano_height;
              }else{
                theta=PI-2*PI*x/pano_width;
                phi=y*PI/pano_height;
                phi=PI-phi;
              }

              double dist=0;
              double dist_gt=0;
              if(depthMap.at<double>(y,x)!=0 && ground_truth_depth_map.at<double>(y,x)!=0){
                  dist=depthMap.at<double>(y,x);
                  dist_gt=ground_truth_depth_map.at<double>(y,x);


                  //From theta and phi find the 3D coordinates.
                  double globalZ=dist*cos(phi);
                  double globalX=dist*sin(phi)*cos(theta);
                  double globalY=dist*sin(phi)*sin(theta);
                  double globalZ_gt=dist_gt*cos(phi);
                  double globalX_gt=dist_gt*sin(phi)*cos(theta);
                  double globalY_gt=dist_gt*sin(phi)*sin(theta);


                  int result_;

                  //From the 3D coordinates, find where the point falls in the individual image
                  int cam=(int)pano_contribution_map.at<unsigned char>(y,x);
                  final_camera=cam;
                  if (final_camera!=0){
                    cv::Mat PointGlobal(4,1,CV_64F);
                    PointGlobal.at<double>(0,0)=globalX;
                    PointGlobal.at<double>(1,0)=globalY;
                    PointGlobal.at<double>(2,0)=globalZ;
                    PointGlobal.at<double>(3,0)=1.0;
                    cv::Mat PointLocal(4,1,CV_64F);

                    calibration.multiCam3DPoint2camera3dPoint(PointGlobal,final_camera,PointLocal);
                    PointGlobal.at<double>(0,0)=globalX_gt;
                    PointGlobal.at<double>(1,0)=globalY_gt;
                    PointGlobal.at<double>(2,0)=globalZ_gt;
                    PointGlobal.at<double>(3,0)=1.0;
                    cv::Mat PointLocal_gt(4,1,CV_64F);
                    calibration.multiCam3DPoint2camera3dPoint(PointGlobal,final_camera,PointLocal_gt);

                    double len1=norm(PointLocal(cv::Rect(0,0,1,3)));
                    double len2=norm(PointLocal_gt(cv::Rect(0,0,1,3)));

                    double dot=PointLocal.at<double>(0,0)*PointLocal_gt.at<double>(0,0) +
                                PointLocal.at<double>(1,0)*PointLocal_gt.at<double>(1,0) +
                                PointLocal.at<double>(2,0)*PointLocal_gt.at<double>(2,0);

                    double angular_error=dot/(len1*len2);

                    if (angular_error >= 1.0) angular_error= 0.0;
                    else if (angular_error <= -1.0) angular_error= PI;
                    else angular_error= acos(angular_error); // 0..PI

                    panoramic.at<double>(y,x)=angular_error;
                }
                //If we have second contribtion map take the maximum.
                cam=(int)pano_second_contribution_map.at<unsigned char>(y,x);
                final_camera=cam;
                if (final_camera!=0){
                  cv::Mat PointGlobal(4,1,CV_64F);
                  PointGlobal.at<double>(0,0)=globalX;
                  PointGlobal.at<double>(1,0)=globalY;
                  PointGlobal.at<double>(2,0)=globalZ;
                  PointGlobal.at<double>(3,0)=1.0;
                  cv::Mat PointLocal(4,1,CV_64F);

                  calibration.multiCam3DPoint2camera3dPoint(PointGlobal,final_camera,PointLocal);
                  PointGlobal.at<double>(0,0)=globalX_gt;
                  PointGlobal.at<double>(1,0)=globalY_gt;
                  PointGlobal.at<double>(2,0)=globalZ_gt;
                  PointGlobal.at<double>(3,0)=1.0;
                  cv::Mat PointLocal_gt(4,1,CV_64F);
                  calibration.multiCam3DPoint2camera3dPoint(PointGlobal,final_camera,PointLocal_gt);

                  double len1=norm(PointLocal(cv::Rect(0,0,1,3)));
                  double len2=norm(PointLocal_gt(cv::Rect(0,0,1,3)));

                  double dot=PointLocal.at<double>(0,0)*PointLocal_gt.at<double>(0,0) +
                              PointLocal.at<double>(1,0)*PointLocal_gt.at<double>(1,0) +
                              PointLocal.at<double>(2,0)*PointLocal_gt.at<double>(2,0);

                  double angular_error=dot/(len1*len2);

                  if (angular_error >= 1.0) angular_error= 0.0;
                  else if (angular_error <= -1.0) angular_error= PI;
                  else angular_error= acos(angular_error); // 0..PI

                  if(angular_error>panoramic.at<double>(y,x)){
                    panoramic.at<double>(y,x)=angular_error;
                  }
                }

              }
          }
          done[y]=true;

          int numberDone=0;
          for( int i = 0; i < pano_height; i++ ) {
              if( done[i]==true ) {
                  numberDone++;
              }
          }
          //std::cout << numberDone << std::flush;
          //std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;
      }

      //We save the exr file that contains -1 (point no model)
      std::vector<int> qualityType;
      qualityType.push_back(IMWRITE_PNG_COMPRESSION);
      qualityType.push_back(9); //Jpeg quality

      stringstream pano_filename;
      pano_filename.str("");
      if(first_camera!=last_camera){
        pano_filename << "depthmap_error_frame" << setfill('0')<< setw(5) << frame << ".exr";
      }
      else{
        pano_filename << "depthmap_error_frame" << setfill('0')<< setw(5) << frame << "_camera" << first_camera <<".exr";
      }

      cv::Mat panoramic_float;
      panoramic.convertTo(panoramic_float,CV_32F);

      bool writing_success=imwrite(pano_filename.str(), panoramic_float, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename.str() << endl;
      }

      panoramic.setTo(0, panoramic == -1);




      double min,max;
      cv::Mat colorDepthMap,mask;

      cv::minMaxLoc( panoramic, &min, &max);
      cv::Scalar mean=cv::mean(panoramic,panoramic!=0);
      //double median=std::nth_element(panoramic.data.begin(), panoramic.data.begin() + panoramic.data.size()/2, panoramic.data.end());
      cout << "Max angular error is: " <<  max << endl;
      cout << "Mean angular error is: " <<  mean.val[0] << endl;


      cv::convertScaleAbs( panoramic, colorDepthMap, 255 / (max-min), -min*255/(max-min));
      applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
      cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
      colorDepthMap.setTo(Scalar(0,0,0), panoramic_float==-1);

      if(SHOW_DEBUG_IMAGES){
        cv::namedWindow("Depth Map Error", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map Error",colorDepthMap);
        cv::waitKey(20);
      }

      pano_filename.str("");
      if(first_camera!=last_camera){
        pano_filename << "depthmap_error_frame" << setfill('0')<< setw(5) << frame << ".png";
      }else{
        pano_filename << "depthmap_error_frame" << setfill('0')<< setw(5) << frame << "_camera" << first_camera <<".png";
      }

      writing_success=imwrite(pano_filename.str(), colorDepthMap, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename.str() << endl;
      }


      if(first_camera!=last_camera){

        //Let's compute interesting statistics
        cout << "Generating contribution map border" << endl;
        cv::Mat panoramic_gradient=cv::Mat::zeros(pano_height,pano_width,CV_8U);
        //For each pixel of the contribution map we open a window and see if there is any value different than it.
        #pragma omp parallel for default(none) shared(panoramic_gradient,pano_contribution_map, pano_width, pano_height)
        for(int col=0;col<pano_width;col++){
            for(int row=0;row<pano_height;row++){

                int halfsize=1; //Should be even.
                cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
                cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,pano_width),std::min(row+halfsize+1,pano_height));
                cv::Mat image_roi=pano_contribution_map(cv::Rect(top_left,bottom_right));
                cv::Mat nonZeroCoordinates;
                cv::Mat tmp;
                cv::bitwise_and(image_roi!=(unsigned char) 0, image_roi!=pano_contribution_map.at<unsigned char>(row,col) , tmp);
                cv::findNonZero(tmp, nonZeroCoordinates);
                if(pano_contribution_map.at<unsigned char>(row,col) != 0 and nonZeroCoordinates.total()>0){
                    panoramic_gradient.at<unsigned char>(row,col)=(unsigned char)255;
                }
            }
        }
        panoramic_gradient=panoramic_gradient & (panoramic!=0);

        if(SHOW_DEBUG_IMAGES){

          cv::namedWindow("Panoramic mask", WINDOW_NORMAL | WINDOW_KEEPRATIO);
          cv::imshow("Panoramic mask",panoramic_gradient);
          cv::waitKey(0);
        }
        //Find the error only for this mask.
        double max_border;
        cv::Scalar mean_border;
        cv::Point minloc,maxloc;
        cv::minMaxLoc( panoramic, &min, &max_border,&minloc,&maxloc,panoramic_gradient);
        mean_border=cv::mean(panoramic,panoramic_gradient);
        cout << "Max angular error in contribution border is: " <<  max_border << endl;
        cout << "Mean angular error in contribution border is: " <<  mean_border.val[0] << endl;

        myfile << frame << "," << max << "," << mean.val[0]<< "," << max_border << "," << mean_border.val[0] <<"\n";

        if(compute2ndCont){
          std::vector<int> qualityType;
          qualityType.push_back(IMWRITE_PNG_COMPRESSION);
          qualityType.push_back(9); //Jpeg quality

          ss.str("");
          ss << depthmap_path_part1 << setw(5) << frame << "_1st_contribution_map.png";
          string pano_filename=ss.str();
          bool writing_success=imwrite(pano_filename, pano_contribution_map, qualityType);
          if(!writing_success){
              cout << "\nError writing image! Check output directory existence." << endl;
              return 1;
          }
          else{
            cout << "Image written in " << pano_filename << endl;
          }

          ss.str("");
          ss << depthmap_path_part1 << setw(5) << frame << "_2nd_contribution_map.png";
          pano_filename=ss.str();
          writing_success=imwrite(pano_filename, pano_second_contribution_map, qualityType);
          if(!writing_success){
              cout << "\nError writing image! Check output directory existence." << endl;
              return 1;
          }
          else{
            cout << "Image written in " << pano_filename << endl;
          }


        }

      }
    }
    myfile.close();

    return 0;
}
