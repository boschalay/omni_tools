#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp"

#define PI 3.14159265358979323846

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width calibration_file radius first_camera last_camera draw_border\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc <7)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    int first_camera=atoi(argv[4]);
    int last_camera=atoi(argv[5]);
    bool draw_border=atoi(argv[6]);

    cout << "Arguments: " << argc << endl;
    cv::Mat scaledDepthMap, adjMap;
    bool haveDepthMap=false;


    //List of colors
    std::vector<cv::Vec3b> colors_list;
      colors_list.push_back(cv::Vec3b(165,194,102));
      colors_list.push_back(cv::Vec3b(98,141,252));
      colors_list.push_back(cv::Vec3b(203,160,141));
      colors_list.push_back(cv::Vec3b(195,138,231));
      colors_list.push_back(cv::Vec3b(84,216,166));
      colors_list.push_back(cv::Vec3b(47,217,255));


    //colors_list.push_back(cv::Vec3b(90,90,235));
    //colors_list.push_back(cv::Vec3b(75,190,45));
    //colors_list.push_back(cv::Vec3b(75,165,245));
    //colors_list.push_back(cv::Vec3b(160,95,175));
    //colors_list.push_back(cv::Vec3b(200,140,87));
    //colors_list.push_back(cv::Vec3b(55,185,205));








    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[2]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=atof(argv[3]);
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();

    double R=calibration.getSphereRadius();
    int result;

    //Panorama that details the FOV of each camera
    cv::Mat panoramic;
    panoramic=cv::Mat::ones(pano_height,pano_width,CV_8UC3);
    panoramic.setTo(cv::Scalar(255,255,255));

    //Panorama details the contribution of each camera. (The one that its projection is closer to the center)
    cv::Mat panoramic_no_overlap;
    panoramic_no_overlap=cv::Mat::ones(pano_height,pano_width,CV_8UC3);
    panoramic_no_overlap.setTo(cv::Scalar(255,255,255));

    //Panorama with the transition borders.
    cv::Mat panoramic_no_overlap_numbers;
    panoramic_no_overlap_numbers=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
    panoramic_no_overlap_numbers.setTo(cv::Scalar(255));

    cv::Mat equiIndividualMask[last_camera+1];
    for(int cam = first_camera; cam <= last_camera; cam++) {
        equiIndividualMask[cam]=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);

    }

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(y = 0; y< pano_height; y++){
        double dRow[last_camera];
        std::fill_n(dRow, numberCameras,-1);
        double dCol[last_camera];
        std::fill_n(dCol, numberCameras,-1);
        int x;
        for(x = 0; x <pano_width ; x++) {
            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=-(2*PI*x/(pano_width-1))+PI;// To rotate 180: -PI;
            double phi=PI*y/(pano_height-1);

            double dist=R;
            if(haveDepthMap){
                if(scaledDepthMap.at<double>(y,x)!=0){
                    dist=scaledDepthMap.at<double>(y,x)!=0;
                }
            }

            //From theta and phi find the 3D coordinates.
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            bool covered_first_time=false;
            int covered=0;
            int result_;
            float minDistanceCenter=pano_width*pano_width; // Max value possible.
            int cameraMinDistanceCenter=-1;

            //From the 3D coordinates, find in how many camera falls the point!
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                }
            }
            if(covered>0){
                for(cam = first_camera; cam<= last_camera; cam++){
                    result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                    if (result_!=-1) {
                        if(!covered_first_time){
                            panoramic.at<cv::Vec3b>(y,x)= cv::Vec3b(0,0,0);
                            covered_first_time=true;
                        }
                        float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                        /*This doesnot work
                        if (cam!=cameraMinDistanceCenter && (distanceCenter-minDistanceCenter)<100.0){
                            panoramic3.at<unsigned char>(y,x)=(unsigned char) 255;
                        }
                        */
                        panoramic.at<cv::Vec3b>(y,x)=panoramic.at<cv::Vec3b>(y,x)+colors_list[cam]*1.0/covered;
                    }
                }
                panoramic_no_overlap.at<cv::Vec3b>(y,x)=colors_list[cameraMinDistanceCenter];
                panoramic_no_overlap_numbers.at<unsigned char>(y,x)=cameraMinDistanceCenter;
                equiIndividualMask[cameraMinDistanceCenter].at<unsigned char>(y,x)=(unsigned char) 255;
            }


            if(draw_border){
                bool covered_border=false;
                //From the 3D coordinates, find in how many camera falls the point!
                for(cam = first_camera; cam<= last_camera; cam++){
                    result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                    if (result_!=-1 && (dRow[cam]<50 || dRow[cam]>(imageHeight-50)|| dCol[cam]<50 || dCol[cam] > (imageWidth-50))) {
                        covered_border=true;
                    }
                    else{
                        dRow[cam]=-1;
                        dCol[cam]=-1;
                    }
                }
                if(covered_border){
                    panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) 0;
                    panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) 0;
                    panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) 0;
                }
            }



        }
        /*
        done[y]=true;

        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;
        */
        
        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;

    }

    cv::Mat panoramic_grayscale, panoramic_gradient;
    //Now calculate gradient image according to sobel to extract borders of image contribution.
    cv::cvtColor(panoramic_no_overlap, panoramic_grayscale, cv::COLOR_BGR2GRAY);

    //originalGrayscaleImage_.convertTo(tmp_original,CV_32F);
    // Gradient X
    cv::Mat Dx;
    Sobel( panoramic_grayscale, Dx, CV_32F, 1, 0, 5);
    cv::Mat Dy;
    Sobel( panoramic_grayscale, Dy, CV_32F, 0, 1, 5);
    cv::addWeighted( cv::abs(Dx), 0.5, cv::abs(Dy), 0.5, 0, panoramic_gradient ); //Approximation
    cv::threshold(panoramic_gradient,panoramic_gradient,0,255,cv::THRESH_BINARY);
    panoramic_gradient.convertTo(panoramic_gradient, CV_8UC1);

    //Display
    cv::namedWindow("Gradient", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Gradient", panoramic_gradient );
    cv::waitKey();


    stringstream ss;
    string pano_filename;
    ss.str("");
    pano_filename.clear();
    ss << "pano_fov_radius"<< R <<"_overlap.png";
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    cout << pano_filename << endl;

    bool writing_success;
    writing_success=imwrite(pano_filename, panoramic, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    cout << pano_filename << endl;

    ss.str("");
    pano_filename.clear();
    ss << "pano_fov_radius"<< R <<".png";
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, panoramic_no_overlap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    cout << pano_filename << endl;

    ss.str("");
    pano_filename.clear();
    ss << "pano_fov_radius"<< R <<"_controbution_map.png";
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, panoramic_no_overlap_numbers, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    cout << pano_filename << endl;

    ss.str("");
    pano_filename.clear();
    ss << "pano_fov_radius"<< R <<"_mask.png";
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, panoramic_gradient, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    cout << pano_filename << endl;


    cout << "Success!" << endl;

    return 0;
}
