#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel

//LadybugCalibration class
#include "mcs_calibration.hpp"

//Global variable with look up table.
#define PI 3.14159265358979323846

using namespace cv;
using namespace std;


//Global variables
cv::Mat panoramic[6];
int pano_width, pano_height;

bool read_3d_mesh( char *mesh_file_path, int first_camera, int last_camera)
{
        FILE *fp = fopen( mesh_file_path, "r");
        if ( fp == NULL){
                printf( "Can't read 3D mesh file: %s\n", mesh_file_path);
                return false;
        }

        if ( fscanf( fp, "cols %d rows %d\n", &pano_width, &pano_height) != 2){
                printf( "Can't read cols/rows in 3d mesh file.\n");
                fclose( fp);
                return false;
        }

        printf("cols: %d, rows: %d\n",pano_width,pano_height);

        for(int i=0; i <6; i++){
                panoramic[i] =cv::Mat::zeros(pano_height,pano_width,CV_64FC3);
        }

        for ( int c = 0; c < last_camera-first_camera+1; c++){
                for ( int iRow = 0; iRow < pano_height; iRow++ ){
                        for ( int iCol = 0; iCol < pano_width; iCol++ ){
                                int R,C;
                                double alpha;
                                if ( fscanf( fp, "%d, %d, %lf", &R, &C, &alpha) != 3){
                                        printf( "Can't read grid data in 3d mesh file.\n");
                                        printf("c: %d, row: %d, col: %d\n",c, iRow, iCol);
                                        return false;
                                }
                                panoramic[c+first_camera].ptr<int>(iRow,iCol)[0]=R;
                                panoramic[c+first_camera].ptr<int>(iRow,iCol)[1]=C;
                                panoramic[c+first_camera].ptr<double>(iRow,iCol)[2]=alpha;
                        }
                }
        }
        fclose( fp);
        return true;
}

void printhelp(char *argv[]){
	fprintf(stderr,"Usage: %s <camera0_first_frame_channel0.jpg> <mesh> <Nframes-1> <Select every X frames> <first_camera> <last_camera>\n",argv[0]);
    fprintf(stderr,"This will generate Nframes panoramas starting from first frame. \n");
}


int main(int argc, char *argv[])
{

	if(argc != 7)
	{
		printhelp(argv);
		return(1);
	}

	///Split name of the file in four parts. PART0-#CAMERA-PART1-#FRAME-PART3
	string image0=string(argv[1]);
	int cameraindex = image0.rfind("camera");
	string file_part0 = image0.substr(0, cameraindex);
	string file_part1 = image0.substr(0, cameraindex+6);
	//Find now the frame. The numbering of the frame is 5 digits.
	int indexframe = image0.rfind("frame");
	string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
	string first_frame_str = image0.substr(indexframe+5,5);
	int first_frame=atoi(first_frame_str.c_str());
	string file_part3 = image0.substr(indexframe+10);
  string file_part4 = image0.substr(indexframe+10,image0.length()-4-indexframe-10);


  int first_camera= atoi(argv[5]);
  int last_camera= atoi(argv[6]);

	cout << "Reading look up table ..." << endl;
  if (!read_3d_mesh( argv[2],first_camera, last_camera))
  {
      return 1;
  }

    int skip_rate=atoi(argv[4]);
	int nframes=atoi(argv[3])/skip_rate; //Nframes+1
	//Initialize frame as first_frame
	//El bucle hauria de ser fora per ser més eficient
	//#pragma omp parallel for default(none) shared(first_frame, nframes, file_part0, file_part1, file_part2, file_part3, file_part4, pano_height, pano_width, panoramic,skip_rate)
	for (int i=0;i<nframes;i++) {
	//for (int frame=first_frame;frame<(first_frame+nframes);frame++) {

        int frame=first_frame+i*skip_rate;

        //Create the images from bayer jpegs.
        Mat image[6];
        string image_path;
        stringstream ss;
        char file[150];
        bool reading_error=false;
        for (int cam=0;cam<5;cam++) {
			reading_error=false;
			Mat imgMat[4]; //height, width
			sprintf (file, "%scamera%i_frame%05d%s.jpg",file_part0.c_str(),cam+1,frame,file_part4.c_str());
			image[cam] = cv::imread(file, IMREAD_COLOR);
			if(image[cam].data==NULL){
				printf("Error reading image: %s!Check names and directory existence\n", file);
				reading_error=true;
				//return 1;
			}
		}

        //cout << "All images opened succesfully." << endl;
        //cout << "Starting pano." << endl;

        //Panoramic image
        cv::Mat panoramic_image =cv::Mat::zeros(pano_height,pano_width,CV_8UC3);

        for(int y = 0; y< pano_height; y++){
            for(int x = 0; x !=pano_width ; x++) {
                for(int cam = first_camera; cam<= last_camera; cam++){
                    cv::Vec3b intensity = image[cam-1].at<cv::Vec3b>(panoramic[cam].ptr<int>(y,x)[0],panoramic[cam].ptr<int>(y,x)[1]);
                    panoramic_image.ptr<unsigned char>(y,x)[0]+=(unsigned char) ((double) intensity.val[0])*panoramic[cam].ptr<double>(y,x)[2];
                    panoramic_image.ptr<unsigned char>(y,x)[1]+=(unsigned char) ((double) intensity.val[1])*panoramic[cam].ptr<double>(y,x)[2];
                    panoramic_image.ptr<unsigned char>(y,x)[2]+=(unsigned char) ((double) intensity.val[2])*panoramic[cam].ptr<double>(y,x)[2];
                    /*if(panoramic[cam].ptr<double>(y,x)[2]!=0){
						panoramic_image.at<cv::Vec3b>(y,x)=intensity;
					}
					*/
                }
            }
        }
        /*
        //Display
        cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow( "Display window", panoramic_image );
        cv::waitKey();
        */

        string pano_filename;
        ss.str("");
        pano_filename.clear();
        ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << frame <<  file_part4 <<".jpg";
        //ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << frame << ".png";
        pano_filename=ss.str();

        std::vector<int> qualityType;
        qualityType.push_back(IMWRITE_JPEG_QUALITY);
        qualityType.push_back(85); //Jpeg quality

        printf("%s\n",pano_filename.c_str());

        bool writing_success;
        //writing_success=imwrite(pano_filename, panoramic_image, qualityType);
        writing_success=imwrite(pano_filename, panoramic_image);
        if(!writing_success){
            //std::cout << "\nError writing image! Check output directory existence." << std::endl;
            //return 1;
        }
        //cout << frame-first_frame << "/" << nframes << "\r" << flush;
    }
    cout << "Success!" << endl;
    return 0;
}
