#include <stdio.h>
#include <iostream>
#include "mcs_calibration.hpp"


// Constructor
MultiCamCalibration::MultiCamCalibration(){
    fisheyeModel_=false;
}
//Destructor
MultiCamCalibration::~MultiCamCalibration(){
}

//=========================================PUBLIC METHODS===================================================

void MultiCamCalibration::setSphereRadius(double radius){
    sphereRadius_=radius;
}

void MultiCamCalibration::setNumberCameras(int numberCameras){
    
    //Initialize matrices
    for(int i=0; i <numberCameras; i++){
        cameraMatrix_.push_back(cv::Mat::eye(3,3,CV_64F));
        extrinsics_.push_back(cv::Mat::zeros(6,1,CV_64F));
        distCoeffs_.push_back(cv::Mat::zeros(8,1,CV_64F));
        local2globalMatrix_.push_back(cv::Mat::eye(4,4,CV_64F));
        global2localMatrix_.push_back(cv::Mat::eye(4,4,CV_64F));
    }

    numberCameras_=numberCameras;
}

void MultiCamCalibration::setImageWidth(int width){
    imageWidth_=width;
}
void MultiCamCalibration::setModel(bool fisheye_model){
    fisheyeModel_=fisheye_model;
}
void MultiCamCalibration::setImageHeight(int height){
    imageHeight_=height;
}

double MultiCamCalibration::getSphereRadius() const{
    return sphereRadius_;
}

int MultiCamCalibration::getImageWidth() const{
    return imageWidth_;
}

int MultiCamCalibration::getImageHeight() const{
    return imageHeight_;
}
int MultiCamCalibration::getNumberCameras() const{
    return numberCameras_;
}

//Set values
void MultiCamCalibration::setExtrinsics(int camera, cv::Mat extrinsicsMat){
    extrinsics_[camera]=extrinsicsMat.clone();
}

void MultiCamCalibration::setDistCoeffs(int camera, cv::Mat distCoeffsMat){
    distCoeffs_[camera]=distCoeffsMat.clone();
}

void MultiCamCalibration::getDistCoeffs(int camera, cv::Mat& distCoeffsMat){
    distCoeffsMat=distCoeffs_[camera].clone();
}

void MultiCamCalibration::setCameraMatrix(int camera, cv::Mat matrix){
    cameraMatrix_[camera]=matrix.clone();
}

void MultiCamCalibration::getCameraMatrix(int camera, cv::Mat& matrix) const {
    matrix=cameraMatrix_[camera].clone();
}

void MultiCamCalibration::computeAllMatrices(){
    cv::Mat matrix(4, 4, CV_64F);
    for(int i=0; i<numberCameras_;i++){
        extrinsics2HomogeneousMatrix( extrinsics_[i].at<double>(0), extrinsics_[i].at<double>(1), extrinsics_[i].at<double>(2), extrinsics_[i].at<double>(3), extrinsics_[i].at<double>(4), extrinsics_[i].at<double>(5),matrix);
        local2globalMatrix_[i]=matrix.clone();
        homogeneousMatrixInverse(local2globalMatrix_[i],matrix);
        global2localMatrix_[i]=matrix.clone();
    }
}

// Get Transformation matrices

void MultiCamCalibration::getCameraTransformationMatrix(int camera, cv::Mat& matrix)const{
    matrix=local2globalMatrix_[camera];
}

// Get Inverse Transformation matrices

void MultiCamCalibration::getCameraInverseTransformationMatrix(int camera, cv::Mat& matrix){
    matrix=global2localMatrix_[camera];
}

int MultiCamCalibration::MultiCamXYZtoRC(double dMultiCamX, double dMultiCamY, double dMultiCamZ, unsigned int uiCamera, double& dRow, double& dCol) const{

    cv::Mat PointGlobal(4,1,CV_64F);
    PointGlobal.at<double>(0,0)=dMultiCamX;
    PointGlobal.at<double>(1,0)=dMultiCamY;
    PointGlobal.at<double>(2,0)=dMultiCamZ;
    PointGlobal.at<double>(3,0)=1.0;

    //Let's convert this to the camera 3D coordinates...
    cv::Mat PointLocal(4,1,CV_64F);
    PointLocal= global2localMatrix_[uiCamera]*PointGlobal;
    
    int ret=cameraXYZtoRC(PointLocal.at<double>(0,0), PointLocal.at<double>(1,0), PointLocal.at<double>(2,0), uiCamera, dRow,dCol);
    if(ret==-1) return -1;
    
    return 0;
}

int MultiCamCalibration::cameraXYZtoRC(double dcameraX, double dcameraY, double dcameraZ, unsigned int uiCamera, double& dRow, double& dCol) const{

    if(dcameraZ<0){
        return -1;
    }

    cv::Mat inputPoint(1,1,CV_64FC3);
    inputPoint.at<cv::Vec3d>(0,0)=cv::Vec3d(dcameraX,dcameraY,dcameraZ);

    cv::Mat rvec=cv::Mat::zeros(3,1,CV_64F);
    cv::Mat tvec=cv::Mat::zeros(3,1,CV_64F);;

    cv::Mat cameraMatrix= cameraMatrix_[uiCamera];

    cv::Mat distCoeffsMat= distCoeffs_[uiCamera].clone();

    cv::Mat outputPoint(1,1,CV_64F);
    if (!fisheyeModel_){
        cv::projectPoints(inputPoint, rvec, tvec, cameraMatrix, distCoeffsMat, outputPoint);
    }
    else{
        cv::fisheye::projectPoints(inputPoint, outputPoint,rvec, tvec, cameraMatrix, distCoeffsMat(cv::Rect(0,0,1,4)).clone());
    }

    dRow=outputPoint.at<cv::Vec3d>(0,0)[1];
    dCol=outputPoint.at<cv::Vec3d>(0,0)[0];

    if(dRow<0 || dRow>(imageHeight_-1) || dCol<0 || dCol>(imageWidth_-1) ){
        return -1;
    }
    return 0;
}

int MultiCamCalibration::RCtoMultiCamXYZ(double dRow, double dCol, unsigned int uiCamera, double& dMultiCamX, double& dMultiCamY, double& dMultiCamZ) const{

    double vx,vy,vz;
    RCtoCameraVector(dRow, dCol, uiCamera, vx, vy, vz);

    cv::Mat localVector(4,1,CV_64F);
    localVector.at<double>(0,0)=vx;
    localVector.at<double>(1,0)=vy;
    localVector.at<double>(2,0)=vz;
    localVector.at<double>(3,0)=0.0;

    cv::Mat localOrigin=cv::Mat::zeros(4,1,CV_64F);
    localOrigin.at<double>(3,0)=1.0;
    //Let's convert these to MultiCam 3D coordinates...
    cv::Mat globalOrigin(4,1,CV_64F);
    globalOrigin= local2globalMatrix_[uiCamera]*localOrigin;
    cv::Mat globalVector(4,1,CV_64F);
    globalVector= local2globalMatrix_[uiCamera]*localVector;
    //Solve 2n order equation
    cv::Mat pointGlobal(4,1,CV_64F);
        
    double a=globalVector.dot(globalVector);
    double b=2*globalOrigin.dot(globalVector);
    double c=globalOrigin.dot(globalOrigin)-pow(sphereRadius_,2);
            
    double t1=(-b+sqrt(pow(b,2)-4*a*c))/(2*a);
    double t2=(-b-sqrt(pow(b,2)-4*a*c))/(2*a);
    if (t1>0 && t2<0){
        pointGlobal=globalOrigin+t1*globalVector;
    }
    else if (t2>0 && t1<0){
        pointGlobal=globalOrigin+t2*globalVector;
    }
    else{
        printf( "Bug in the core class. . (RC to MultiCam XYZ) \n");
    }

    dMultiCamX=pointGlobal.at<double>(0,0);
    dMultiCamY=pointGlobal.at<double>(1,0);
    dMultiCamZ=pointGlobal.at<double>(2,0);
    
    //Fix
    return 0;
}

int MultiCamCalibration::RCtoCameraVector(double dRow, double dCol, unsigned int uiCamera, double& dLocalX, double& dLocalY, double& dLocalZ) const{

    //Undistort point.
    cv::Mat cameraMatrix= cameraMatrix_[uiCamera];
    cv::Mat distCoeffsMat= distCoeffs_[uiCamera].clone();

    cv::Mat inputPoint(1,1,CV_64FC2);
    //Point is [u,v]
    inputPoint.at<cv::Vec2d>(0,0)[0]=dCol;
    inputPoint.at<cv::Vec2d>(0,0)[1]=dRow;

    cv::Mat outputPoint(1,1,CV_64FC3);
    if(!fisheyeModel_) cv::undistortPoints(inputPoint, outputPoint, cameraMatrix, distCoeffsMat, cv::Mat());
    else cv::fisheye::undistortPoints(inputPoint, outputPoint, cameraMatrix, distCoeffsMat(cv::Rect(0,0,1,4)).clone(), cv::Mat(), cv::Mat());
    //const double dRectifiedRow=outputPoint.at<cv::Vec2d>(0,0)[1];
    //const double dRectifiedCol=outputPoint.at<cv::Vec2d>(0,0)[0];

    //double fx=cameraMatrix_[uiCamera].at<double>(0,0);
    //double fy=cameraMatrix_[uiCamera].at<double>(1,1);
    //double cx=cameraMatrix_[uiCamera].at<double>(0,2);
    //double cy=cameraMatrix_[uiCamera].at<double>(1,2);

    //Ray is (x/fx,y/fy,1.0)

   // dLocalX=(dRectifiedCol - cx)/fx;
    //dLocalY=(dRectifiedRow - cy)/fy;
    dLocalX=outputPoint.at<cv::Vec2d>(0,0)[0];
    dLocalY=outputPoint.at<cv::Vec2d>(0,0)[1];

    dLocalZ=1.0;

    return 0;
}

void MultiCamCalibration::camera3dPointToMultiCam3dPoint(int camera,cv::Mat& PointLocal,cv::Mat& PointGlobal){
    
    PointGlobal= local2globalMatrix_[camera]*PointLocal;

}

void MultiCamCalibration::multiCam3DPoint2camera3dPoint(cv::Mat& PointGlobal,int camera,cv::Mat& PointLocal){
    
    PointLocal= global2localMatrix_[camera]*PointGlobal;

}

int MultiCamCalibration::loadCalibrationFile(std::string calibration_file){

    //-- Load calibation matrices.
    cv::FileStorage fs_;
    bool result_open;    
    result_open= fs_.open(calibration_file, cv::FileStorage::READ );
    if(result_open == false)
    {
        perror("Couldn't open calibration file");
        return(1);
    }
    else{
        printf("Calibration file found. :D\n");    
    }

    int numberCameras, imageWidth, imageHeight;
    bool fisheye_model;
    fs_["image_Width"] >>imageWidth;
    fs_["image_Height"] >>imageHeight;
    std::cout << "Dimensions of the images read succesfully: " << imageWidth << " , " << imageHeight <<std::endl;

    fs_["number_of_cameras"] >>numberCameras;
    std::cout << "Number of cameras read succesfully: " << numberCameras << std::endl;

    fs_["fisheye_model"] >>fisheye_model;
    std::cout << "Number of cameras read succesfully: " << numberCameras << std::endl;


    setImageWidth(imageWidth);
    setImageHeight(imageHeight);
    setNumberCameras(numberCameras);
    setModel(fisheye_model);


    //Read calibration parameters
    for(int i=0;i<numberCameras;i++){
        std::cout << "Reading parameters for camera " << i << std::endl;
        cv::Mat cameraMat, distCoeffs, extrinsics;
        std::ostringstream  parameter_name;
        parameter_name << "Camera_Matrix"  << i;
        fs_[parameter_name.str()] >>cameraMat;
        parameter_name.str("");
        parameter_name.clear();
        parameter_name << "Distortion_Coefficients"<< i;
        fs_[parameter_name.str()] >>distCoeffs;
        parameter_name.str("");
        parameter_name.clear();
        parameter_name << "Extrinsics" << i;
        fs_[parameter_name.str()] >>extrinsics;

        setCameraMatrix(i,cameraMat);
        setExtrinsics(i,extrinsics);
        setDistCoeffs(i,distCoeffs);
        std::cout << "Paramters read " << std::endl;
    }
    fs_.release();

    //Set all matrices values.
    computeAllMatrices();
    return 0;
}

//Other utilites

void extrinsics2HomogeneousMatrix( const double rotX, const double rotY, const double rotZ, const double transX, const double transY, const double transZ, cv::Mat& matrix )
{
    //matrix(4, 4, CV_64F);
    double cosX, sinX, cosY, sinY, cosZ, sinZ;

    cosX = cos( rotX );        sinX = sin( rotX );
    cosY = cos( rotY );        sinY = sin( rotY );
    cosZ = cos( rotZ );        sinZ = sin( rotZ );

    // cz*cy;
    matrix.at<double>(0,0) = cosZ * cosY; 
    // cz*sy*sx - sz*cx;
    matrix.at<double>(0,1) = cosZ * sinY * sinX - sinZ * cosX; 
    // cz*sy*cx + sz*sx;
    matrix.at<double>(0,2) = cosZ * sinY * cosX + sinZ * sinX; 

    // sz*cy;
    matrix.at<double>(1,0) = sinZ * cosY; 
    // sz*sy*sx + cz*cx;
    matrix.at<double>(1,1) = sinZ * sinY * sinX + cosZ * cosX; 
    // sz*sy*cx - cz*sx;
    matrix.at<double>(1,2) = sinZ * sinY * cosX - cosZ * sinX; 

    //-sy;
    matrix.at<double>(2,0) = -sinY; 
    //cy*sx;
    matrix.at<double>(2,1) = cosY * sinX; 
    //cy*cx;
    matrix.at<double>(2,2) = cosY * cosX; 

    // translation portion of transform
    matrix.at<double>(0,3)= transX;
    matrix.at<double>(1,3) = transY;
    matrix.at<double>(2,3) = transZ;

    // bottom row, always the same
    matrix.at<double>(3,0) = 0.0;
    matrix.at<double>(3,1) = 0.0;
    matrix.at<double>(3,2) = 0.0;
    matrix.at<double>(3,3) = 1.0;
}
void R2Euler(const cv::Mat R, double& RotX, double& RotY, double& RotZ){
    //Computes euler angles from Rotation Matrix
    if(R.at<double>(2,0) != -1 && R.at<double>(2,0) !=1){
        RotY=-asin(R.at<double>(2,0));
        RotX=atan2(R.at<double>(2,1)/cos(RotY),R.at<double>(2,2)/cos(RotY));
        RotZ=atan2(R.at<double>(1,0)/cos(RotY),R.at<double>(0,0)/cos(RotY));
    }
    else{
        RotZ=0; //Or anything
        if(R.at<double>(2,0)==-1){
            RotY=PI/2;
            RotX=RotZ+atan2(R.at<double>(0,1),R.at<double>(0,2));
        }
        else{
            RotY=-PI/2;
            RotX=-RotZ+atan2(-R.at<double>(0,1),-R.at<double>(0,2));
        }
    }
}

void euler2R(const double rotX, const double rotY, const double rotZ, cv::Mat& matrix ){
    double cosX, sinX, cosY, sinY, cosZ, sinZ;

    cosX = cos( rotX );        sinX = sin( rotX );
    cosY = cos( rotY );        sinY = sin( rotY );
    cosZ = cos( rotZ );        sinZ = sin( rotZ );
    
    matrix.at<double>(0,0) = cosZ * cosY; 
    // cz*sy*sx - sz*cx;
    matrix.at<double>(0,1) = cosZ * sinY * sinX - sinZ * cosX; 
    // cz*sy*cx + sz*sx;
    matrix.at<double>(0,2) = cosZ * sinY * cosX + sinZ * sinX; 

    // sz*cy;
    matrix.at<double>(1,0) = sinZ * cosY; 
    // sz*sy*sx + cz*cx;
    matrix.at<double>(1,1) = sinZ * sinY * sinX + cosZ * cosX; 
    // sz*sy*cx - cz*sx;
    matrix.at<double>(1,2) = sinZ * sinY * cosX - cosZ * sinX; 

    //-sy;
    matrix.at<double>(2,0) = -sinY; 
    //cy*sx;
    matrix.at<double>(2,1) = cosY * sinX; 
    //cy*cx;
    matrix.at<double>(2,2) = cosY * cosX; 
}

void homogeneousMatrix2extrinsics(const cv::Mat& matrix, double & rotX, double & rotY, double & rotZ, double & transX, double & transY, double & transZ){
    //Computes extrinsics from Transformation Matrix
    cv::Mat R=matrix(cv::Rect(0,0,3,3)).clone();
    R2Euler(R,rotX,rotY,rotZ);
    transX=matrix.at<double>(0,3);
    transY=matrix.at<double>(1,3);
    transZ=matrix.at<double>(2,3);
}

void homogeneousMatrixInverse(const cv::Mat& matrix, cv::Mat& inverseMatrix){
    //Rotation Range is [inclusive, exclusive).
    cv::Mat Rot=matrix(cv::Rect(0,0,3,3)).clone();
    cv::Mat Rot_t;
    cv::transpose(Rot,Rot_t);


    //Assign translated rot matrix
    cv::Mat tmp = inverseMatrix(cv::Rect(0,0,3,3));
    Rot_t.copyTo(tmp);
    

    //Translation
    cv::Mat Trans=matrix(cv::Rect(3,0,1,3)).clone();
    // translation portion of transform -R'*T
    cv::Mat Result= -Rot_t*Trans;
    inverseMatrix.at<double>(0,3)= Result.at<double>(0,0);
    inverseMatrix.at<double>(1,3) = Result.at<double>(1,0);
    inverseMatrix.at<double>(2,3) = Result.at<double>(2,0);

    // bottom row, always the same
    inverseMatrix.at<double>(3,0) = 0.0;
    inverseMatrix.at<double>(3,1) = 0.0;
    inverseMatrix.at<double>(3,2) = 0.0;
    inverseMatrix.at<double>(3,3) = 1.0;
}
