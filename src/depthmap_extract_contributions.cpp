#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

//LadybugCalibration class
#include "mcs_calibration.hpp"
#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 1

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s calibration_file final_depth_map first_camera last_camera last_frame \n",argv[0]);
}


bool isFloat( string myString ) {
    std::istringstream iss(myString);
    float f;
    iss >> noskipws >> f; // noskipws considers leading whitespace invalid
    // Check the entire string was consumed and if either failbit or badbit is set
    return iss.eof() && !iss.fail();
}

int main(int argc, char *argv[])
{

    if(argc <6)
    {
        printhelp(argv);
        return(1);
    }

    //-- Load calibation matrices.
    MultiCamCalibration calibration; //Create calibration object.
    string calibration_file=string(argv[1]);
    calibration.loadCalibrationFile(calibration_file);


    int first_camera=atoi(argv[3]);
    int last_camera=atoi(argv[4]);
    int last_frame=atoi(argv[5]);

    //Depthmap path
    string depthmap_path_part1,depthmap_path_part2;
    string depthmap_path=argv[2];
    int indexframe = depthmap_path.rfind("frame");
    string first_frame_str = depthmap_path.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    depthmap_path_part1 = depthmap_path.substr(0,indexframe+5);
    depthmap_path_part2 = depthmap_path.substr(indexframe+10,depthmap_path.length());

    //cout << depthmap_path_part1 << endl;
    //cout << depthmap_path_part2 << endl;

    for(int frame=first_frame;frame<=last_frame;frame++){
      cout << frame << endl;

      //READ SMOOTHED DEPTHMAP
      depthmap_path=depthmap_path_part1 + to_string(frame) + depthmap_path_part2;
      cv::Mat depthMap;
      depthMap=read_depthmap(depthmap_path);
      if(depthMap.data==NULL){
        cout << "Error loading depthmap" << endl;
      }

      cv::resize(depthMap, depthMap, cv::Size(2000,1000));


      int pano_height,pano_width;
      pano_height=depthMap.rows;
      pano_width=depthMap.cols;

      stringstream ss;

      //We need to generate a contribution map.
      cout << "Generating contribution map for depthmap" << endl;
      cv::Mat contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
      cv::Mat second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);

      int y;
      //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
      for(y = 0; y< pano_height; y++){
          int x;
          for(x = 0; x <pano_width ; x++) {

            double theta=PI-x*2*PI/pano_width;
            double phi=PI*y/pano_height;

            double dist=0;
            if(depthMap.at<double>(y,x)!=0){
                dist=depthMap.at<double>(y,x);
            }

            //From theta and phi find the 3D coordinates.
            double globalZ=dist*cos(phi);
            double globalX=dist*sin(phi)*cos(theta);
            double globalY=dist*sin(phi)*sin(theta);

            int result_;
            //From the 3D coordinates, find where the point falls in the individual image
            int cam;
            //This pixel is not in the contribution map. But check anyway if according to model there is somebody that can use it
            float minDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.
            float secondMinDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.

            int cameraMinDistanceCenter=-1;
            int secondCameraMinDistanceCenter=-1;
            int covered=0;
            int numberCameras=calibration.getNumberCameras();
            int imageWidth=calibration.getImageWidth();
            int imageHeight=calibration.getImageHeight();

            double dRow[last_camera];
            std::fill_n(dRow, numberCameras,-1);
            double dCol[last_camera];
            std::fill_n(dCol, numberCameras,-1);

            //From the 3D coordinates, find in how many camera falls the point!
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        secondMinDistanceCenter=minDistanceCenter;
                        secondCameraMinDistanceCenter=cameraMinDistanceCenter;

                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                    if (distanceCenter<secondMinDistanceCenter && distanceCenter!= minDistanceCenter){
                        secondMinDistanceCenter=distanceCenter;
                        secondCameraMinDistanceCenter=cam;
                    }
                }
            }
            if(covered>0){
              contribution_map.at<unsigned char>(y,x)=(unsigned char) cameraMinDistanceCenter;
              if(secondCameraMinDistanceCenter>-1){
                second_contribution_map.at<unsigned char>(y,x)=(unsigned char) secondCameraMinDistanceCenter;
              }
            }
          }
      }

      std::vector<int> qualityType;
      qualityType.push_back(IMWRITE_PNG_COMPRESSION);
      qualityType.push_back(9); //Jpeg quality

      ss.str("");
      ss << depthmap_path_part1 << setw(5) << frame << "_1st_contribution_map.png";
      string pano_filename=ss.str();
      bool writing_success=imwrite(pano_filename, contribution_map, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename << endl;
      }

      ss.str("");
      ss << depthmap_path_part1 << setw(5) << frame << "_2nd_contribution_map.png";
      pano_filename=ss.str();
      writing_success=imwrite(pano_filename, second_contribution_map, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename << endl;
      }
    }
  return 0;
}
