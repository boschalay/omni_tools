#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>


#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depthmap.exr\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc != 2)
    {
        printhelp(argv);
        return(1);
    }


    cout << "Depth map" << endl;
    string depth_map=string(argv[1]);
    string file_extension=depth_map.substr(depth_map.rfind(".")+1);

    cout << "File extension " << file_extension << endl;

    int width_depth_map;
    std::vector<float> depthmapdata_equi;
    cv::Mat depthMap;

    if(file_extension=="exr"){
      cv::Mat exrMat=cv::imread(depth_map,CV_LOAD_IMAGE_ANYDEPTH);
      Mat bgr[3];   //destination array
      split(exrMat,bgr);//split source
      depthMap=bgr[0].clone();
      width_depth_map=depthMap.cols;

    }
    else{
      cout << "Depth map extension not recognised." << endl;
      return 1;
    }
    depthMap.convertTo(depthMap,CV_64F);

    //===============================SAVING TXT===============================
    FILE * pFile;
    string filename;

    stringstream ss;
    ss.str("");
     ss << depth_map << ".txt";
    filename=ss.str();
    pFile = fopen (filename.c_str(),"w");

    for(int v = 0; v< width_depth_map/2; v++){
        int u;
        for(u = 0; u <width_depth_map ; u++) {
            fprintf (pFile, "%4.4lf ", depthMap.at<double>(v,u));
        }
        fprintf (pFile, "\n");
    }

    fclose (pFile);

    cout << "Saved " << filename << endl;

    return 0;
}
