#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <dirent.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
using namespace std;

void printMat(Mat mat, int prec)
{      
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl; 
        }
    }
}

int main(int argc, char *argv[])
{

	struct dirent *de=NULL;
	DIR *d=NULL;
	string path;

        bool fix_aspect_ratio = true;

	if(argc != 5)
	{
		fprintf(stderr,"Usage: %s dirname extension calibrationFile alhpa\n",argv[0]);
		return(1);
	}

	d=opendir(argv[1]);
	if(d == NULL)
	{
		perror("Couldn't open directory");
		return(1);
	}
	else{
		path=string(argv[1]);
		string::iterator it = path.end() - 1;
		if (*it == '/'){
		     path.erase(it);
		}
	}
	
	string extension=string(argv[2]);
	string calibrationFile=string(argv[3]);
	double alpha=atof(argv[4]);

	Mat image, imageRect;
        Size imageSize = image.size();

	//-- Load calibation matrices.
	FileStorage fs_;
	string calibration_file = string(calibrationFile);	
	cout << calibration_file << endl;

	bool result;	
	bool first_image=true;

	result= fs_.open(calibration_file, FileStorage::READ );
	if(result == false)
	{
		perror("Couldn't open calibration file");
		return(1);
	}
	else{
		printf("Calibration file found.\n");	
	}

	Mat map1, map2, cameraMat, distCoeffs;
	string fisheye_raw;
	bool fisheye=false;

	fs_["cameraMatrix"] >>cameraMat;
	fs_["distCoeffs"] >>distCoeffs;
	fs_["fisheye"] >> fisheye_raw;
	
	cout << "Fisheye: " << fisheye_raw << endl;
	if(fisheye_raw.compare("0")!=0){
		fisheye=true;
	}	


	//Create a loop with the list of images.
	vector <string> list;

	while(de = readdir(d)){
		if (de->d_type == DT_REG){		// if entry is a regular file
			string fname = string(de->d_name);	// filename
			// if filename's last characters are extension
			if (fname.find(extension, (fname.length() - extension.length())) != string::npos){
				string image_fn = string(string(path + "/" +fname));
				list.push_back( image_fn);
			}
		}
	}	
	closedir(d);
	sort( list.begin(), list.end() );

	cout << "Found " << list.size() << " images to rectify." << endl;


	for(vector<string>::size_type i = 0; i != list.size(); i++) {
		cout << list[i] << "\r" << flush;
		image = imread(list[i], IMREAD_COLOR);		
		//-- Remap
		if(first_image){
			//Do mapping for remap.
			imageSize = image.size();//new_imag_size – Image size after rectification. By default,it is set to imageSize .

			//validPixROI – Optional output rectangle that outlines all-good-pixels region in the undistorted image. See roi1, roi2 description in stereoRectify() .			
			Mat NewCameraMat;
			
			if(!fisheye){
                                NewCameraMat=getOptimalNewCameraMatrix(cameraMat,distCoeffs, imageSize ,alpha,imageSize,0,false);

                                if (fix_aspect_ratio)
                                {
                                    double f_new = max(NewCameraMat.at<double>(0,0),NewCameraMat.at<double>(1,1));
                                    NewCameraMat.at<double>(0,0) = f_new;
                                    NewCameraMat.at<double>(1,1) = f_new;
                                }
                                initUndistortRectifyMap(cameraMat, distCoeffs,cv::Mat(), NewCameraMat,imageSize, CV_16SC2, map1, map2);
			}
			else{
				cv::fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMat, distCoeffs, imageSize, cv::noArray(), NewCameraMat, alpha);
                //NewCameraMat.at<double>(0,0)=1010;
                //NewCameraMat.at<double>(1,1)=1010;
                //NewCameraMat.at<double>(0,2)=1351;
                //NewCameraMat.at<double>(1,2)=1031;

                NewCameraMat.at<double>(0,0)=665;
                NewCameraMat.at<double>(1,1)=665;
                NewCameraMat.at<double>(0,2)=808;
                NewCameraMat.at<double>(1,2)=616;


                
				fisheye::initUndistortRectifyMap(cameraMat, distCoeffs, Mat::eye(3, 3, CV_64F), NewCameraMat, imageSize, CV_16SC2, map1, map2);
			}
            //cout << map1 << endl;
			first_image=false;
            cout << "New camera mat: " << endl;
            cout << NewCameraMat << endl;

		}
		remap(image, imageRect, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0); //200=; blue
		//Save new rect image.
		int lastindex = list[i].find_last_of("."); 
		string rawname = list[i].substr(0, lastindex); 
        rawname.append("_rect.jpg");
        imwrite(rawname, imageRect);
	}
	return 0;
}
