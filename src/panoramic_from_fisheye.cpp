#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <math.h>
#include <omp.h> //Parallel

#define PI 3.14159265358979323846

using namespace cv; 
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{      
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl; 
        }
    }
}


int loadCalibrationFile(std::string calibration_file, int& width, int& height, cv::Mat& cameraMatrix, cv::Mat& distortion){

    //-- Load calibation matrices.
    cv::FileStorage fs_;
    bool result_open;    
    cout << calibration_file << endl;
    result_open= fs_.open(calibration_file, cv::FileStorage::READ );
    if(result_open == false)
    {
        perror("Couldn't open calibration file");
        return(1);
    }
    else{
        printf("Calibration file found.\n");    
    }

    fs_["image_width"] >>width;
    cout << width<< endl;
    fs_["image_height"] >>height;
    std::cout << "Dimensions of the images read succesfully." << std::endl;
    fs_["camera_matrix"] >>cameraMatrix;
    std::cout << "Intrisic parameters read succesfully." << std::endl;
    fs_["distortion_coefficients"] >>distortion;
    fs_.release();

    return 0;
}

void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width image calibration_file ",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc < 4)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    ///Split name of the file in four parts. PART0-#CAMERA-PART1-#FRAME-PART3
    string image_path=string(argv[2]);
    
    //-- Load calibation matrices.
    cout << "Reading calibration from file " << argv[3] << endl;
    int image_width, image_height;
    cv::Mat camera_matrix, dist_coeffs;
    loadCalibrationFile(argv[3],image_width, image_height, camera_matrix, dist_coeffs);

    //camera_matrix.at<double>(0,0)*=1.5;
    //camera_matrix.at<double>(1,1)*=1.5;

    cout << image_width << endl;
    cout << image_height << endl;
    cout << camera_matrix << endl;
    cout << dist_coeffs << endl;

    //Read the image
    
    Mat image=imread(image_path, IMREAD_COLOR);
    if(image.data==NULL){
        cout << "Error reading image: "<< image_path << endl;
        return 1;
    }
    
    
    cout << "Image opened succesfully. About to start panoramic image" << endl;

    /*
    //For every image assume the distortion will be pincushion like. Find the 3D position of the 4 corners for every image

    double R=calibration.getSphereRadius();
    int result;


    double theta_min[6]={0,0,0,0,0,0};
    double theta_max[6]={0,0,0,0,0,0};

    double phi_min[6]={0,0,0,0,0,0};
    double phi_max[6]={0,0,0,0,0,0};


    for(int cam = 0; cam!= 6; cam++){

        double globalX[9]={0,0,0,0,0,0,0,0,0};
        double globalY[9]={0,0,0,0,0,0,0,0,0};
        double globalZ[9]={0,0,0,0,0,0,0,0,0};
        double theta[9]={0,0,0,0,0,0,0,0,0};
        double phi[9]={0,0,0,0,0,0,0,0,0};
        
        //TODO: Add middlepoints here
        cv::Mat K;
        calibration.getFundamentalMatrix(cam,K);
        double cx, cy;
        cx=K.at<double>(0,2);
        cy=K.at<double>(1,2);
        result=calibration.RCtoLadybugXYZ(0, 0, cam, globalX[0], globalY[0], globalZ[0],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(0, cx, cam, globalX[1], globalY[1], globalZ[1],fisheye_dist);        
        result=calibration.RCtoLadybugXYZ(0, calibration.getImageWidth()-1, cam, globalX[2], globalY[2], globalZ[2],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(cy, calibration.getImageWidth()-1, cam, globalX[3], globalY[3], globalZ[3],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(calibration.getImageHeight()-1, calibration.getImageWidth()-1, cam, globalX[4], globalY[4], globalZ[4],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(calibration.getImageHeight()-1, cx, cam, globalX[5], globalY[5], globalZ[5],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(calibration.getImageHeight()-1, 0, cam, globalX[6], globalY[6], globalZ[6],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(cy, 0, cam, globalX[7], globalY[7], globalZ[7],fisheye_dist);
        result=calibration.RCtoLadybugXYZ(cy, cx, cam, globalX[8], globalY[8], globalZ[8],fisheye_dist); //Center of the image

        //Transform them to spherical coordinates.
        //r =sqrt(x^2+y^2+z^2)
        // theta= arcos(z/r)
        // phi=arctan(y/x)
        for(int i=0; i<9; i++){
            theta[i]=acos(globalZ[i]/R); // From 0 to Pi.
            phi[i]=atan2(globalY[i],globalX[i]); //From 0 to 2Pi.
        }
        
        if(cam<5){
            theta_min[cam]=*std::min_element(theta,theta+8);
            theta_max[cam]=*std::max_element(theta,theta+8);

            //Find max,min phi and theta for each camera
            phi_min[cam]=phi[8];
            phi_max[cam]=phi[8];
            for(int i=0; i<8; i++){
                if (isBiggerAngle(phi[i],phi[8]) && isBiggerAngle(phi[i],phi_max[cam])){
                    phi_max[cam]=phi[i];
                }
                else if(isBiggerAngle(phi[8],phi[i]) && isBiggerAngle(phi_min[cam],phi[i])){
                    phi_min[cam]=phi[i];
                }
            }
        }
        else{
            theta_min[cam]=0;
            theta_max[cam]=*std::max_element(theta,theta+8);
            phi_max[cam]=2*PI;
            phi_min[cam]=0; //Doesn't have sense because it's all.                
        }
        phi_max[cam]=2*PI;
        phi_min[cam]=-2*PI; //Doesn't have sense because it's all.             
    }

    printf("Camera 0: Phi min: %f, Phi max: %f\n",degrees(phi_min[0]),degrees(phi_max[0]));
    printf("Camera 1: Phi min: %f, Phi max: %f\n",degrees(phi_min[1]),degrees(phi_max[1]));
    printf("Camera 2: Phi min: %f, Phi max: %f\n",degrees(phi_min[2]),degrees(phi_max[2]));
    printf("Camera 3: Phi min: %f, Phi max: %f\n",degrees(phi_min[3]),degrees(phi_max[3]));
    printf("Camera 4: Phi min: %f, Phi max: %f\n",degrees(phi_min[4]),degrees(phi_max[4]));

    printf("Camera 0: Theta min: %f, Theta max: %f\n",degrees(theta_min[0]),degrees(theta_max[0]));
    printf("Camera 1: Theta min: %f, Theta max: %f\n",degrees(theta_min[1]),degrees(theta_max[1]));
    printf("Camera 2: Theta min: %f, Theta max: %f\n",degrees(theta_min[2]),degrees(theta_max[2]));
    printf("Camera 3: Theta min: %f, Theta max: %f\n",degrees(theta_min[3]),degrees(theta_max[3]));
    printf("Camera 4: Theta min: %f, Theta max: %f\n",degrees(theta_min[4]),degrees(theta_max[4]));
    printf("Camera 5: Theta min: %f, Theta max: %f\n",degrees(theta_min[5]),degrees(theta_max[5]));

    */
    //Panoramic image
    cv::Mat panoramic;
    panoramic=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);

    //Maps for ffmpeg
    cv::Mat maps[2];
    for(int j=0; j < 2; j++){
        maps[j]= cv::Mat(pano_height,pano_width,CV_16U,0.0); //Unsigned integer
    }


    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;

    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height,projection_type,R, underwater,imageHeight,imageWidth,image,mask, phi_min, phi_max,theta_min, theta_max, fisheye_dist, calibration) 
    for(y = 0; y< pano_height; y++){
        double dRow=-1;
        double dCol=-1;
        int x;
        for(x = 0; x <pano_width ; x++) {

            double globalX;
            double globalY;
            double globalZ;
                                               
            //Find 3D position depending on the projection
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi

            double theta=-(2*PI*x/(pano_width-1));
            double phi=PI*y/(pano_height-1); //Invert

            /*if (underwater){
                //theta=-theta;// To rotate 180: -PI;
                //phi=PI*(pano_height-1-y)/(pano_height-1);                
            }
            */
            double R=1;
            //From theta and phi find the 3D coordinates.
            //globalZ=R*cos(phi);
            //globalX=R*sin(phi)*cos(theta);


            globalX=R*cos(phi);
            globalZ=-R*sin(phi)*cos(theta);
            globalY=R*sin(phi)*sin(theta);

            //From the 3D coordinates, find where this point falls in the image
            std::vector<cv::Point3f> points3d;
            points3d.push_back(cv::Point3f(globalX, globalY, globalZ));
            std::vector<cv::Point2f> imagePoints;
            //if(globalZ>=0){
                cv::fisheye::projectPoints	(points3d, imagePoints, cv::Affine3d(), camera_matrix, dist_coeffs);
                float col=imagePoints[0].x;
                float row=imagePoints[0].y;
                
                //cout << col << " , " << row << endl;
                if (col>=0 && col<image_width && row>=0 && row<image_height) {
                    cv::Vec3b intensity =image.at<cv::Vec3b>(row,col);
                    panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) intensity.val[0];
                    panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) intensity.val[1];
                    panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) intensity.val[2];
                    maps[0].at<unsigned short>(y,x)= row;
                    maps[1].at<unsigned short>(y,x)= col;
                }
            //}
        }
        done[y]=true;
        
        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;

        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;
                
    }
    
    //Display
    cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Display window", panoramic );
    cv::waitKey();
    
    std::stringstream ss;
    string pano_filename;
    ss.str("");
    pano_filename.clear();
    ss << image_path.substr(0, image_path.find(".", 0)) << "_equi.png";
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    cout << pano_filename << endl;    

    bool writing_success;
    writing_success=imwrite(pano_filename, panoramic, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    
    
    for(int j=0; j < 2; j++){
        ss.str("");
        pano_filename.clear();
        string letter;
        if(j==0) letter="y"; else letter="x";
        ss << image_path.substr(0, image_path.find_last_of("/\\")) << "/map_"<< letter << ".pgm";
        pano_filename=ss.str();

        cout << pano_filename << endl;    

        std::vector<int> params;
        params.push_back(CV_IMWRITE_PXM_BINARY);
        params.push_back(1); // 1 for binary format, 0 for ascii format

        bool writing_success;
        writing_success=imwrite(pano_filename, maps[j], params);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
            return 1;
        }
    }
    
    cout << "Success!" << endl;

    return 0;
}
