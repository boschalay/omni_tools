#ifndef __MCS_CALIBRATION_H__
#define __MCS_CALIBRATION_H__

#include <math.h>
#include <string>

#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>


#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>

#define PI 3.14159265358979323846


// Constructor
class MultiCamCalibration{
    private:
    
        int nCameras;
        std::vector<cv::Mat> cameraMatrix_;
        std::vector<cv::Mat> extrinsics_;
        std::vector<cv::Mat> distCoeffs_;

        std::vector<cv::Mat> local2globalMatrix_;
        std::vector<cv::Mat> global2localMatrix_;

        double sphereRadius_; //20 by default.

        int imageWidth_; //1616 by def
        int imageHeight_; //1232 by def
        int numberCameras_;
        bool fisheyeModel_;

    public:
        //Const and dest.
        MultiCamCalibration();
        ~MultiCamCalibration();
        
        void setSphereRadius(double radius);
        void setNumberCameras(int numberCameras);
        void setImageWidth(int width);
        void setImageHeight(int height);
        void setModel(bool fisheye_model);
        double getSphereRadius() const;
        int getImageWidth() const;
        int getImageHeight() const;
        int getNumberCameras() const;
        void setExtrinsics(int camera, cv::Mat extrinsicsMat);
        void setDistCoeffs(int camera, cv::Mat distCoeffsMat);
        void getDistCoeffs(int camera, cv::Mat& distCoeffsMat);
        void setCameraMatrix(int camera, cv::Mat matrix);
        void getCameraMatrix(int camera, cv::Mat& matrix) const;
        void computeAllMatrices();
        void getCameraTransformationMatrix(int camera, cv::Mat& matrix)const;
        void getCameraInverseTransformationMatrix(int camera, cv::Mat& matrix);
        int MultiCamXYZtoRC(double dMultiCamX, double dMultiCamY, double dMultiCamZ, unsigned int uiCamera, double& dRow, double& dCol) const;
        int cameraXYZtoRC(double dcameraX, double dcameraY, double dcameraZ, unsigned int uiCamera, double& dRow, double& dCol) const;
        int RCtoMultiCamXYZ(double dRow, double dCol, unsigned int uiCamera, double& dMultiCamX, double& dMultiCamY, double& dMultiCamZ) const;
        int RCtoCameraVector(double dRow, double dCol, unsigned int uiCamera, double& dLocalX, double& dLocalY, double& dLocalZ) const;
        void camera3dPointToMultiCam3dPoint(int camera,cv::Mat& PointLocal,cv::Mat& PointGlobal);
        void multiCam3DPoint2camera3dPoint(cv::Mat& PointGlobal,int camera,cv::Mat& PointLocal);
        int loadCalibrationFile(std::string calibration_file);
};

void extrinsics2HomogeneousMatrix( const double rotX, const double rotY, const double rotZ, const double transX, const double transY, const double transZ, cv::Mat& matrix );
void R2Euler(const cv::Mat R, double& RotX, double& RotY, double& RotZ);
void euler2R(const double rotX, const double rotY, const double rotZ, cv::Mat& matrix );
void homogeneousMatrix2extrinsics(const cv::Mat& matrix, double & rotX, double & rotY, double & rotZ, double & transX, double & transY, double & transZ);
void homogeneousMatrixInverse(const cv::Mat& matrix, cv::Mat& inverseMatrix);

#endif
