#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>


using namespace std;
using namespace cv;

#define SHOW_DEBUG_IMAGES 0

#define PI 3.14159265358979323846

bool read_depth( string depth_file, int& width, int& height, std::vector<float>&  data)
{

    cout << "TXT FILE" << endl;
    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    height=0;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
        height++;
    }
    return true;
}

bool read_depth_binary( string depth_file, int& width, int& height, std::vector<float>&  data)
{

    cout << "BIN FILE" << endl;

    std::ifstream fin(depth_file.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cout << " Error, Couldn't find the file" << "\n";
        return false;
    }

    fin.seekg(0, std::ios::end);
    const size_t num_elements = fin.tellg() / sizeof(float);
    fin.seekg(0, std::ios::beg);

    float w,h;

    fin.read(reinterpret_cast<char*>(&w), sizeof(float));
    fin.read(reinterpret_cast<char*>(&h), sizeof(float));

    width=(int)round(w);
    height=(int)round(h);

    cout << "Depth map Width:" << width << endl;
    cout << "Depth map Height:" << h << endl;

    data.resize(num_elements-2);
    fin.read(reinterpret_cast<char*>(&data[0]), (num_elements-2)*sizeof(float));

    return true;
}

cv::Mat read_depthmap( string file_path){
  //Return CV_64F depthmap
  string file_extension=file_path.substr(file_path.rfind(".")+1);

  cout << "File extension " << file_extension << endl;

  int height_depth_map,width_depth_map;
  std::vector<float> depthmapdata;
  cv::Mat depthMap;

  if(file_extension=="txt"){
    if(read_depth(file_path,width_depth_map, height_depth_map, depthmapdata)){
        depthMap=cv::Mat::zeros(height_depth_map,width_depth_map,CV_32F);
        memcpy(depthMap.data, depthmapdata.data(), depthmapdata.size()*sizeof(float));
    }
  }
  else if(file_extension=="bin"){
      if(read_depth_binary(file_path,width_depth_map, height_depth_map, depthmapdata)){
          depthMap=cv::Mat::zeros(height_depth_map,width_depth_map,CV_32F);
          memcpy(depthMap.data, depthmapdata.data(), depthmapdata.size()*sizeof(float));
      }
  }else if(file_extension=="exr"){
    cv::Mat exrMat=cv::imread(file_path,CV_LOAD_IMAGE_ANYDEPTH);
    Mat bgr[3];   //destination array
    split(exrMat,bgr);//split source
    depthMap=bgr[0].clone();
  }
  else{
    cout << "Depth map extension not recognised." << endl;
    return cv::Mat();
  }

  depthMap.convertTo(depthMap,CV_64F);
  return depthMap;
}



cv::Mat interpolate3(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int patch_width=11){
  //PATCH WIDTH ODD

  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  double angular_increment_per_pixel=2*PI/(double)imageWidth;

  //1-First a very basic and fast-rough interpolation
  cv::Mat temp=originalImage.clone();
  cv::Mat previous=temp.clone();
  //cv::Mat distanceInterpolated=cv::Mat
  cv::Mat toInterpolateOriginal=toInterpolate.clone();

  std::vector<cv::Point> toInterpolateIndexes;
  cv::findNonZero(toInterpolate,toInterpolateIndexes);

  while(toInterpolateIndexes.size()>0){
      int changed =0;
      std::random_shuffle ( toInterpolateIndexes.begin(), toInterpolateIndexes.end() );
      for(int i=toInterpolateIndexes.size()-1;i>=0;i--){
          int col=toInterpolateIndexes[i].x;
          int row=toInterpolateIndexes[i].y;
          cv::Mat new_image=cv::Mat::zeros(patch_width,patch_width,CV_64F);

          //Compute theta and phi
          double theta=PI-col*2*PI/imageWidth;
          double phi=row*PI/imageHeight;

          //Create rotation matrix.
          //We first rotate randomly the first vector (the only one known by 90 and then vectorial product)
          cv::Mat rot=cv::Mat::zeros(3,3,CV_64F);
          rot.at<double>(0,0)=sin(phi)*cos(theta);
          rot.at<double>(1,0)=sin(phi)*sin(theta);
          rot.at<double>(2,0)=cos(phi);
          rot.at<double>(0,1)=0;
          rot.at<double>(1,1)=cos(phi)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
          rot.at<double>(2,1)=-sin(phi)*sin(theta)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
          rot.at<double>(0,2)=rot.at<double>(1,0)*rot.at<double>(2,1)-rot.at<double>(1,1)*rot.at<double>(2,0);
          rot.at<double>(1,2)=-(rot.at<double>(0,0)*rot.at<double>(2,1)-rot.at<double>(0,1)*rot.at<double>(2,0));
          rot.at<double>(2,2)=rot.at<double>(0,0)*rot.at<double>(1,1)-rot.at<double>(0,1)*rot.at<double>(1,0);

          //Compute theta and phi
          for(int v=0;v<new_image.rows;v++){
            for(int u=0;u<new_image.cols;u++){
              //Compute 3d point/vector
              double new_theta=(new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
              double new_phi=PI/2 - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;

              if(new_phi<=PI){ //Useful when doing a whole equi panorama
                double new_x=sin(new_phi)*cos(new_theta);
                double new_y=sin(new_phi)*sin(new_theta);
                double new_z=cos(new_phi);

                //We need to convert from new to old coordinates
                cv::Mat new_point=cv::Mat(3,1,CV_64F);
                new_point.at<double>(0,0)=new_x;
                new_point.at<double>(1,0)=new_y;
                new_point.at<double>(2,0)=new_z;

                cv::Mat old_point=rot*new_point;
                double old_theta=atan2(old_point.at<double>(1,0),old_point.at<double>(0,0));
                double old_phi=acos(old_point.at<double>(2,0));
                if(old_point.at<double>(2,0)>=1) old_phi=PI/2;

                int old_u=roundf((PI-old_theta)/2/PI*imageWidth);
                int old_v=roundf(old_phi/PI*imageHeight);

                new_image.at<double>(v,u)=previous.at<double>(old_v, old_u);
              }
            }
          }
          if(countNonZero(new_image)!=0){
            temp.at<double>(row,col)=sum(new_image).val[0]/countNonZero(new_image);
            toInterpolate.at<unsigned char>(row,col)=(unsigned char) 0;
            changed ++;
          }
      }
      cout << "Changed " << changed << endl;
      cv::findNonZero(toInterpolate,toInterpolateIndexes);
      previous=temp.clone();

      double min,max;
      cv::minMaxLoc( temp, &min, &max);
      //cv::Mat adjMap;
      cv::Mat colorDepthMap;
      cv::convertScaleAbs( temp, colorDepthMap, 255 / max);
      applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

      cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
      cv::imshow("Depth Map",colorDepthMap);
      cv::waitKey();


      //cout << "To interpolate "  << toInterpolateIndexes.size() << endl;
  }
  //cout << "Finished interpolation" << endl;

return temp;
}



cv::Mat smooth5(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, cv::Mat iteration, int patch_width=11){
  //patch_width ODD
  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  if(iteration.data==NULL){
    iteration=cv::Mat::ones(originalImage.rows,originalImage.cols,CV_64F);
    cout << "No iteration info" << endl;
  }
  //2- smoothing
  double angular_increment_per_pixel=2*PI/(double)imageWidth;

  cv::Mat temp=originalImage.clone();
  std::vector<cv::Point> toInterpolateIndexes;
  cv::findNonZero(toInterpolate,toInterpolateIndexes);

  for(int i=toInterpolateIndexes.size()-1;i>=0;i--){

      int col=toInterpolateIndexes[i].x;
      int row=toInterpolateIndexes[i].y;

      //Compute theta and phi
      double theta=PI-col*2*PI/imageWidth;
      double phi=row*PI/imageHeight;

      double iter=round(iteration.at<double>(row,col)/2)+1;
      cv::Mat new_image=cv::Mat::zeros(patch_width*iter,patch_width*iter,CV_64F);

      for(int v=0;v<new_image.rows;v++){
        for(int u=0;u<new_image.cols;u++){
          //Compute 3d point/vector
          double new_theta=theta + (new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
          double new_phi=phi - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;
          if(new_theta<-PI) theta= 2*PI+theta;
          else if(new_theta>PI) theta= -2*PI+ theta;

          //cout << new_phi << endl;
          if(new_phi<=PI && new_phi>=0){ //Useful when doing a whole equi panorama
            int new_u=roundf((PI-new_theta)/2/PI*imageWidth);
            int new_v=roundf(new_phi/PI*imageHeight);
            if((new_u<0 || new_v<0 || new_u>=(imageWidth) || new_v>=(imageHeight))){
            }
            else{
              new_image.at<double>(v,u)=originalImage.at<double>(new_v, new_u);
            }
          }
      }
    }
    if(countNonZero(new_image)>0){
      temp.at<double>(row,col)=sum(new_image).val[0]/(double)countNonZero(new_image);
    }
  }

  return temp;

}

cv::Mat smooth3(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, cv::Mat iteration, int patch_width=11){
  //patch_width ODD
  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  if(iteration.data==NULL){
    iteration=cv::Mat::ones(originalImage.rows,originalImage.cols,CV_64F);
    cout << "No iteration info" << endl;
  }
  //2- smoothing
  double angular_increment_per_pixel=2*PI/(double)imageWidth;

  cv::Mat temp=originalImage.clone();
  std::vector<cv::Point> toInterpolateIndexes;
  cv::findNonZero(toInterpolate,toInterpolateIndexes);

  for(int i=toInterpolateIndexes.size()-1;i>=0;i--){

      int col=toInterpolateIndexes[i].x;
      int row=toInterpolateIndexes[i].y;

      //Compute theta and phi
      double theta=PI-col*2*PI/imageWidth;
      double phi=row*PI/imageHeight;

      int halfsize=(patch_width-1)/2;
      cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
      cv::Point bottom_right=cv::Point(std::min(col+halfsize,imageWidth-1),std::min(row+halfsize,imageHeight-1));
      cv::Mat roi_iter=iteration(cv::Rect(top_left,bottom_right));
      double min, max;
      cv::Scalar mean=cv::mean(roi_iter);
      cv::minMaxLoc(roi_iter, &min, &max);
      cv::Mat new_image=cv::Mat::zeros(patch_width*(mean.val[0]),patch_width*(mean.val[0]),CV_64F);

      for(int v=0;v<new_image.rows;v++){
        for(int u=0;u<new_image.cols;u++){
          //Compute 3d point/vector
          double new_theta=theta + (new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
          double new_phi=phi - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;
          if(new_theta<-PI) theta= 2*PI+theta;
          else if(new_theta>PI) theta= -2*PI+ theta;

          //cout << new_phi << endl;
          if(new_phi<=PI && new_phi>=0){ //Useful when doing a whole equi panorama
            int new_u=roundf((PI-new_theta)/2/PI*imageWidth);
            int new_v=roundf(new_phi/PI*imageHeight);
            if((new_u<0 || new_v<0 || new_u>=(imageWidth) || new_v>=(imageHeight))){
            }
            else{
              new_image.at<double>(v,u)=originalImage.at<double>(new_v, new_u);
            }
          }
      }
    }
    if(countNonZero(new_image)>0){
      temp.at<double>(row,col)=sum(new_image).val[0]/(double)countNonZero(new_image);
    }
  }

  return temp;

}

cv::Mat interpolate4(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int patch_width=11){
  //PATCH WIDTH ODD

  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  double angular_increment_per_pixel=2*PI/(double)imageWidth;

  //1-First a very basic and fast-rough interpolation
  cv::Mat temp=originalImage.clone();
  cv::Mat previous=temp.clone();
  //cv::Mat distanceInterpolated=cv::Mat
  cv::Mat toInterpolateOriginal=toInterpolate.clone();
  cv::Mat iteration=cv::Mat::zeros(imageHeight,imageWidth,CV_64F);
  iteration.setTo(10000,toInterpolate!=0);



  //cv::Mat adjMap;
  cv::Mat colorDepthMap;
  double min,max;
  /*
  cv::minMaxLoc( iteration, &min, &max);
  //cv::Mat adjMap;
  cv::convertScaleAbs( iteration, colorDepthMap, 255 / max);
  applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

  cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
  cv::imshow("Depth Map", iteration);
  cv::waitKey();
  */



  std::vector<cv::Point> toInterpolateIndexes;
  cv::findNonZero(toInterpolate,toInterpolateIndexes);
  int loop=0;

  while(toInterpolateIndexes.size()>0){
      int changed =0;
      //std::random_shuffle ( toInterpolateIndexes.begin(), toInterpolateIndexes.end() );
      for(int i=toInterpolateIndexes.size()-1;i>=0;i--){
          int col=toInterpolateIndexes[i].x;
          int row=toInterpolateIndexes[i].y;
          if(temp.at<double>(row,col)==0){
            int halfsize=(patch_width-1)/2;
            cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
            cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
            cv::Mat roi_iter=iteration(cv::Rect(top_left,bottom_right));
            double min, max;
            cv::minMaxLoc(roi_iter, &min, &max);
            if(min<10000){ //This is because in sifts interpolation there are very few samples
            cv::Mat new_image=cv::Mat::zeros(patch_width*(min+1),patch_width*(min+1),CV_64F);

            //Compute theta and phi
            double theta=PI-col*2*PI/imageWidth;
            double phi=row*PI/imageHeight;

            //Create rotation matrix.
            //We first rotate randomly the first vector (the only one known by 90 and then vectorial product)
            cv::Mat rot=cv::Mat::zeros(3,3,CV_64F);
            rot.at<double>(0,0)=sin(phi)*cos(theta);
            rot.at<double>(1,0)=sin(phi)*sin(theta);
            rot.at<double>(2,0)=cos(phi);
            rot.at<double>(0,1)=0;
            rot.at<double>(1,1)=cos(phi)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
            rot.at<double>(2,1)=-sin(phi)*sin(theta)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
            rot.at<double>(0,2)=rot.at<double>(1,0)*rot.at<double>(2,1)-rot.at<double>(1,1)*rot.at<double>(2,0);
            rot.at<double>(1,2)=-(rot.at<double>(0,0)*rot.at<double>(2,1)-rot.at<double>(0,1)*rot.at<double>(2,0));
            rot.at<double>(2,2)=rot.at<double>(0,0)*rot.at<double>(1,1)-rot.at<double>(0,1)*rot.at<double>(1,0);

            //Compute theta and phi
            for(int v=0;v<new_image.rows;v++){
              for(int u=0;u<new_image.cols;u++){
                //Compute 3d point/vector
                double new_theta=(new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
                double new_phi=PI/2 - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;

                if(new_phi<=PI){ //Useful when doing a whole equi panorama
                  double new_x=sin(new_phi)*cos(new_theta);
                  double new_y=sin(new_phi)*sin(new_theta);
                  double new_z=cos(new_phi);

                  //We need to convert from new to old coordinates
                  cv::Mat new_point=cv::Mat(3,1,CV_64F);
                  new_point.at<double>(0,0)=new_x;
                  new_point.at<double>(1,0)=new_y;
                  new_point.at<double>(2,0)=new_z;

                  cv::Mat old_point=rot*new_point;
                  double old_theta=atan2(old_point.at<double>(1,0),old_point.at<double>(0,0));
                  double old_phi=acos(old_point.at<double>(2,0));
                  if(old_point.at<double>(2,0)>=1) old_phi=PI/2;

                  int old_u=roundf((PI-old_theta)/2/PI*imageWidth);
                  int old_v=roundf(old_phi/PI*imageHeight);

                  if((old_u<0 || old_v<0 || old_u>=(imageWidth) || old_v>=(imageHeight))){
                  }
                  else{
                    if(iteration.at<double>(old_v, old_u)<(loop+1) || loop>0){
                      new_image.at<double>(v,u)=temp.at<double>(old_v, old_u);
                    }
                  }
                }
              }
            }
            if(countNonZero(new_image)!=0){
              //temp.at<double>(row,col)=sum(new_image).val[0]/countNonZero(new_image);
              //Open window and put the same value to all

              int halfsize=(new_image.cols-1)/2;
              cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
              cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
              cv::Mat roi=temp(cv::Rect(top_left,bottom_right));
              cv::Mat roi_interp=toInterpolate(cv::Rect(top_left,bottom_right));
              cv::Mat roi_iter=iteration(cv::Rect(top_left,bottom_right));
              cv::Mat mask=roi==0;

              roi.setTo(sum(new_image).val[0]/countNonZero(new_image),mask);
              roi_interp.setTo((unsigned char) 0);
              changed ++;

              double min,max;
              //Min and max of max_num_iteration
              cv::minMaxLoc(roi_iter, &min, &max);
              roi_iter.setTo(min+1,mask);
              //cv::Mat adjMap;
              /*
              cv::Mat colorDepthMap;
              cv::minMaxLoc( temp, &min, &max);
              //cv::Mat adjMap;
              cv::convertScaleAbs( temp, colorDepthMap, 255 / max);
              applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

              cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
              cv::imshow("Depth Map",colorDepthMap);
              cv::waitKey(10);
              */
            }
            }
          }
      }
      loop++;
      cout << "Changed " << changed << endl;
      cv::findNonZero(toInterpolate,toInterpolateIndexes);
      //previous=temp.clone();
      //cout << "To interpolate "  << toInterpolateIndexes.size() << endl;
  }

  temp=smooth3(temp,validPixels,cv::Mat::ones(imageHeight,imageWidth,CV_8U)*255,iteration,31);
return temp;
}

cv::Mat interpolate5(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int patch_width=11){
  //PATCH WIDTH ODD

  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  double angular_increment_per_pixel=2*PI/(double)imageWidth;

  //1-First a very basic and fast-rough interpolation
  cv::Mat temp=originalImage.clone();
  cv::Mat previous=temp.clone();
  //cv::Mat distanceInterpolated=cv::Mat
  cv::Mat toInterpolateOriginal=toInterpolate.clone();
  //We only need to keep pixels wichi neighbours are not known.

  //For each pixel of the contribution map we open a window and see if there is any value different than it.
  for(int col=0;col<toInterpolate.cols;col++){
      for(int row=0;row<toInterpolate.rows;row++){

          int halfsize=1; //Should be even.
          cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
          cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
          cv::Mat image_roi=toInterpolate(cv::Rect(top_left,bottom_right));

          std::vector<cv::Point> nonZeroCoordinates;
          cv::findNonZero(image_roi, nonZeroCoordinates);
          if(nonZeroCoordinates.size()==9){
              toInterpolate.at<unsigned char>(row,col)=(unsigned char)0;
          }
      }
  }



  cv::Mat angular_distance=cv::Mat::ones(imageHeight,imageWidth,CV_64F)*-1.0;
  angular_distance.setTo(0,toInterpolateOriginal==0);
  cv::Mat last_distance=cv::Mat::zeros(imageHeight,imageWidth,CV_64F);

  cv::Mat colorDepthMap;
  double min,max;

  std::vector<cv::Point> toInterpolateIndexes;
  std::vector<cv::Point> invalidPixels;
  cv::findNonZero(angular_distance==0,toInterpolateIndexes);
  cv::findNonZero(angular_distance==-1,invalidPixels);
  int loop=1;

  while(invalidPixels.size()>0){
      //cout << "Iteration points" << toInterpolateIndexes.size()  << endl;
      //cout << "Invalid pixels" << invalidPixels.size()  << endl;
    //  cout << "Loop: " << loop << endl;
      double last_angular_threshold=angular_increment_per_pixel*pow(loop-1,2);
      double angular_threshold=angular_increment_per_pixel*pow(loop,2);
      int changed =0;
      std::random_shuffle ( toInterpolateIndexes.begin(), toInterpolateIndexes.end() );
      for(int i=toInterpolateIndexes.size()-1;i>=0;i--){

          int col=toInterpolateIndexes[i].x;
          int row=toInterpolateIndexes[i].y;
          //Compute theta and phi
          double theta=PI-col*2*PI/imageWidth;
          double phi=row*PI/imageHeight;


          bool finished=false;
          while(!finished){
            finished=true;
            patch_width=1+loop*2;
            patch_width=std::max(patch_width,(int) last_distance.at<double>(row,col)+2);
            last_distance.at<double>(row,col)=patch_width;
            cv::Mat new_image=cv::Mat::zeros(patch_width,patch_width,CV_64F);

              for(int v=0;v<new_image.rows;v++){
                for(int u=0;u<new_image.cols;u++){
                  //Compute 3d point/vector
                  int new_col=col -(new_image.cols-1)/2+u;
                  int new_row=row -(new_image.rows-1)/2+v;
                  double previous_value=angular_distance.at<double>(new_row,new_col);
                  //if(new_col>=0 && new_row>=0 && new_col<imageWidth && new_row <imageHeight && (previous_value==-1 || previous_value>last_angular_threshold)) {
                  if(new_col>=0 && new_row>=0 && new_col<imageWidth && new_row <imageHeight && (previous_value==-1)) {

                    double new_theta=PI-new_col*2*PI/imageWidth;
                    double new_phi=new_row*PI/imageHeight;

                    //Compute angle between two pixels in the same row
                    cv::Vec3d v1(sin(phi)*cos(theta),sin(phi)*sin(theta),cos(phi));
                    cv::Vec3d v2(sin(new_phi)*cos(new_theta),sin(new_phi)*sin(new_theta),cos(new_phi));
                    double angle_diff=acos(v1.dot(v2)/(cv::norm(v1)*cv::norm(v2)));

                    //if( angle_diff<=angular_threshold && (angle_diff < previous_value || previous_value==-1)){
                    if( angle_diff<=angular_threshold ){
                      angular_distance.at<double>(new_row,new_col) = angle_diff;
                      temp.at<double>(new_row,new_col)=originalImage.at<double>(row,col);
                      finished=false;
                    }
                  }
                }
              }
          }
      }
      cout << "Finished loop " << loop << endl;

      cv::findNonZero(angular_distance==-1,invalidPixels);
      loop++;

  }

  /*
  cv::minMaxLoc(temp, &min, &max);
  cv::Mat mask;
  cv::convertScaleAbs( temp, colorDepthMap, 255 / max);
  applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
  mask=angular_distance==-1;
  colorDepthMap.setTo(Scalar(0,0,0), mask);


  cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
  cv::imshow("Depth Map",colorDepthMap);
  cv::waitKey();
  */

  //Now for every pixel open a patch proportional to distance
  //Instead of angular distances it can be loop, and works perfectly fine.
  cout << "Smoothing" << endl;
  //temp=smooth5(temp,validPixels,cv::Mat::ones(imageHeight,imageWidth,CV_8U)*255,angular_distance.mul(angular_distance)*30,3);
  temp=smooth5(temp,validPixels,toInterpolateOriginal,angular_distance*70,3);
  return temp;
}



cv::Mat regionfill3(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int pano_width, cv::Mat weight_values, cv::Mat& before_blur){
  int imageWidth=originalImage.cols;
  int imageHeight=originalImage.rows;
  double angular_increment_per_pixel=2*PI/(double)imageWidth;


  //1-First a very basic and fast-rough interpolation
  cv::Mat temp=originalImage.clone();

  //cv::Mat distanceInterpolated=cv::Mat
  cv::Mat toInterpolateOriginal=toInterpolate.clone();

  std::vector<cv::Point> toInterpolateIndexes;
  cv::findNonZero(toInterpolate,toInterpolateIndexes);

  int patch_width=11;//Should be odd
  while(toInterpolateIndexes.size()>0){
      int changed =0;
      std::random_shuffle ( toInterpolateIndexes.begin(), toInterpolateIndexes.end() );
      for(int i=toInterpolateIndexes.size()-1;i>=0;i--){
          int col=toInterpolateIndexes[i].x;
          int row=toInterpolateIndexes[i].y;
          cv::Mat new_image=cv::Mat::zeros(patch_width,patch_width,CV_64F);

          //Compute theta and phi
          double theta=PI-col*2*PI/imageWidth;
          double phi=row*PI/imageHeight;

          //Create rotation matrix.
          //We first rotate randomly the first vector (the only one known by 90 and then vectorial product)
          cv::Mat rot=cv::Mat::zeros(3,3,CV_64F);
          rot.at<double>(0,0)=sin(phi)*cos(theta);
          rot.at<double>(1,0)=sin(phi)*sin(theta);
          rot.at<double>(2,0)=cos(phi);
          rot.at<double>(0,1)=0;
          rot.at<double>(1,1)=cos(phi)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
          rot.at<double>(2,1)=-sin(phi)*sin(theta)/(sqrt(cos(phi)*cos(phi)+sin(phi)*sin(phi)*sin(theta)*sin(theta)));
          rot.at<double>(0,2)=rot.at<double>(1,0)*rot.at<double>(2,1)-rot.at<double>(1,1)*rot.at<double>(2,0);
          rot.at<double>(1,2)=-(rot.at<double>(0,0)*rot.at<double>(2,1)-rot.at<double>(0,1)*rot.at<double>(2,0));
          rot.at<double>(2,2)=rot.at<double>(0,0)*rot.at<double>(1,1)-rot.at<double>(0,1)*rot.at<double>(1,0);

          //Compute theta and phi
          for(int v=0;v<new_image.rows;v++){
            for(int u=0;u<new_image.cols;u++){
              //Compute 3d point/vector
              double new_theta=(new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
              double new_phi=PI/2 - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;

              if(new_phi<=PI){ //Useful when doing a whole equi panorama
                double new_x=sin(new_phi)*cos(new_theta);
                double new_y=sin(new_phi)*sin(new_theta);
                double new_z=cos(new_phi);

                //We need to convert from new to old coordinates
                cv::Mat new_point=cv::Mat(3,1,CV_64F);
                new_point.at<double>(0,0)=new_x;
                new_point.at<double>(1,0)=new_y;
                new_point.at<double>(2,0)=new_z;

                cv::Mat old_point=rot*new_point;
                double old_theta=atan2(old_point.at<double>(1,0),old_point.at<double>(0,0));
                double old_phi=acos(old_point.at<double>(2,0));
                if(old_point.at<double>(2,0)>=1) old_phi=PI/2;

                int old_u=roundf((PI-old_theta)/2/PI*imageWidth);
                int old_v=roundf(old_phi/PI*imageHeight);

                new_image.at<double>(v,u)=temp.at<double>(old_v, old_u);
              }
            }
          }
          if(countNonZero(new_image)!=0){
            temp.at<double>(row,col)=sum(new_image).val[0]/countNonZero(new_image);
            toInterpolate.at<unsigned char>(row,col)=(unsigned char) 0;
            changed ++;
          }
      }
      //cout << "Changed " << changed << endl;
      cv::findNonZero(toInterpolate,toInterpolateIndexes);
      //cout << "To interpolate "  << toInterpolateIndexes.size() << endl;

      /*
      double min,max;
      cv::minMaxLoc( temp, &min, &max);
      //cv::Mat adjMap;
      cv::Mat colorDepthMap;
      cv::convertScaleAbs( temp, colorDepthMap, 255 / max);
      applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

      cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
      cv::imshow("Depth Map Smoothed",colorDepthMap);
      cv::waitKey();
      */
  }
  //cout << "Finished interpolation" << endl;

  before_blur=temp.clone();

  //2- smoothing
  angular_increment_per_pixel=2*PI/(double)imageWidth;
  //double angle_to_open=2*PI/180; //2 degrees
  //int patch_width=2*round(angle_to_open/angular_increment_per_pixel)+1; //Should be odd
  patch_width=11;//Should be odd

  for(int row=0;row<imageHeight;row++){
    for(int col=0;col<imageWidth;col++){
      cv::Mat new_image=cv::Mat::zeros(patch_width,patch_width,CV_64F);

      //Compute theta and phi
      double theta=PI-col*2*PI/imageWidth;
      double phi=row*PI/imageHeight;

      //We need to compute rotation matrix.
      for(int v=0;v<new_image.rows;v++){
        for(int u=0;u<new_image.cols;u++){
          //Compute 3d point/vector
          double new_theta=theta + (new_image.cols-1)/2*angular_increment_per_pixel -u*angular_increment_per_pixel;
          double new_phi=phi - (new_image.rows-1)/2*angular_increment_per_pixel +v*angular_increment_per_pixel;
          //cout << new_phi << endl;
          if(new_phi<=PI && new_phi>=0){ //Useful when doing a whole equi panorama

            int new_u=roundf((PI-new_theta)/2/PI*imageWidth);
            int new_v=roundf(new_phi/PI*imageHeight);

            new_image.at<double>(v,u)=before_blur.at<double>(new_v, new_u);
          }
      }
    }
    if(countNonZero(new_image)!=0){
      temp.at<double>(row,col)=sum(new_image).val[0]/countNonZero(new_image);
    }
  }
  }
  /*
  double min,max;
  cv::minMaxLoc( temp, &min, &max);
  //cv::Mat adjMap;
  cv::Mat colorDepthMap;
  cv::convertScaleAbs( temp, colorDepthMap, 255 / max);
  applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

  cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
  cv::imshow("Depth Map Smoothed",colorDepthMap);
  cv::waitKey();
  */

return temp;
}



cv::Mat regionfill2(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int pano_width, cv::Mat weight_values, cv::Mat& before_blur){
    //originalImage should be CV_64FC1 and validPixels and toInterpolage CV_8UC1
    //We will create 4 temporary matrices with 4 channels value and distance.
    int imageWidth=originalImage.cols;
    int imageHeight=originalImage.rows;

    int halfsize=round(pano_width/500/2)*2; //Should be even.
    cv::Mat kernel=cv::Mat(halfsize*2+1,halfsize*2+1,CV_64F);
    for(int row=0; row<=halfsize*2; row++){
        for(int col=0; col<=halfsize*2; col++){
            double dist=sqrt(pow(row-halfsize,2)+pow(col-halfsize,2));
            kernel.at<double>(row,col)=1.0/(pow(dist,1)+0.001);
        }
    }

    cv::Mat temp=originalImage.clone();

    cv::Mat toInterpolateOriginal=toInterpolate.clone();

    std::vector<cv::Point> toInterpolateIndexes;
    cv::findNonZero(toInterpolate,toInterpolateIndexes);

    while(toInterpolateIndexes.size()>0){
        int changed=0;
        std::random_shuffle ( toInterpolateIndexes.begin(), toInterpolateIndexes.end() );
        for(int i=0;i<toInterpolateIndexes.size();i++){
            int col=toInterpolateIndexes[i].x;
            int row=toInterpolateIndexes[i].y;
            //Apply kernel to compute a value
            cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
            cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
            cv::Mat image_roi=temp(cv::Rect(top_left,bottom_right));
            cv::Mat weight_roi=weight_values(cv::Rect(top_left,bottom_right));
            //cout << "Image kernel sizes " << kernel.cols << " , " << kernel.rows << endl;
            //cout << "Image roi sizes " << image_roi.cols << " , " << image_roi.rows << endl;
            cv::Mat kernel_roi=kernel(cv::Rect(cv::Point(halfsize,halfsize)-(cv::Point(col,row)-top_left),cv::Point(halfsize,halfsize)+(bottom_right-cv::Point(col,row))));
            cv::Mat weighted_image_roi=image_roi.mul(kernel_roi.mul(weight_roi));

            double sum_values=(double) cv::sum(weighted_image_roi)[0];
            if(sum_values==0){
                temp.at<double>(row,col)=0.0;
            }
            else{
                cv::Mat image_roi2;
                //cv::threshold(image_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                //cv::threshold(weight_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                //image_roi2=image_roi2.mul(kernel_roi);
                double sum_valid=(double)cv::sum(kernel_roi.mul(weight_roi))[0];
                //cout << "Value copmuted: " << sum_values/sum_valid << endl;
                temp.at<double>(row,col)=sum_values/sum_valid;
                changed++;
                //We already have put a value for this pixel, so we don't want to compute it again.
                toInterpolate.at<unsigned char>(row,col)=(unsigned char) 0;
                weight_values.at<double>(row,col)=0.5;
            }
/*            if(i%1000==0){
                cout << i << endl;
                double min,max;
                cv::Mat adjMap;
                cv::minMaxLoc( temp, &min, &max);
                cv::convertScaleAbs( temp, adjMap, 255 / max);


                cv::namedWindow("Temp", WINDOW_NORMAL | WINDOW_KEEPRATIO);
                cv::imshow("Temp",adjMap);
                cv::waitKey(100);
            }
*/        }
        cv::findNonZero(toInterpolate,toInterpolateIndexes);
    }

    if(SHOW_DEBUG_IMAGES){

        double min,max;
        cv::Mat adjMap;
        cv::minMaxLoc( temp, &min, &max);
        cv::convertScaleAbs( temp, adjMap, 255 / max);


        cv::namedWindow("Temp", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Temp",adjMap);
        cv::waitKey(100);
    }

    //We need to blur the image, but we can not use Standard Gaussian blur because it will use parts of the image out of contribution map.

    //kernel=cv::getGaussianKernel(halfsize*2+1, 5);

    //cout << "Kernel size :" << kernel.cols << " , " << kernel.rows << endl;
    //halfsize=round(pano_width/100/2)*2; //Should be even.
    halfsize=round(pano_width/32)*2; //Should be even.
    kernel=cv::Mat(halfsize*2+1,halfsize*2+1,CV_64F);
    for(int row=0; row<=halfsize*2; row++){
        for(int col=0; col<=halfsize*2; col++){
            //double dist=sqrt(pow(row-halfsize,2)+pow(col-halfsize,2));
            //kernel.at<double>(row,col)=1.0/(pow(dist,0.25)+0.001);
            kernel.at<double>(row,col)=1.0;
        }
    }

    //For most important
    int halfsize2=round(pano_width/256)*2; //Should be even.
    cv::Mat kernel2=cv::Mat(halfsize2*2+1,halfsize2*2+1,CV_64F);
    for(int row=0; row<=halfsize2*2; row++){
        for(int col=0; col<=halfsize2*2; col++){
            //double dist=sqrt(pow(row-halfsize2,2)+pow(col-halfsize2,2));
            //kernel2.at<double>(row,col)=1.0/(pow(dist,0.25)+0.001);
            kernel2.at<double>(row,col)=1.0;
        }
    }


    before_blur=temp.clone();

    cout <<"Blur" << endl;
    //10 iter
    for(int iter=0; iter<1; iter++){
        for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){
                if(weight_values.at<double>(row,col)>0.4){

                    cv::Point top_left, bottom_right;
                    cv::Mat image_roi, kernel_roi, weight_roi, weighted_image_roi;

                    if(weight_values.at<double>(row,col)>=1){
                    //if(0){
                        //Apply kernel to compute a value
                        top_left=cv::Point(std::max(col-halfsize2,0),std::max(row-halfsize2,0));
                        bottom_right=cv::Point(std::min(col+halfsize2+1,imageWidth),std::min(row+halfsize2+1,imageHeight));
                        image_roi=temp(cv::Rect(top_left,bottom_right));
                        kernel_roi=kernel2(cv::Rect(cv::Point(halfsize2,halfsize2)-(cv::Point(col,row)-top_left),cv::Point(halfsize2,halfsize2)+(bottom_right-cv::Point(col,row))));
                        weight_roi=weight_values(cv::Rect(top_left,bottom_right));
                        weighted_image_roi=image_roi.mul(kernel_roi.mul(weight_roi));

                    }
                    else{

                        //Apply kernel to compute a value
                        top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
                        bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
                        image_roi=temp(cv::Rect(top_left,bottom_right));
                        kernel_roi=kernel(cv::Rect(cv::Point(halfsize,halfsize)-(cv::Point(col,row)-top_left),cv::Point(halfsize,halfsize)+(bottom_right-cv::Point(col,row))));
                        weight_roi=weight_values(cv::Rect(top_left,bottom_right));
                        weighted_image_roi=image_roi.mul(kernel_roi.mul(weight_roi));

                    }

                    double sum_values=(double) cv::sum(weighted_image_roi)[0];
                    if(sum_values==0){
                        temp.at<double>(row,col)=0.0;
                    }
                    else{
                        cv::Mat image_roi2;
                        //cv::threshold(kernel_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                        //image_roi2=image_roi2.mul(image_roi2);
                        double sum_valid=(double)cv::sum(kernel_roi.mul(weight_roi))[0];
                        //cout << "Value copmuted: " << sum_values/sum_valid << endl;
                        temp.at<double>(row,col)=sum_values/sum_valid;

                    }
                }
            }
        }

        if(SHOW_DEBUG_IMAGES){
            double min,max;
            cv::Mat adjMap;
            cv::minMaxLoc( temp, &min, &max);
            cv::convertScaleAbs( temp, adjMap, 255 / max);


            cv::namedWindow("Temp smooth", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Temp smooth",adjMap);
            cv::waitKey();
        }
    }

    cout <<"Blur 2 " << endl;
    //10 iter
    for(int iter=0; iter<1; iter++){
        for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){
                if(weight_values.at<double>(row,col)>0.4){

                    cv::Point top_left, bottom_right;
                    cv::Mat image_roi, kernel_roi, weight_roi, weighted_image_roi;
                    //if(0){
                        //Apply kernel to compute a value
                        top_left=cv::Point(std::max(col-halfsize2,0),std::max(row-halfsize2,0));
                        bottom_right=cv::Point(std::min(col+halfsize2+1,imageWidth),std::min(row+halfsize2+1,imageHeight));
                        image_roi=temp(cv::Rect(top_left,bottom_right));
                        kernel_roi=kernel2(cv::Rect(cv::Point(halfsize2,halfsize2)-(cv::Point(col,row)-top_left),cv::Point(halfsize2,halfsize2)+(bottom_right-cv::Point(col,row))));
                        weight_roi=weight_values(cv::Rect(top_left,bottom_right));
                        weighted_image_roi=image_roi.mul(kernel_roi.mul(weight_roi));


                    double sum_values=(double) cv::sum(weighted_image_roi)[0];
                    if(sum_values==0){
                        temp.at<double>(row,col)=0.0;
                    }
                    else{
                        cv::Mat image_roi2;
                        //cv::threshold(kernel_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                        //image_roi2=image_roi2.mul(image_roi2);
                        double sum_valid=(double)cv::sum(kernel_roi.mul(weight_roi))[0];
                        //cout << "Value copmuted: " << sum_values/sum_valid << endl;
                        temp.at<double>(row,col)=sum_values/sum_valid;

                    }
                }
            }
        }
    }


    /*
            for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){



                if(temp.at<double>(row,col)==0.0 && toInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){ //Pixel doesn't have a value yet and needs to have one!
                    finished=false; //We have just filled a value, so we are not finished yet!
                    //Apply kernel to compute a value
                    cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
                    cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
                    cv::Mat image_roi=previousLoop(cv::Rect(top_left,bottom_right));
                    cv::Mat kernel_roi=kernel(cv::Rect(cv::Point(halfsize,halfsize)-(cv::Point(col,row)-top_left),cv::Point(halfsize,halfsize)+(bottom_right-cv::Point(col,row))));
                    cv::Mat weighted_image_roi=image_roi.mul(kernel_roi);


                    double sum_values=(double) cv::sum(weighted_image_roi)[0];
                    if(sum_values==0){
                        temp.at<double>(row,col)=0.0;
                    }
                    else{
                        cv::Mat image_roi2;
                        cv::threshold(image_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                        image_roi2=image_roi2.mul(kernel_roi);
                        double sum_valid=(double)cv::sum(image_roi2)[0];
                        //cout << "Valid pixels: " << sum_values/sum_valid << endl;
                        temp.at<double>(row,col)=sum_values/sum_valid;
                        changed++;
                        //We already have put a value for this pixel, so we don't want to compute it again.

                    }
                }
                else{
                    temp.at<double>(row,col)=previousLoop.at<double>(row,col);
                }
            }
        }
    *
    //We need to blur the image, but we can not use Standard Gaussian blur because it will use parts of the image out of contribution map.

    //kernel=cv::getGaussianKernel(halfsize*2+1, 5);

    //cout << "Kernel size :" << kernel.cols << " , " << kernel.rows << endl;
    halfsize=round(pano_width/40/2)*2; //Should be even.
    //10 iter
    for(int iter=0; iter<5; iter++){
        cv::Mat previousLoop=temp.clone();
        for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){
                if(toInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                    //Mean filter

                    cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
                    cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
                    cv::Mat image_roi=previousLoop(cv::Rect(top_left,bottom_right));
                    double sum_values=(double) cv::sum(image_roi)[0];
                    cv::Mat image_roi2;
                    cv::threshold(image_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                    double sum_valid=(double)cv::sum(image_roi2)[0];
                    //cout << "Valid pixels: " << sum_values/sum_valid << endl;
                    temp.at<double>(row,col)=sum_values/sum_valid;

                }
            }
        }

        double min,max;
        cv::Mat adjMap;
        cv::minMaxLoc( temp, &min, &max);
        cv::convertScaleAbs( temp, adjMap, 255 / max);


        cv::namedWindow("Temp", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Temp",adjMap);
        cv::waitKey(20);
    }
    */


    //cv::GaussianBlur(temp,temp,cv::Size(0,0),15);

    return temp;


}
cv::Mat regionfill(cv::Mat originalImage, cv::Mat validPixels, cv::Mat toInterpolate, int pano_width){
    //originalImage should be CV_64FC1 and validPixels and toInterpolage CV_8UC1
    //We will create 4 temporary matrices with 4 channels value and distance.
    int imageWidth=originalImage.cols;
    int imageHeight=originalImage.rows;

    cv::Mat transposed, transposedValidPixels, transposedToInterpolate;
    cv::transpose(originalImage, transposed);
    cv::transpose(validPixels, transposedValidPixels);
    cv::transpose(toInterpolate, transposedToInterpolate);


    cv::Mat temp[4];
    temp[0]=cv::Mat::zeros(imageHeight,imageWidth,CV_64FC2);
    double last_value=0;
    double distance_last_value=0;
    for(int row=0; row<imageHeight; row++){
        last_value=0;
        distance_last_value=0;
        for(int col=0; col<imageWidth; col++){
            if(validPixels.at<unsigned char>(row,col)==(unsigned char) 255){
                last_value=originalImage.at<double>(row,col);
                distance_last_value=0;
                temp[0].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
            else if(toInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                distance_last_value=distance_last_value+1;
                temp[0].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
        }
    }

    temp[1]=cv::Mat::zeros(imageHeight,imageWidth,CV_64FC2);
    for(int row=0; row<imageHeight; row++){
        last_value=0;
        distance_last_value=0;
        for(int col=imageWidth-1; col>=0; col--){
            if(validPixels.at<unsigned char>(row,col)==(unsigned char) 255){
                last_value=originalImage.at<double>(row,col);
                distance_last_value=0;
                temp[1].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
            else if(toInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                distance_last_value=distance_last_value+1;
                temp[1].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
        }
    }

    temp[2]=cv::Mat::zeros(imageWidth,imageHeight,CV_64FC2);
    for(int row=0; row<imageWidth; row++){
        last_value=0;
        distance_last_value=0;
        for(int col=0; col<imageHeight; col++){
            if(transposedValidPixels.at<unsigned char>(row,col)==(unsigned char) 255){
                last_value=transposed.at<double>(row,col);
                distance_last_value=0;
                temp[2].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
            else if(transposedToInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                distance_last_value=distance_last_value+1;
                temp[2].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
        }
    }

    temp[3]=cv::Mat::zeros(imageWidth,imageHeight,CV_64FC2);
    for(int row=0; row<imageWidth; row++){
        last_value=0;
        distance_last_value=0;
        for(int col=imageHeight-1; col>=0; col--){
            if(transposedValidPixels.at<unsigned char>(row,col)==(unsigned char) 255){
                last_value=transposed.at<double>(row,col);
                distance_last_value=0;
                temp[3].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
            else if(transposedToInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                distance_last_value=distance_last_value+1;
                temp[3].at<Vec2d>(row,col)=cv::Vec2d(last_value,distance_last_value);
            }
        }
    }
    cv::transpose(temp[2],temp[2]);
    cv::transpose(temp[3],temp[3]);


    //Need to scale?
    std::vector<cv::Mat> channels;
    split(temp[0], channels);
    double min,max;
    cv::minMaxLoc( channels[0], &min, &max);
    cv::convertScaleAbs( channels[0], channels[0], 255 / max);
    cv::namedWindow("Temp1", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Temp1",channels[0]);
    cv::waitKey();

    split(temp[1], channels);
    cv::minMaxLoc( channels[0], &min, &max);
    cv::convertScaleAbs( channels[0], channels[0], 255 / max);
    cv::namedWindow("Temp2", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Temp2",channels[0]);
    cv::waitKey();


    split(temp[2], channels);
    cv::minMaxLoc( channels[0], &min, &max);
    cv::convertScaleAbs( channels[0], channels[0], 255 / max);
    cv::namedWindow("Temp3", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Temp3",channels[0]);
    cv::waitKey();

    split(temp[3], channels);
    cv::minMaxLoc( channels[0], &min, &max);
    cv::convertScaleAbs( channels[0], channels[0], 255 / max);
    cv::namedWindow("Temp4", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Temp4",channels[0]);
    cv::waitKey();


    //We should now mix them!
    cv::Mat temp5=cv::Mat::zeros(imageHeight,imageWidth,CV_64FC1);
    for(int row=0; row<imageHeight; row++){
        for(int col=0; col<imageWidth; col++){

            bool done=false;
            for(int i=0;i<4;i++){
                if(temp[i].at<cv::Vec2d>(row,col)[0]!=0.0 && temp[i].at<cv::Vec2d>(row,col)[1]==0.0){//We have a correct value (Given value)
                    done=true;
                    temp5.at<double>(row,col)=temp[i].at<cv::Vec2d>(row,col)[0];
                }
            }

            if(!done){
                std::vector<cv::Vec2d> values;
                double min=1e6;
                for(int i=0;i<4;i++){
                    if(temp[i].at<cv::Vec2d>(row,col)[0]!=0.0 && temp[i].at<cv::Vec2d>(row,col)[1]!=0.0){//We have a correct value
                        if(temp[i].at<cv::Vec2d>(row,col)[1]<min){
                            min=temp[i].at<cv::Vec2d>(row,col)[1];
                        }
                        values.push_back(temp[i].at<cv::Vec2d>(row,col));
                    }
                }
                double sum=0;
                for(int i=0;i<values.size();i++){
                    sum=sum+min/values[i][1];
                }
                double k=1/sum;
                double value=0.0;
                for(int i=0;i<values.size();i++){
                    value=value+values[i][0]*k*min/values[i][1];
                }
                temp5.at<double>(row,col)=value;
            }
        }
    }


    //We need to blur the image, but we can not use Standard Gaussian blur because it will use parts of the image out of contribution map.

    //kernel=cv::getGaussianKernel(halfsize*2+1, 5);

    //cout << "Kernel size :" << kernel.cols << " , " << kernel.rows << endl;
    int halfsize=round(pano_width/50/2)*2; //Should be even.
 //Should be even.
    //10 iter
    for(int iter=0; iter<5; iter++){
        cout << "Iter: " << iter << endl;
        //cv::Mat previousLoop=temp5.clone();
        cv::Mat previousLoop=temp5.clone();
        for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){
                if(toInterpolate.at<unsigned char>(row,col)==(unsigned char) 255){
                    //Mean filter

                    cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
                    cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,imageWidth),std::min(row+halfsize+1,imageHeight));
                    cv::Mat image_roi=previousLoop(cv::Rect(top_left,bottom_right));
                    double sum_values=(double) cv::sum(image_roi)[0];
                    cv::Mat image_roi2;
                    cv::threshold(image_roi,image_roi2,0.0,1.0,cv::THRESH_BINARY); //If value is diff than 0 put to 1
                    double sum_valid=(double)cv::sum(image_roi2)[0];
                    //cout << "Valid pixels: " << sum_values/sum_valid << endl;
                    if(sum_valid!=0.0){
                        temp5.at<double>(row,col)=sum_values/sum_valid;
                    }
                    else{
                        temp5.at<double>(row,col)=0.0;
                    }
                }
            }
        }

        if(SHOW_DEBUG_IMAGES){
            double min,max;
            cv::Mat adjMap;
            cv::minMaxLoc( temp5, &min, &max);
            cv::convertScaleAbs( temp5, adjMap, 255 / max);


            cv::namedWindow("Temp", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Temp",adjMap);
            cv::waitKey(20);
        }
    }

    return temp5;

}
