#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

#include "depthmap_utils.cpp"


#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depthmap.exr\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc != 2)
    {
        printhelp(argv);
        return(1);
    }


    cout << "Depth map" << endl;
    string depth_map=string(argv[1]);
    string file_extension=depth_map.substr(depth_map.rfind(".")+1);

    cout << "File extension " << file_extension << endl;



    int width_depth_map, height_depth_map;
    std::vector<float> depthmapdata_equi;
    cv::Mat depthMap;
    std::vector<float> depthmapdata;

    depthMap=read_depthmap(depth_map);
    width_depth_map=depthMap.cols;
    height_depth_map=depthMap.rows;

    cv::Mat mask_no_values=depthMap==0;
    cv::Mat mask_values=depthMap!=0;

    cv::Mat colorDepthMap;
    double min, max;
    cv::minMaxLoc( depthMap, &min, &max,NULL, NULL, mask_values);
    cout << "Min value: " << min << endl;
    cout << "Max value: " << max << endl;

    //cv::Mat colorDepthMap;
    //CHEAT FOR MOST depthmap_utils
    //if(max>15.0) max=15.0;
    cv::convertScaleAbs( depthMap, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);

    cv::Mat mask;
    colorDepthMap.setTo(Scalar(0,0,0), mask_no_values);

    string file_without_extension=depth_map.substr(0,depth_map.rfind("."));

    stringstream ss;
    ss.str("");
    ss << file_without_extension << ".png";
    string filename=ss.str();

    std::vector<int> params;
    params.push_back(CV_IMWRITE_PNG_COMPRESSION);
    params.push_back(9);

    bool writing_success=imwrite(filename, colorDepthMap, params);
    if(!writing_success){
      cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< filename << endl;
      return 0;
    }
