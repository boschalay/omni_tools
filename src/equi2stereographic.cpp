#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel

#define PI 3.14159265358979323846

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s pano_first pano_last width max_angle rotate_original_180\n Name of pano must contain 'frameXXXXX'",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc < 6)
    {
        printhelp(argv);
        return(1);
    }

    string first_pano=string(argv[1]);
    string last_pano=string(argv[2]);
    const int pano_width=atoi(argv[3]); //Width is adjustable
    const int pano_height=pano_width;
    const double max_angle=atof(argv[4]);
    const bool rotate_original=atof(argv[5]);

    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    //Find first frame
    int indexframe = first_pano.rfind("frame");
    std::string first_frame_str = first_pano.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    //Find last frame
    indexframe = last_pano.rfind("frame");
    std::string last_frame_str = last_pano.substr(indexframe+5,5);
    int last_frame=atoi(last_frame_str.c_str());

    int i;

    //#pragma omp parallel for default(none) shared(first_frame, last_frame, first_pano, indexframe, cout)
    for(i = first_frame; i <= last_frame; i++) {
        string image_path;
        stringstream ss;
        ss.str("");
        image_path.clear();
        ss << first_pano.substr(0,indexframe+5) << setfill('0') << setw(5) << i << first_pano.substr(indexframe+10,first_pano.length()-indexframe-10);
        image_path=ss.str();
        cv::Mat equi_pano = imread(image_path, IMREAD_COLOR);

        if(equi_pano.data!=NULL){

            double equi_height=equi_pano.rows;
            double equi_width=equi_pano.cols;

            //Panoramic image
            cv::Mat panoramic;
            panoramic=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);

            int y;
            for(y = 0; y< pano_height; y++){
                int x;
                for(x = 0; x <pano_width ; x++) {

                    //We need a constant factor
                    double k=pano_width/(4*tan(max_angle/2));
                    double u_new=pano_width/2-x;
                    double v_new=pano_height/2-y;

                    //Find theta and phi
                    double theta=atan2(u_new,v_new);
                    double phi=2*atan2(sqrt(v_new*v_new+u_new*u_new),2*k);

                    if(rotate_original) phi=PI-phi;

                    //cout << "theta: " << theta << " phi: " << phi << endl;

                    int u=roundf((PI-theta)/(2*PI)*equi_width);
                    int v=roundf(phi/PI*equi_height);

                    //cout << "u: " << u << " v: " << v << endl;
                    if(u<0 || v<0 || u>equi_width || v>equi_height){
                        cout << "Out of limits" << endl;
                    }
                    else{
                        panoramic.ptr<unsigned char>(y,x)[0]=equi_pano.ptr<unsigned char>(v,u)[0];
                        panoramic.ptr<unsigned char>(y,x)[1]=equi_pano.ptr<unsigned char>(v,u)[1];
                        panoramic.ptr<unsigned char>(y,x)[2]=equi_pano.ptr<unsigned char>(v,u)[2];
                    }
                }
            }
            /*
            //Display
            cv::namedWindow("Display window", WINDOW_NORMAL);
            cv::imshow( "Display window", panoramic );
            cv::waitKey();
            */

            //Find full filename
            int indexdot = first_pano.rfind(".");
            string pano_filename;
            ss.str("");
            pano_filename.clear();
            ss << first_pano.substr(0,indexframe+5) << setfill('0') << setw(5) << i << first_pano.substr(indexframe+10,indexdot-indexframe-10)<< "_stereographic.png";
            pano_filename=ss.str();

            std::vector<int> qualityType;
            qualityType.push_back(IMWRITE_JPEG_QUALITY);
            qualityType.push_back(85); //Jpeg quality

            cout << pano_filename << endl;

            bool writing_success;
            writing_success=imwrite(pano_filename, panoramic, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

        }
        else{
            //cout << "Error reading image: "<< image_path << endl;
        }
    }
    cout << "Success!" << endl;

    return 0;
}
