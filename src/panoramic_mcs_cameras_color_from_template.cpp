#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel

//Global variable with look up table.
#define PI 3.14159265358979323846

using namespace cv;
using namespace std;


//Global variables
//Global variables
cv::Mat panoramic[6];
int pano_width, pano_height;

bool read_3d_mesh( char *mesh_file_path, int first_camera, int last_camera)
{
        FILE *fp = fopen( mesh_file_path, "r");
        if ( fp == NULL){
                printf( "Can't read 3D mesh file: %s\n", mesh_file_path);
                return false;
        }

        if ( fscanf( fp, "cols %d rows %d\n", &pano_width, &pano_height) != 2){
                printf( "Can't read cols/rows in 3d mesh file.\n");
                fclose( fp);
                return false;
        }

        printf("cols: %d, rows: %d\n",pano_width,pano_height);

        for(int i=0; i <6; i++){
                panoramic[i] =cv::Mat::zeros(pano_height,pano_width,CV_64FC3);
        }

        for ( int c = 0; c < last_camera-first_camera+1; c++){
                for ( int iRow = 0; iRow < pano_height; iRow++ ){
                        for ( int iCol = 0; iCol < pano_width; iCol++ ){
                                int R,C;
                                double alpha;
                                if ( fscanf( fp, "%d, %d, %lf", &R, &C, &alpha) != 3){
                                        printf( "Can't read grid data in 3d mesh file.\n");
                                        printf("c: %d, row: %d, col: %d\n",c, iRow, iCol);
                                        return false;
                                }
                                panoramic[c+first_camera].ptr<int>(iRow,iCol)[0]=R;
                                panoramic[c+first_camera].ptr<int>(iRow,iCol)[1]=C;
                                panoramic[c+first_camera].ptr<double>(iRow,iCol)[2]=alpha;
                        }
                }
        }
        fclose( fp);
        return true;
}

void printhelp(char *argv[]){
	fprintf(stderr,"Usage: %s <mesh> first_camera last_camera\n",argv[0]);
        fprintf(stderr,"This will generate a mask for the panoramas.\n");
}


int main(int argc, char *argv[])
{

	if(argc != 4)
	{
		printhelp(argv);
		return(1);
	}
  int first_camera= atoi(argv[2]);
  int last_camera= atoi(argv[3]);

    cout << "Reading look up table ..." << endl;
    if (!read_3d_mesh( argv[1], first_camera, last_camera))
    {
        return 1;
    }

    //Panoramic image
    cv::Mat panoramic_image=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);
    //#pragma omp parallel for default(none) shared(panoramic,panoramic_image,pano_height,pano_width)
    for(int y = 0; y< pano_height; y++){
        for(int x = 0; x !=pano_width ; x++) {
            int covered=0;
            for(int cam = first_camera; cam<= last_camera; cam++){
                if(panoramic[cam].ptr<double>(y,x)[2]!=0){
                    covered++;
                }
            }
            for(int cam = first_camera; cam<= last_camera; cam++){
                if(panoramic[cam].ptr<double>(y,x)[2]!=0){
                    if (cam==1){
                        panoramic_image.ptr<unsigned char>(y,x)[0]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[0] +(1.0/covered) * 0;
                        panoramic_image.ptr<unsigned char>(y,x)[1]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[1] +(1.0/covered) * 127;
                        panoramic_image.ptr<unsigned char>(y,x)[2]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[2] +(1.0/covered) * 255;
                    }
                    else if (cam==2){
                      panoramic_image.ptr<unsigned char>(y,x)[0]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[0] +(1.0/covered) * 0;
                      panoramic_image.ptr<unsigned char>(y,x)[1]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[1] +(1.0/covered) * 255;
                      panoramic_image.ptr<unsigned char>(y,x)[2]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[2] +(1.0/covered) * 255;
                    }
                    else if (cam==3) {
                      panoramic_image.ptr<unsigned char>(y,x)[0]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[0] +(1.0/covered) * 127;
                      panoramic_image.ptr<unsigned char>(y,x)[1]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[1] +(1.0/covered) * 255;
                      panoramic_image.ptr<unsigned char>(y,x)[2]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[2] +(1.0/covered) * 127;
                    }
                    else if (cam==4) {
                      panoramic_image.ptr<unsigned char>(y,x)[0]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[0] +(1.0/covered) * 255;
                      panoramic_image.ptr<unsigned char>(y,x)[1]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[1] +(1.0/covered) * 255;
                      panoramic_image.ptr<unsigned char>(y,x)[2]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[2] +(1.0/covered) * 0;
                    }
                    else if (cam==5) {
                      panoramic_image.ptr<unsigned char>(y,x)[0]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[0] +(1.0/covered) * 255;
                      panoramic_image.ptr<unsigned char>(y,x)[1]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[1] +(1.0/covered) * 127;
                      panoramic_image.ptr<unsigned char>(y,x)[2]= (unsigned char) panoramic_image.ptr<unsigned char>(y,x)[2] +(1.0/covered) * 0;
                    }
                }
            }
        }
    }
    /*
    //Display
    cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Display window", panoramic_image );
    cv::waitKey();
    */

    stringstream ss;
    string pano_filename;
    ss.str("");
    pano_filename.clear();
    ss << "pano_cameras_color" << ".png";
    pano_filename=ss.str();

    cout << pano_filename << endl;

    bool writing_success;
    writing_success=imwrite(pano_filename, panoramic_image);
    if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
            return 1;
    }
	cout << "Success!" << endl;
        return 0;
}
