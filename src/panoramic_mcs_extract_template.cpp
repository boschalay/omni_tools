#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp"

#define PI 3.14159265358979323846

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width calibration_file radius blending_width first_camera last_camera\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc < 7)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;
    cout << "Arguments: " << argc << endl;

    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[2]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=atof(argv[3]);
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    const double blending_width=atof(argv[4]);

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();

    int first_camera=atoi(argv[5]);
    int last_camera=atoi(argv[6]);

    double R=calibration.getSphereRadius();
    int result;

    //Panoramic table
    cv::Mat panoramic[last_camera+1];
    //Fundamental mat.
    for(int i=0; i <=last_camera; i++){
      panoramic[i] =cv::Mat::zeros(pano_height,pano_width,CV_64FC3);
    }

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(y = 0; y< pano_height; y++){
        double dRow[last_camera];
        std::fill_n(dRow, numberCameras,-1);
        double dCol[last_camera];
        std::fill_n(dCol, numberCameras,-1);
        int x;
        for(x = 0; x <pano_width ; x++) {
            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=-(2*PI*x/(pano_width-1))+PI;// To rotate 180: -PI;
            double phi=PI*y/(pano_height-1);

            double dist=R;

            //From theta and phi find the 3D coordinates.
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            float minDistanceCenter=pano_width*pano_width; // Max value possible.
            int cameraMinDistanceCenter=-1;
            int covered=0;
            int result_;

            //From the 3D coordinates, find in how many camera falls the point!
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                }
                else{
                    dRow[cam]=-1;
                    dCol[cam]=-1;
                }
            }
            if(covered){
                if (blending_width==0){
                    for(int cam = first_camera; cam<= last_camera; cam++){
                        if(dRow[cam]!=-1){
                            float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+ pow(dCol[cam]-imageWidth/2,2));
                            if(covered) {
                                if (distanceCenter<=minDistanceCenter){
                                    panoramic[cam].ptr<double>(y,x)[0]= dRow[cam];
                                    panoramic[cam].ptr<double>(y,x)[1]= dCol[cam];
                                    panoramic[cam].ptr<double>(y,x)[2]=1.0;
                                }
                            }
                        }
                    }
                }
                else{
                    //We know now the minimum distance
                    float sum_distance=0;
                    for(int cam = first_camera; cam<= last_camera; cam++){
                        if(dRow[cam]!=-1){
                            float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+ pow(dCol[cam]-imageWidth/2,2));
                            float diff=round(abs(distanceCenter)-abs(minDistanceCenter));
                            if (diff<=blending_width) {
                                    sum_distance+=(blending_width-diff);
                            }
                        }
                    }
                    for(int cam = first_camera; cam<= last_camera; cam++){
                        if(dRow[cam]!=-1){
                            float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+ pow(dCol[cam]-imageWidth/2,2));
                            float diff=round(abs(distanceCenter)-abs(minDistanceCenter));
                            if (diff<=blending_width) {
                                panoramic[cam].ptr<double>(y,x)[0]=dRow[cam];
                                panoramic[cam].ptr<double>(y,x)[1]=dCol[cam];
                                panoramic[cam].ptr<double>(y,x)[2]= ((double)(blending_width-diff))/((double)sum_distance);
                                //  printf("%f\n",(double)(blending_width-diff));
                            }
                        }
                    }
                }
              }
        }

        done[y]=true;

        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;

        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;

    }

    FILE * pFile;
	string filename;
  stringstream ss;
	ss.str("");
    ss << "pano_" << pano_width << "_radius"<< radius << ".txt";
	filename=ss.str();
    pFile = fopen (filename.c_str(),"w");

	fprintf(pFile, "cols %d rows %d\n", pano_width, pano_height);

	for(int cam = first_camera; cam<= last_camera; cam++){
		for(int y = 0; y!= pano_height; y++){
			for(int x = 0; x !=pano_width ; x++) {
                int R= (int) panoramic[cam].ptr<double>(y,x)[0];
                int C= (int) panoramic[cam].ptr<double>(y,x)[1];
                double alpha=panoramic[cam].ptr<double>(y,x)[2];
                fprintf (pFile, "%d,%d,%4.4lf\n", R, C, alpha);
			}
		}
	}

    fclose (pFile);
	cout << "Success!" << endl;
    cout << "Mesh file written in "<< filename << endl;

	cout << "Success!" << endl;

    return 0;
}
