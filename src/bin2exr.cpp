#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depthmap.exr\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc != 2)
    {
        printhelp(argv);
        return(1);
    }


    string file_path=argv[1];
    string file_extension=file_path.substr(file_path.rfind(".")+1);

    cout << "File extension " << file_extension << endl;

    int width_depth_map, height_depth_map;
    std::vector<float> depthmapdata;
    cv::Mat depthMap, panoramic;

    depthMap=read_depthmap(file_path);

    //===============================SAVING EXR===============================
    stringstream ss;
    string file_without_extension=file_path.substr(0,file_path.rfind("."));

    ss.str("");
    ss << file_without_extension << ".exr";
    string filename=ss.str();
    cv::Mat temp;
    depthMap.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);

    cout << "Mesh file written in "<< filename << endl;

}
