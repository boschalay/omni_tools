#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel

#define PI 3.14159265358979323846

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s pano_first pano_last width max_angle rotate_original_180\n ",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc < 6)
    {
        printhelp(argv);
        return(1);
    }

    string first_pano=string(argv[1]);
    string last_pano=string(argv[2]);
    const int pano_width=atoi(argv[3]); //Width is adjustable
    const int pano_height=pano_width/2;
    const double max_angle=atof(argv[4]);
    const bool rotate_original=atof(argv[5]);

    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    //Find first frame
    int indexframe = first_pano.rfind("frame");
    std::string first_frame_str = first_pano.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    //Find last frame
    indexframe = last_pano.rfind("frame");
    std::string last_frame_str = last_pano.substr(indexframe+5,5);
    int last_frame=atoi(last_frame_str.c_str());

    int i;

    //#pragma omp parallel for default(none) shared(first_frame, last_frame, first_pano, indexframe, cout)
    for(i = first_frame; i <= last_frame; i++) {
        string image_path;
        stringstream ss;
        ss.str("");
        image_path.clear();
        ss << first_pano.substr(0,indexframe+5) << setfill('0') << setw(5) << i << first_pano.substr(indexframe+10,first_pano.length()-indexframe-10);
        image_path=ss.str();
        string file_extension=image_path.substr(image_path.rfind(".")+1);

        cv::Mat stereographic_pano;
        if(file_extension=="exr"){
          cout << "Exr file" << endl;
          stereographic_pano = imread(image_path,CV_LOAD_IMAGE_ANYDEPTH);
        }
        else{
          stereographic_pano = imread(image_path,CV_LOAD_IMAGE_COLOR);
        }

        if(stereographic_pano.data!=NULL){

          double stereographic_height=stereographic_pano.rows;
          double stereographic_width=stereographic_pano.cols;

          //Panoramic image
          cv::Mat panoramic;
          panoramic=cv::Mat::zeros(pano_height,pano_width,stereographic_pano.type());

          int y;
          for(y = 0; y< pano_height; y++){
              int x;
              for(x = 0; x <pano_width ; x++) {
                   //Find theta and phi for panorama coordinates
					          double theta=PI-2*PI*x/pano_width;
                    double phi=y*PI/pano_height;

                    if(rotate_original) phi=PI-phi;

                    int u= roundf((max_angle-phi*sin(theta))*(stereographic_width)/(2*max_angle));
                    int v= roundf((max_angle-phi*cos(theta))*(stereographic_width)/(2*max_angle));


                    if((u<0 || v<0 || u>(stereographic_width-1) || v>(stereographic_height-1))){
                        //cout << "u: " << u << " v: " << v << endl;
                    }
                    else{
                      if(file_extension!="exr"){
                        panoramic.ptr<unsigned char>(y,x)[0]=stereographic_pano.ptr<unsigned char>(v,u)[0];
                        panoramic.ptr<unsigned char>(y,x)[1]=stereographic_pano.ptr<unsigned char>(v,u)[1];
                        panoramic.ptr<unsigned char>(y,x)[2]=stereographic_pano.ptr<unsigned char>(v,u)[2];
                      }
                      else{
                          panoramic.ptr<float>(y,x)[0]=stereographic_pano.ptr<float>(v,u)[0];
                      }
                    }
                }
            }
            /*
            //Display
            cv::namedWindow("Display window", WINDOW_NORMAL);
            cv::imshow( "Display window", panoramic );
            cv::waitKey();
            */

            //Find full filename
            int indexdot = first_pano.rfind(".");
            string pano_filename;
            ss.str("");
            pano_filename.clear();
            if(file_extension!="exr"){
              ss << first_pano.substr(0,indexframe+5) << setfill('0') << setw(5) << i << first_pano.substr(indexframe+10,indexdot-indexframe-10)<< "_equirectangular.png";
            }
            else{
              ss << first_pano.substr(0,indexframe+5) << setfill('0') << setw(5) << i << first_pano.substr(indexframe+10,indexdot-indexframe-10)<< "_equirectangular.exr";
            }
            pano_filename=ss.str();

            std::vector<int> qualityType;
            qualityType.push_back(IMWRITE_JPEG_QUALITY);
            qualityType.push_back(85); //Jpeg quality

            cout << pano_filename << endl;

            bool writing_success;
            writing_success=imwrite(pano_filename, panoramic, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }
            else{
				cout << "Image written " << pano_filename << endl;
			}

        }
        else{
            //cout << "Error reading image: "<< image_path << endl;
        }
    }

    return 0;
}
