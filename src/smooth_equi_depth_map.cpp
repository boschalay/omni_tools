#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

#include "mcs_calibration.hpp"
#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depth_map_txt calibration_file first_camera last_camera\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc <5)
    {
        printhelp(argv);
        return(1);
    }

    //-- Load calibation matrices.
    MultiCamCalibration calibration; //Create calibration object.
    string calibration_file=string(argv[2]);
    calibration.loadCalibrationFile(calibration_file);

    int first_camera=atoi(argv[3]);
    int last_camera=atoi(argv[4]);

    string file_path=argv[1];

    cv::Mat depthMap;
    depthMap=read_depthmap(file_path);
    int width_depth_map, height_depth_map;
    width_depth_map=depthMap.cols;
    height_depth_map=depthMap.rows;

    //We resize it to make things faster. Anyway we are smoothing it...
    float ratio=(float)height_depth_map/(float)width_depth_map;
    width_depth_map=1000;
    height_depth_map=1000.0*ratio;
    /*if(depthMap.rows==depthMap.cols){
      width_depth_map=500;
      height_depth_map=500.0*ratio;
    }*/
    cv::resize(depthMap, depthMap, cv::Size(width_depth_map, height_depth_map),0,0,cv::INTER_NEAREST );

    //=====================SMOOTH PANORAMA

    cv::Mat valid_pixels;
    cv::threshold(depthMap,valid_pixels,0,255,cv::THRESH_BINARY);
    valid_pixels.convertTo(valid_pixels, CV_8U);

    cv::Mat tobeInpainted;
    if(depthMap.rows==depthMap.cols){
      cout << "Fisheye projection" << endl;
      //Dome
      tobeInpainted = cv::Mat::zeros(depthMap.rows,depthMap.cols,CV_8U);
      cv::circle(tobeInpainted, cv::Point(depthMap.cols/2, depthMap.rows/2), depthMap.rows/2, 1,-1);
      tobeInpainted=tobeInpainted*255-valid_pixels;
    }
    else{
      cout << "Equirectangular projection" << endl;
      //EQUI
      tobeInpainted = cv::Mat::ones(depthMap.rows,depthMap.cols,CV_8U)*255-valid_pixels;

    }

    //All same weight
    cv::Mat weight_values;
    cv::Mat(valid_pixels/255*1.0).convertTo(weight_values, CV_64F);

    cv::Mat beforeBlur;
    cout << "About to start interpolation" << endl;
    beforeBlur=interpolate5(depthMap,valid_pixels,tobeInpainted,21);
    cv::Mat smoothed=beforeBlur;
    cout << "About to start smoothing " << endl;
  //cv::Mat smoothed=smooth3(beforeBlur,valid_pixels,cv::Mat::ones(height_depth_map,width_depth_map,CV_8U)*255,cv::Mat(),1); //Smoothing distance


  /*
   //We check if it's on the FOV of the cameras.

   for(int y = 0; y< depthMap.rows; y++){
       int x;
       for(x = 0; x <depthMap.cols ; x++) {
           double globalX;
           double globalY;
           double globalZ;

           //Find 3D position
           //Theta goes from -pi to pi
           //Phi goes from 0  to pi
           double theta=-(2*PI*x/(depthMap.cols-1))+PI;// To rotate 180: -PI;
           double phi=PI*y/(depthMap.rows-1);

           double dist=smoothed.at<double>(y,x);
           //From theta and phi find the 3D coordinates.
           globalZ=dist*cos(phi);
           globalX=dist*sin(phi)*cos(theta);
           globalY=dist*sin(phi)*sin(theta);

           int covered=0;
           int result_;
           //From the 3D coordinates, find in how many camera falls the point!
           int cam;
           for(cam = first_camera; cam<= last_camera; cam++){
               double dRow, dCol;
               result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow, dCol);
               if (result_!=-1) {
                   covered++;
                   }
               }
           if(covered==0){
               smoothed.at<double>(y,x)=0;
           }
       }
   }
   */


    //For dome If phi> PI -> Black

    double min,max;
    cv::minMaxLoc( beforeBlur, &min, &max);
    cv::Mat adjMap,colorDepthMap,mask;
    cv::convertScaleAbs( beforeBlur, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    string file_without_extension=file_path.substr(0,file_path.rfind("."));
    stringstream ss;
    ss.str("");
    ss << file_without_extension << "_filtered_before_blur.png";
    string pano_filename;
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    bool writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    cv::minMaxLoc( smoothed, &min, &max);
    //cv::Mat adjMap;
    cv::convertScaleAbs( smoothed, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    if(SHOW_DEBUG_IMAGES){

        cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map Smoothed",colorDepthMap);
        cv::waitKey(10);
    }

    ss.str("");
    ss << file_without_extension << "_smoothed.png";
    pano_filename.clear();
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    ss.str("");
    ss << file_without_extension << "_smoothed.exr";
    string filename=ss.str();
    cv::Mat temp;
    smoothed.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);

    cout << "Mesh file written in "<< filename << endl;

    return 0;
}
