#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width image_colour_camera0 calibration_file radius last_camera EquiDepthMap ContributionMap\n",argv[0]);
}

bool read_depth( string depth_file, int& width, std::vector<float>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
    }
    return true;
}

bool read_depth_binary( string depth_file, int& width, std::vector<float>&  data)
{


    std::ifstream fin(depth_file.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cout << " Error, Couldn't find the file" << "\n";
        return false;
    }
    else{
        cout << "Depth map found" << endl;
    }

    fin.seekg(0, std::ios::end);
    const size_t num_elements = fin.tellg() / sizeof(float);
    fin.seekg(0, std::ios::beg);

    float w,h;

    fin.read(reinterpret_cast<char*>(&w), sizeof(float));
    fin.read(reinterpret_cast<char*>(&h), sizeof(float));

    width=(int)round(w);

    cout << "Depth map Width:" << width << endl;
    cout << "Depth map Height:" << h << endl;

    data.resize(num_elements-2);
    fin.read(reinterpret_cast<char*>(&data[0]), (num_elements-2)*sizeof(float));

    return true;
}

int main(int argc, char *argv[])
{

    if(argc != 8)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    string image0=string(argv[2]);
    int cameraindex = image0.rfind("camera");
    string file_part0 = image0.substr(0, cameraindex);
    string file_part1 = image0.substr(0, cameraindex+6);
    int first_camera= atoi(image0.substr(cameraindex+6,1).c_str());
    //Find now the frame. The numbering of the frame is 5 digits.
    int indexframe = image0.rfind("frame");
    string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
    string first_frame_str = image0.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    string file_part3 = image0.substr(indexframe+10);
    int last_camera=atoi(argv[5]);

    cout << "Last camera is: " << last_camera << endl;

    cout << "Arguments: " << argc << endl;
    cv::Mat scaledDepthMap, adjMap;


    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[3]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=atof(argv[4]);
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();


    std::vector<double> depthmapdata[last_camera+1];

    cout << "Depth map" << endl;
    string depth_map=string(argv[6]);
    string file_extension=depth_map.substr(depth_map.rfind(".")+1);

    cout << "File extension " << file_extension << endl;

    int width_depth_map;
    std::vector<float> depthmapdata_equi;
    cv::Mat depthMap;

    if(file_extension=="txt"){
        if(read_depth(depth_map,width_depth_map, depthmapdata_equi)){
            depthMap=cv::Mat::zeros(width_depth_map/2,width_depth_map,CV_32F);
            memcpy(depthMap.data, depthmapdata_equi.data(), depthmapdata_equi.size()*sizeof(float));
        }
    }
    else if(file_extension=="bin"){
        if(read_depth_binary(depth_map,width_depth_map, depthmapdata_equi)){
            depthMap=cv::Mat::zeros(width_depth_map/2,width_depth_map,CV_32F);
            memcpy(depthMap.data, depthmapdata_equi.data(), depthmapdata_equi.size()*sizeof(float));
        }
    }
    else if(file_extension=="exr"){
      cv::Mat exrMat=cv::imread(depth_map,CV_LOAD_IMAGE_ANYDEPTH);
      Mat bgr[3];   //destination array
      split(exrMat,bgr);//split source
      depthMap=bgr[0].clone();
      width_depth_map=depthMap.cols;

    }
    else{
      cout << "Depth map extension not recognised." << endl;
      return 1;
    }
    depthMap.convertTo(depthMap,CV_64F);

    double min, max;
    cv::minMaxLoc(depthMap, &min, &max);

    cv::resize(depthMap, scaledDepthMap, cv::Size(pano_width,pano_height),0,0,cv::INTER_NEAREST );
    cv::convertScaleAbs(scaledDepthMap, adjMap, 255 / max);

    if(SHOW_DEBUG_IMAGES){

        cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map",adjMap);
        cv::waitKey(20);
    }

    string contribution_map_path=string(argv[7]);
    cv::Mat pano_contribution_map=cv::imread(contribution_map_path, cv::IMREAD_GRAYSCALE);
    if(pano_contribution_map.data==NULL){
        cout << "Error reading image: "<< contribution_map_path << endl;
        return 1;
    }
    cout << "Contribution map read" << endl;
    int cam=(int)pano_contribution_map.at<unsigned char>(0,0);
    int resize_ratio=pano_width/pano_contribution_map.cols;
    cout << "Resize ratio: " << resize_ratio << endl;
    cv::resize(pano_contribution_map,pano_contribution_map, cv::Size(pano_width,pano_height),0,0,cv::INTER_NEAREST);


    //Read the images.

    Mat image[last_camera+1];
    string image_path;
    stringstream ss;

    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        image_path.clear();
        ss << file_part1 << cam << file_part2 << setfill('0')<< setw(5) << first_frame<< file_part3;
        image_path=ss.str();
        image[cam] = imread(image_path, IMREAD_COLOR);
        if(image[cam].data==NULL){
            cout << "Error reading image: "<< image_path << endl;
            return 1;
        }
    }

    cout << "All images opened succesfully. About to start panoramic image" << endl;


    double R=calibration.getSphereRadius();
    int result;

    //Panoramic image
    cv::Mat panoramic;
    panoramic=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(y = 0; y< pano_height; y++){
        int x;
        for(x = 0; x <pano_width ; x++) {

            double dRow;
            double dCol;

            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=PI-x*2*PI/pano_width;// To rotate 180: -PI;
            double phi=PI*y/pano_height;

            double dist=0;
            if(scaledDepthMap.at<double>(y,x)!=0){
                dist=scaledDepthMap.at<double>(y,x);
            }

            //From theta and phi find the 3D coordinates.
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            int result_;

            //From the 3D coordinates, find in how many camera falls the point!
            int cam=(int)pano_contribution_map.at<unsigned char>(y,x);
            if (cam!=255){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow, dCol);
                if (result_!=-1) {
                    cv::Vec3b intensity =image[cam].at<cv::Vec3b>(dRow,dCol);
                    panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) intensity.val[0];
                    panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) intensity.val[1];
                    panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) intensity.val[2];
                }
                else{
                  //This is probably due to scaling of the the contribution_map. Try with the neighbour camera.
                  cv::Point top_left=cv::Point(std::max(x-resize_ratio/2,0),std::max(y-resize_ratio/2,0));
                  cv::Point bottom_right=cv::Point(std::min(x+resize_ratio/2+1,pano_width),std::min(y+resize_ratio/2+1,pano_height));
                  cv::Mat image_roi=pano_contribution_map(cv::Rect(top_left,bottom_right));
                  cv::Mat nonZeroCoordinates;
                  cv::Mat mask=(image_roi!=(unsigned char)cam)&(image_roi!=(unsigned char)255);
                  cv::findNonZero(mask, nonZeroCoordinates);
                  while(nonZeroCoordinates.total()>0){
                    cam=(int)image_roi.at<unsigned char>(nonZeroCoordinates.at<cv::Point>(0));
                    result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow, dCol);
                    if (result_!=-1) {
                        cv::Vec3b intensity =image[cam].at<cv::Vec3b>(dRow,dCol);
                        panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) intensity.val[0];
                        panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) intensity.val[1];
                        panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) intensity.val[2];
                        mask.setTo(0);
                    }
                    else{
                      mask.at<unsigned char>(nonZeroCoordinates.at<cv::Point>(0))=0;
                    }
                    cv::findNonZero(mask, nonZeroCoordinates);
                  }

                }
            }
            else{
              //This pixel is not in the contribution map. But check anyway if according to model there is somebody that can use it
              float minDistanceCenter=pano_width*pano_width; // Max value possible.
              int cameraMinDistanceCenter=-1;
              int covered=0;
              int result_;
              double dRow[last_camera];
              std::fill_n(dRow, numberCameras,-1);
              double dCol[last_camera];
              std::fill_n(dCol, numberCameras,-1);

              //From the 3D coordinates, find in how many camera falls the point!
              for(cam = first_camera; cam<= last_camera; cam++){
                  result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                  if (result_!=-1) {
                      covered++;
                      float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                      if (distanceCenter<minDistanceCenter){
                          minDistanceCenter=distanceCenter;
                          cameraMinDistanceCenter=cam;
                      }
                  }
                  else{
                      dRow[cam]=-1;
                      dCol[cam]=-1;
                  }
              }
              if(covered>0){
                  cv::Vec3b intensity =image[cameraMinDistanceCenter].at<cv::Vec3b>(dRow[cameraMinDistanceCenter],dCol[cameraMinDistanceCenter]);
                  panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) intensity.val[0];
                  panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) intensity.val[1];
                  panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) intensity.val[2];
              }
            }
        }

        done[y]=true;

        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;

        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;

    }

    //Display
    /*cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Display window", panoramic );
    cv::waitKey();
    */

    string pano_filename;
    ss.str("");
    pano_filename.clear();
    ss << file_part0 << "pano_smooth"<< file_part2 << setfill('0')<< setw(5) << first_frame << ".png";
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    cout << pano_filename << endl;

    bool writing_success;
    writing_success=imwrite(pano_filename, panoramic, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }

      cv::Mat superImposed;
      cv::cvtColor(adjMap, adjMap, CV_GRAY2RGB);
      cv::addWeighted( panoramic, 0.3, adjMap, 0.7, 0.0, superImposed);
      ss.str("");
      ss << file_part0 << "pano_smooth_super_imposed"<< file_part2 << setfill('0')<< setw(5) << first_frame << ".png";
      pano_filename=ss.str();

      cout << pano_filename << endl;

      writing_success=imwrite(pano_filename, superImposed, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }


    cout << "Success!" << endl;

    return 0;
}
