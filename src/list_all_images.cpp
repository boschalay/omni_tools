// listdir.c
#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>

using namespace std;

int main(int argc, char *argv[])
{
  struct dirent *de=NULL;
  DIR *d=NULL;
  string extension;
  string path;

  if(argc != 3)
  {
    fprintf(stderr,"Usage: %s dirname extension\n",argv[0]);
    return(1);
  }

  d=opendir(argv[1]);
  extension=string(argv[2]);
  if(d == NULL)
  {
    perror("Couldn't open directory");
    return(2);
  }
  else{
	path=string(argv[1]);
	string::iterator it = path.end() - 1;
	if (*it == '/'){
	     path.erase(it);
	}
}

	//Create a loop with the list of images.
	vector <string> list;

	while(de = readdir(d)){
		if (de->d_type == DT_REG){		// if entry is a regular file
			string fname = string(de->d_name);	// filename
			// if filename's last characters are extension
			if (fname.find(extension, (fname.length() - extension.length())) != string::npos){
				string image_fn = string(string(path + "/" +fname));
				list.push_back( image_fn);
			}
		}
	}	
	closedir(d);
	sort( list.begin(), list.end() );

	cout << "Found " << list.size() << " images." << endl;

	for(vector<string>::size_type i = 0; i != list.size(); i++) {
		cout << list[i] << "\r" << endl;
	}
  return(0);
}
