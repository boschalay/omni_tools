#include <iostream>
#include <fstream>
#include <stdio.h>
#include <dirent.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "cvcalibinit3.h"

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
    if(argc < 6){
        fprintf(stderr,"Usage: %s dirname extension fisheye <Dist parameters> <PatternRows> <PatternCols>\n Example: %s selection jpg 0 3 9 11\n",argv[0],argv[0]);
        return(1);
    }

    bool fisheye=atoi(argv[3]);
    int num_ks=0;
    if(argc>4) num_ks=atoi(argv[4]);
    Size boardSize=Size(atoi(argv[5]),atoi(argv[6])); //Number of corners in the board
    double squareSize=0.05;

    bool correct_corners = true; // Check if the orientation of detected corners is ok
    // This function creates the 3D points of your chessboard in its own coordinate system
    float width = ((boardSize.width-1)*squareSize)/2.0;
    float height = ((boardSize.height-1)*squareSize)/2.0;
    cout << "Here" << endl;
    string dir_path(argv[1]);
    //===========Open directory
    struct dirent *de=NULL;
    DIR *d=NULL;
    string extension;
    string path;
    d=opendir(argv[1]);
    extension=string(argv[2]);
    if(d == NULL){
        perror("Couldn't open directory");
        return(1);
    }
    else{
        path=string(argv[1]);
        string::iterator it = path.end() - 1;
        if (*it == '/'){
             path.erase(it);
        }
    }
    cout << "Here" << endl;
    //===========Create a list with the list of images.
	vector <string> list;
	while(de = readdir(d)){
		if (de->d_type == DT_REG){		// if entry is a regular file
			string fname = string(de->d_name);	// filename
			// if filename's last characters are extension
			if (fname.find(extension, (fname.length() - extension.length())) != string::npos){
				string image_fn = string(string(path + "/" +fname));
				list.push_back( image_fn);
			}
		}
	}
	closedir(d);
	sort( list.begin(), list.end() );
	cout << "Found " << list.size() << " images." << endl;

    char k;
    cout << "Do you want to save image with corners Drawn [Y]/N)?\n";
    cin >> k;
    bool save_corner_images=true;
    if(k=='N'){
        save_corner_images=false;
    }

    //============ Loop all images to find corners.
    vector<vector<Point2f> > imagePoints;
    Size imageSize;
	for(vector<string>::size_type i = 0; i != list.size(); i++) {
		cout << "Reading " << list[i] << endl;
        cv::Mat image = imread(list[i], IMREAD_COLOR);
        imageSize=image.size();
        /*
        Downsample the input image to approximately VGA resolution and detect the
        calibration target corners in the full-size image.
        Combines these apparently orthogonal duties as an optimization. Checkerboard
        detection is too expensive on large images, so it's better to do detection on
        the smaller display image and scale the corners back up to the correct size.
        Returns (scrib, corners, downsampled_corners, board, (x_scale, y_scale)).
        */
        // Scale the input image down to ~VGA size
        double height = image.rows;
        double width = image.cols;
        double scale = sqrt( (width*height) / (1024*768) ); //640*480 //1024*768
        cv::Mat image_downsample;
        if(scale > 1.0){
            resize(image, image_downsample, Size(int(width / scale), int(height / scale)));
        }
        else{
            image.copyTo(image_downsample);
        }
        // Due to rounding, actual horizontal/vertical scaling may differ slightly
        double x_scale = float(width) / image_downsample.cols;
        double y_scale = float(height) / image_downsample.rows;
        vector<Point2f> pointCorners;
        //int count;
        int min_number_of_corners=0;


        int count = 0;


        // Allocate memory
        CvPoint2D32f* image_points_buf	= 0;
        int elem_size;
        CvMemStorage* storage;
        elem_size = boardSize.width*boardSize.height*sizeof(image_points_buf[0]);
        storage = cvCreateMemStorage( MAX( elem_size*4, 1 << 16 ));
        image_points_buf = (CvPoint2D32f*)cvAlloc( elem_size );

        std::vector<Point2f> tmpcorners(boardSize.width*boardSize.height+1);

        CvMat c_image = image_downsample;

        int found = cvFindChessboardCorners3( &c_image, boardSize,(CvPoint2D32f*)&tmpcorners[0], &count, min_number_of_corners );
        //cout << "Found: "<< found << endl;
        if( found==1 )
        {
            cout << "Found corners" << endl;
            //std::vector<Point2f> tmpcorners(count+1);
            //(CvPoint2D32f*)&tmpcorners[0]=image_points_buf;
            tmpcorners.resize(count);
            cv::Mat(tmpcorners).copyTo(pointCorners);
            //Mat(image_points_buf).copyTo(pointCorners);
        }
        else{
          cout << "Not found" << endl;
        }
        //&(pointCorners.at(0)=image_points_buf;
        //cout << pointCorners << endl;

        //bool found = findChessboardCorners(image_downsample, boardSize, pointCorners,CALIB_CB_NORMALIZE_IMAGE );//+ CV_CALIB_CB_ADAPTIVE_THRESH);

        if(found==1){
            if(correct_corners){
            // Check if found checkerboard is in the correct orientation
            // Distance of the top left corner of checkerboard should be the smallest

                // Compute the distance to all corners
                float d_top_left = pointCorners[0].x*pointCorners[0].x + pointCorners[0].y*pointCorners[0].y;
                int corner_id = boardSize.width-1;
                float d_top_right = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;
                corner_id = boardSize.width*(boardSize.height-1);
                float d_bottom_left = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;
                corner_id = (boardSize.width*boardSize.height)-1;
                float d_bottom_right = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;

                // Check if the smallest distance is at top left (usually its rotated 90degrees)
                // In worst case rotate corners
                if (d_top_left > d_bottom_left){
                    vector<Point2f> pointCorners_corrected(pointCorners.size());
                    int b_c = 0;
                    for(int b_w=0;b_w<boardSize.width;b_w++){
                        for(int b_h=boardSize.height-1;b_h>=0;b_h--){
                            pointCorners_corrected[b_c] = pointCorners[b_h*boardSize.width + b_w];
                            b_c++;
                        }
                    }
                    pointCorners=pointCorners_corrected;
                    cout << "Rotated corners"<<endl;
                }
                else if (d_top_left > d_top_right){
                    vector<Point2f> pointCorners_corrected(pointCorners.size());
                    int b_c = 0;
                    for(int b_w=boardSize.width-1;b_w>=0;b_w--){
                        for(int b_h=0;b_h<boardSize.height;b_h++){
                            pointCorners_corrected[b_c] = pointCorners[b_h*boardSize.width + b_w];
                            b_c++;
                        }
                    }
                    pointCorners=pointCorners_corrected;
                    cout << "Rotated corners B"<<endl;
                }
            }

            //Find the corners in the original size
            for(std::vector<int>::size_type j = 0; j != pointCorners.size(); j++) {
                pointCorners[j].x*=x_scale;
                pointCorners[j].y*=y_scale;
            }
            //Refine detection
            cv::Mat imageGray;
            cvtColor(image, imageGray, COLOR_BGR2GRAY);
            cornerSubPix(imageGray, pointCorners, Size(20,20), Size(-1,-1),TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
            //Push them to vector list
            imagePoints.push_back(pointCorners);
            // Draw the corners.
            drawChessboardCorners(image, boardSize, pointCorners, found);

            if(save_corner_images){
                string out_file=string( list[i].substr(0,list[i].size()-extension.size()-1)+"_corners."+extension);
                cout << "Writing " << out_file << endl;
                imwrite(out_file, image);
            }

        }
        namedWindow(list[i], WINDOW_NORMAL );
        resizeWindow(list[i], int(width / scale), int(height / scale));
        imshow(list[i], image);
        waitKey();
        destroyWindow(list[i]);
	}

    //================== Calibrate camera

    //Prepare 3d points of the grid.

    vector<vector<Point3f> > objectPoints(1);
    objectPoints[0].clear();
    for( int i = 0; i < boardSize.height; ++i ){
        for( int j = 0; j < boardSize.width; ++j ){
            objectPoints[0].push_back(Point3f(float( j*squareSize )-width, float( i*squareSize )-height, 0));
        }
    }
    objectPoints.resize(imagePoints.size(),objectPoints[0]);

    cv::Mat cameraMatrix, distCoeffs;
    cv::Mat newCameraMatrix;
    double rms,h_fov,v_fov,d_fov;
    cout << "About to start calibration. It might last a while" << endl;
    Mat map1,map2;

    if(fisheye){

        int calib_flags = 0;
        if (num_ks < 4) calib_flags += fisheye::CALIB_FIX_K4;
        if (num_ks < 3) calib_flags += fisheye::CALIB_FIX_K3;
        if (num_ks < 2) calib_flags += fisheye::CALIB_FIX_K2;
        if (num_ks < 1) calib_flags += fisheye::CALIB_FIX_K1;
        calib_flags+= fisheye::CALIB_FIX_SKEW;
        calib_flags+=fisheye::CALIB_RECOMPUTE_EXTRINSIC;
        calib_flags+=fisheye::CALIB_CHECK_COND;
        //calib_flags+=fisheye::CALIB_FIX_ASPECT_RATIO;


        vector<Vec3d> rvecs, tvecs;
        rms = fisheye::calibrate(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, calib_flags);

        //Get All inner points
        //Rectification
        cv::fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imageSize, cv::noArray(), newCameraMatrix, 0.0);

        // Compute FOV
        h_fov=2*atan2(imageSize.width/2,newCameraMatrix.at<double>(0,0))*180/3.1416;
        v_fov=2*atan2(imageSize.height/2,newCameraMatrix.at<double>(1,1))*180/3.1416;
        d_fov = sqrt(h_fov*h_fov+v_fov*v_fov);

        //Compute FOV new method.
        cv::Mat border_left(10,1,CV_32FC2);
        cv::Mat border_left_undist(10,1,CV_32FC2);
        for( int i = 0; i < 10; ++i ){ //Take 10 points along corners
            int u=0;
            int v=i*imageSize.height/10.0;
            border_left.ptr<float>(i)[0]=(float)u;
            border_left.ptr<float>(i)[1]=(float)v;
        }
        cv::fisheye::undistortPoints(border_left, border_left_undist, cameraMatrix, distCoeffs);
        vector<Mat> u_values(2);
        split(border_left_undist, u_values);
        double max_u;
        cv::minMaxLoc(u_values[0], NULL, &max_u);
        h_fov=abs(2*atan(max_u)*180/3.1416);

        cv::Mat border_top(11,1,CV_32FC2);
        cv::Mat border_top_undist(10,1,CV_32FC2);
        for( int i = 0; i < 10; ++i ){ //Take 10 points along corners
            int u=i*imageSize.width/10.0;
            int v=0;
            border_top.ptr<float>(i)[0]=(float)u;
            border_top.ptr<float>(i)[1]=(float)v;
        }
        cv::fisheye::undistortPoints(border_top, border_top_undist, cameraMatrix, distCoeffs);
        vector<Mat> v_values(2);
        split(border_top_undist, v_values);
        double max_v;
        cv::minMaxLoc(v_values[1], NULL, &max_v);
        v_fov=abs(2*atan(max_v)*180/3.1416);

        d_fov = sqrt(h_fov*h_fov+v_fov*v_fov);

        //Get All inner points
        //Rectification
        cv::fisheye::estimateNewCameraMatrixForUndistortRectify(cameraMatrix, distCoeffs, imageSize, cv::noArray(), newCameraMatrix, 1.0);
        fisheye::initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat::eye(3, 3, CV_64F), newCameraMatrix, imageSize, CV_16SC2, map1, map2);


        //Try to find corners in undistorted images to improve calibration

        for(vector<string>::size_type i = 0; i != list.size(); i++) {
            cout << "Reading " << list[i] << endl;
              cv::Mat image = imread(list[i], IMREAD_COLOR);
              imageSize=image.size();
              cv::Mat imageRect;
              remap(image, imageRect, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);

              /*
              Downsample the input image to approximately VGA resolution and detect the
              calibration target corners in the full-size image.
              Combines these apparently orthogonal duties as an optimization. Checkerboard
              detection is too expensive on large images, so it's better to do detection on
              the smaller display image and scale the corners back up to the correct size.
              Returns (scrib, corners, downsampled_corners, board, (x_scale, y_scale)).
              */
              // Scale the input image down to ~VGA size
              double height = image.rows;
              double width = image.cols;
              double scale = sqrt( (width*height) / (640.*480.) ); //640*480 //1024*768
              cv::Mat image_downsample;
              if(scale > 1.0){
                  resize(imageRect, image_downsample, Size(int(width / scale), int(height / scale)));
              }
              else{
                  imageRect.copyTo(image_downsample);
              }
              // Due to rounding, actual horizontal/vertical scaling may differ slightly
              double x_scale = float(width) / image_downsample.cols;
              double y_scale = float(height) / image_downsample.rows;
              vector<Point2f> pointCorners;
              //int count;
              int min_number_of_corners=0;


              int count = 0;


              // Allocate memory
              CvPoint2D32f* image_points_buf	= 0;
              int elem_size;
              CvMemStorage* storage;
              elem_size = boardSize.width*boardSize.height*sizeof(image_points_buf[0]);
              storage = cvCreateMemStorage( MAX( elem_size*4, 1 << 16 ));
              image_points_buf = (CvPoint2D32f*)cvAlloc( elem_size );

              std::vector<Point2f> tmpcorners(boardSize.width*boardSize.height+1);

              CvMat c_image = image_downsample;

              int found = cvFindChessboardCorners3( &c_image, boardSize,(CvPoint2D32f*)&tmpcorners[0], &count, min_number_of_corners );
              //cout << "Found: "<< found << endl;
              if( found==1 )
              {
                  cout << "Found corners" << endl;
                  //std::vector<Point2f> tmpcorners(count+1);
                  //(CvPoint2D32f*)&tmpcorners[0]=image_points_buf;
                  tmpcorners.resize(count);
                  cv::Mat(tmpcorners).copyTo(pointCorners);
                  //Mat(image_points_buf).copyTo(pointCorners);
              }
              else{
                cout << "Not found" << endl;
              }
              //&(pointCorners.at(0)=image_points_buf;
              //cout << pointCorners << endl;

              //bool found = findChessboardCorners(image_downsample, boardSize, pointCorners,CALIB_CB_NORMALIZE_IMAGE );//+ CV_CALIB_CB_ADAPTIVE_THRESH);

              if(found==1){
                  if(correct_corners){
                  // Check if found checkerboard is in the correct orientation
                  // Distance of the top left corner of checkerboard should be the smallest

                      // Compute the distance to all corners
                      float d_top_left = pointCorners[0].x*pointCorners[0].x + pointCorners[0].y*pointCorners[0].y;
                      int corner_id = boardSize.width-1;
                      float d_top_right = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;
                      corner_id = boardSize.width*(boardSize.height-1);
                      float d_bottom_left = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;
                      corner_id = (boardSize.width*boardSize.height)-1;
                      float d_bottom_right = pointCorners[corner_id].x*pointCorners[corner_id].x + pointCorners[corner_id].y*pointCorners[corner_id].y;

                      // Check if the smallest distance is at top left (usually its rotated 90degrees)
                      // In worst case rotate corners
                      if (d_top_left > d_bottom_left){
                          vector<Point2f> pointCorners_corrected(pointCorners.size());
                          int b_c = 0;
                          for(int b_w=0;b_w<boardSize.width;b_w++){
                              for(int b_h=boardSize.height-1;b_h>=0;b_h--){
                                  pointCorners_corrected[b_c] = pointCorners[b_h*boardSize.width + b_w];
                                  b_c++;
                              }
                          }
                          pointCorners=pointCorners_corrected;
                          cout << "Rotated corners"<<endl;
                      }
                      else if (d_top_left > d_top_right){
                          vector<Point2f> pointCorners_corrected(pointCorners.size());
                          int b_c = 0;
                          for(int b_w=boardSize.width-1;b_w>=0;b_w--){
                              for(int b_h=0;b_h<boardSize.height;b_h++){
                                  pointCorners_corrected[b_c] = pointCorners[b_h*boardSize.width + b_w];
                                  b_c++;
                              }
                          }
                          pointCorners=pointCorners_corrected;
                          cout << "Rotated corners B"<<endl;
                      }
                  }

                  //Find the corners in the original size
                  for(std::vector<int>::size_type j = 0; j != pointCorners.size(); j++) {
                      pointCorners[j].x*=x_scale;
                      pointCorners[j].y*=y_scale;
                  }
                  //Refine detection
                  cv::Mat imageGray;
                  cvtColor(image, imageGray, COLOR_BGR2GRAY);
                  cornerSubPix(imageGray, pointCorners, Size(20,20), Size(-1,-1),TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
                  //Push them to vector list
                  imagePoints.push_back(pointCorners);
                  // Draw the corners.
                  drawChessboardCorners(imageRect, boardSize, pointCorners, found);

                  if(save_corner_images){
                      string out_file=string( list[i].substr(0,list[i].size()-extension.size()-1)+"_corners."+extension);
                      cout << "Writing " << out_file << endl;
                      imwrite(out_file, image);
                  }

              }
              namedWindow(list[i], WINDOW_NORMAL );
              resizeWindow(list[i], int(width / scale), int(height / scale));
              imshow(list[i], imageRect);
              waitKey();
              destroyWindow(list[i]);
        }

          //================== Calibrate camera

          //Prepare 3d points of the grid.

          vector<vector<Point3f> > objectPoints(1);
          objectPoints[0].clear();
          for( int i = 0; i < boardSize.height; ++i ){
              for( int j = 0; j < boardSize.width; ++j ){
                  objectPoints[0].push_back(Point3f(float( j*squareSize )-width, float( i*squareSize )-height, 0));
              }
          }
          objectPoints.resize(imagePoints.size(),objectPoints[0]);

          cv::Mat cameraMatrix, distCoeffs;
          cv::Mat newCameraMatrix;
          double rms,h_fov,v_fov,d_fov;
          cout << "About to start calibration. It might last a while" << endl;
          Mat map1,map2;

    }
    else{
        vector<Mat> rvecs, tvecs;
        int calib_flags = 0;
        if (num_ks > 3) calib_flags += CALIB_RATIONAL_MODEL;
        if (num_ks < 6) calib_flags += CALIB_FIX_K6;
        if (num_ks < 5) calib_flags += CALIB_FIX_K5;
        if (num_ks < 4) calib_flags += CALIB_FIX_K4;
        if (num_ks < 3) calib_flags += CALIB_FIX_K3;
        if (num_ks < 2) calib_flags += CALIB_FIX_K2;
        if (num_ks < 1) calib_flags += CALIB_FIX_K1;
        calib_flags = +CALIB_FIX_ASPECT_RATIO;
        //calib_flags = +CV_CALIB_ZERO_TANGENT_DIST;
                
        rms=calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, calib_flags);

        // Compute FOV
        h_fov=2*atan2(imageSize.width/2,cameraMatrix.at<double>(0,0))*180/3.1416;
        v_fov=2*atan2(imageSize.height/2,cameraMatrix.at<double>(1,1))*180/3.1416;
        d_fov = sqrt(h_fov*h_fov+v_fov*v_fov);


        //Get All inner points
        //Rectification
        newCameraMatrix=getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1.0, imageSize);
        initUndistortRectifyMap(cameraMatrix, distCoeffs,cv::Mat(), newCameraMatrix,imageSize, CV_16SC2, map1, map2);
    }

    cout << "========= Calibration finished. ===============" << endl;
    if(fisheye)
        cout << "FISHEYE LENSES!" << endl;
    cout << "===============================================" << endl;
    cout << "Image size: "<<imageSize<<endl;
    cout << "Final RMS: " << rms << endl;
    cout << "===============================================" << endl;
    cout << "Camera Matrix is: " << endl;
    cout << cameraMatrix << endl;
    cout << "Distortion coefficients are:" << endl;
    cout << distCoeffs << endl;
    cout << "===============================================" << endl;
    cout << "Camera Matrix (Alpha 1) is: " << endl;
    cout << newCameraMatrix << endl;
    cout << "===============================================" << endl;
    cout << "Fields of view are: " << endl;
    cout << "Horizontal: " << h_fov << " . Vertical: " << v_fov << " . Diagonal: " << d_fov<< endl;

    cout << endl;
    cout << "You should check:" << endl;
    cout << "1- All corners of the images have been detected properly. Check the output images. If not, remove the conflictive images." << endl;
    cout << "2- The rectified images show straight lines instead of curvy ones." << endl;


    //================= SAVE CALIBRATION ==================

    cout << endl;

    cout << "Do you want to save calibration [Y]/N)?\n";
    cin >> k;

    if(k!='N'){
        ofstream calib_out(dir_path.append("/calib.txt").c_str());

        calib_out << "========= Calibration finished. ===============" << endl;
        if(fisheye) calib_out << "FISHEYE LENSES!" << endl;
        calib_out << "===============================================" << endl;
        calib_out << "Image size: "<<imageSize<<endl;
        calib_out << "Final RMS: " << rms << endl;
        calib_out << "===============================================" << endl;
        calib_out << "Camera Matrix is: " << endl;
        calib_out << cameraMatrix << endl;
        calib_out << "Distortion coefficients are:" << endl;
        calib_out << distCoeffs << endl;
        calib_out << "===============================================" << endl;
        calib_out << "Camera Matrix (Alpha 1) is: " << endl;
        calib_out << newCameraMatrix << endl;
        calib_out << "===============================================" << endl;
        calib_out << "Fields of view are: " << endl;
        calib_out << "Horizontal: " << h_fov << " . Vertical: " << v_fov << " . Diagonal: " << d_fov<< endl;
        calib_out.close();


        cv::FileStorage fs("calibration.xml", cv::FileStorage::WRITE);

		time_t rawtime; time(&rawtime);
		fs << "calibrationDate" << asctime(localtime(&rawtime));
		fs << "imageSize" << imageSize;
		if(fisheye){
			fs << "fisheye" << "1" ;
		}
		else{
			fs << "fisheye" << "0" ;
		}
		fs << "cameraMatrix" << cameraMatrix << "distCoeffs" << distCoeffs;
		fs.release();
    }

    //================= RECTIFY IMAGES==================

    cout << "Do you want to save rectified images [Y]/N)?\n";
    cin >> k;
    bool save_rectified_images=true;
    if(k=='N'){
        save_rectified_images=false;
    }


    Mat imageRect;
    for(vector<string>::size_type i = 0; i != list.size(); i++) {
        cv::Mat image = imread(list[i], IMREAD_COLOR);
        remap(image, imageRect, map1, map2, INTER_LINEAR, BORDER_CONSTANT, 0);
        namedWindow(list[i], WINDOW_NORMAL );
        resizeWindow(list[i], 640, 480);
        imshow(list[i], imageRect);
        waitKey(50);
        destroyWindow(list[i]);
        if(save_rectified_images){
            string out_file=string( list[i].substr(0,list[i].size()-extension.size()-1)+"_rect."+extension);
            cout << "Writing " << out_file << endl;
            imwrite(out_file, imageRect);
        }
    }
    return 0;
}
