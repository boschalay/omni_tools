#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width individual_depth_map calibration_file last_camera\n",argv[0]);
}

bool read_depth( string depth_file, int& width, std::vector<float>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
    }
    return true;
}

bool read_depth_binary( string depth_file, int& width, std::vector<float>&  data)
{


    std::ifstream fin(depth_file.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cout << " Error, Couldn't find the file" << "\n";
        return false;
    }
    else{
        cout << "Depth map found" << endl;
    }

    fin.seekg(0, std::ios::end);
    const size_t num_elements = fin.tellg() / sizeof(float);
    fin.seekg(0, std::ios::beg);

    float w,h;

    fin.read(reinterpret_cast<char*>(&w), sizeof(float));
    fin.read(reinterpret_cast<char*>(&h), sizeof(float));

    width=(int)round(w);

    cout << "Depth map Width:" << width << endl;
    cout << "Depth map Height:" << h << endl;

    data.resize(num_elements-2);
    fin.read(reinterpret_cast<char*>(&data[0]), (num_elements-2)*sizeof(float));

    return true;
}

int main(int argc, char *argv[])
{

    if(argc < 5 )
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    int last_camera=atoi(argv[4]);
    cout << "Last camera is: " << last_camera << endl;

    cv::Mat adjMap;

    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[3]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=1.0;
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();


    std::vector<float> depthmapdata[last_camera+1];
    cv::Mat individualDepthMap[last_camera+1];

   //Read all depthmaps
    string depthmap0=string(argv[2]);
    int cameraindex = depthmap0.rfind("camera");
    int first_camera= atoi(depthmap0.substr(cameraindex+6,1).c_str());
    string depthfile_part1 = depthmap0.substr(0, cameraindex+6);
    //Find now the frame. The numbering of the frame is 5 digits.
    string depthfile_part2 = depthmap0.substr(cameraindex+7);
    stringstream ss;
    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        string depthmap_path;
        depthmap_path.clear();
        ss << depthfile_part1 << cam << depthfile_part2;
        depthmap_path=ss.str();
        cout << "Depth map " << depthmap_path << endl;
        int width_depth_map;

        string file_extension=depthmap_path.substr(depthmap_path.rfind(".")+1);
        cout << "File extension " << file_extension << endl;

        individualDepthMap[cam]=cv::Mat::zeros(imageHeight,imageWidth,CV_32F);
        if(file_extension=="bin"){
          read_depth_binary(depthmap_path,width_depth_map, depthmapdata[cam]);
          memcpy(individualDepthMap[cam].data, depthmapdata[cam].data(), depthmapdata[cam].size()*sizeof(float));
        }
        else if(file_extension=="exr"){
          cv::Mat exrMat=cv::imread(depthmap_path,CV_LOAD_IMAGE_ANYDEPTH);
          Mat bgr[3];   //destination array
          split(exrMat,bgr);//split source
          individualDepthMap[cam]=bgr[0].clone();
        }

        individualDepthMap[cam].convertTo(individualDepthMap[cam],CV_64F);
    }

    cout << "All depthmaps opened succesfully. About to start panoramic image" << endl;


    double R=calibration.getSphereRadius();
    int result;

    //Panoramic image
    cv::Mat panoramic;
    panoramic=cv::Mat::zeros(pano_height,pano_width,CV_64FC1);

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(y = 0; y< pano_height; y++){
        double dRow[last_camera];
        std::fill_n(dRow, numberCameras,-1);
        double dCol[last_camera];
        std::fill_n(dCol, numberCameras,-1);
        int x;
        for(x = 0; x <pano_width ; x++) {
            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=-(2*PI*x/(pano_width-1))+PI;// To rotate 180: -PI;
            double phi=PI*y/(pano_height-1);

            //From theta and phi find the 3D coordinates.
            double dist=R;
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            float minDistanceCenter=pano_width*pano_width; // Max value possible.
            int cameraMinDistanceCenter=-1;
            int covered=0;
            bool parallax=false;
            int result_;

            //From the 3D coordinates, find in how many camera falls the point!
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    //Check if distance of individual depth map matches.
                    cv::Mat PointGlobal(4,1,CV_64F);
                    PointGlobal.at<double>(0,0)=globalX;
                    PointGlobal.at<double>(1,0)=globalY;
                    PointGlobal.at<double>(2,0)=globalZ;
                    PointGlobal.at<double>(3,0)=1.0;
                    cv::Mat PointLocal;
                    calibration.multiCam3DPoint2camera3dPoint(PointGlobal, cam, PointLocal);
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                }
                else{
                    dRow[cam]=-1;
                    dCol[cam]=-1;
                }
            }
            if(covered>0){
                panoramic.at<double>(y,x)= individualDepthMap[cameraMinDistanceCenter].at<double>(dRow[cameraMinDistanceCenter],dCol[cameraMinDistanceCenter]);
            }
            else{
                panoramic.at<double>(y,x)=0.0;
            }
        }

        done[y]=true;

        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;

        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;

    }

    //Display
    /*cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Display window", panoramic );
    cv::waitKey();
    */

    string pano_filename;
    ss.str("");
    pano_filename.clear();
    int indexframe = depthmap0.rfind("frame");
    string first_frame_str = depthmap0.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    ss << "equi_depth_map_frame" << setfill('0')<< setw(5) << first_frame << ".png";
    pano_filename=ss.str();

    std::vector<int> qualityType;
    cout << pano_filename << endl;


    double min, max;
    cv::minMaxLoc(panoramic, &min, &max);
    cout << "Max" << max << endl;

    cv::convertScaleAbs(panoramic, adjMap, 255 / max);


    bool writing_success;
    writing_success=imwrite(pano_filename, adjMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    else{
      cout << "Image written in " << pano_filename << endl;
    }

    cout << "Success!" << endl;

    ss.str("");
    ss << "equi_depth_map_frame" << setfill('0')<< setw(5) << first_frame << ".exr";
    string filename=ss.str();
    cv::Mat temp;
    panoramic.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    else{
      cout << "Image written in " << filename << endl;
    }
    cout << "Success!" << endl;

    return 0;
}
