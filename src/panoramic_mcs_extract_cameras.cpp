#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp" 

#define PI 3.14159265358979323846

using namespace cv; 
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{      
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl; 
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width image_colour_camera0 calibration_file radius last_camera <EquiDepthMap> <IndividualDepthMap>\n",argv[0]);
}

bool read_depth( string depth_file, int& width, std::vector<double>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
    }
    return true;
}

int main(int argc, char *argv[])
{

    if(argc < 6 || argc > 8)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    string image0=string(argv[2]);
    int cameraindex = image0.rfind("camera");
    string file_part0 = image0.substr(0, cameraindex); 
    string file_part1 = image0.substr(0, cameraindex+6); 
    int first_camera= atoi(image0.substr(cameraindex+6,1).c_str());
    //Find now the frame. The numbering of the frame is 5 digits.
    int indexframe = image0.rfind("frame");
    string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
    string first_frame_str = image0.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    string file_part3 = image0.substr(indexframe+10);
    int last_camera=atoi(argv[5]);
    
    cout << "Arguments: " << argc << endl;
    cv::Mat scaledDepthMap, adjMap;
    bool haveDepthMap=false;
    
    
    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[3]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=atof(argv[4]);
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();
    
    
    std::vector<double> depthmapdata[last_camera+1];
    cv::Mat individualDepthMap[last_camera+1];
    
    if(argc>7){
        haveDepthMap=true;
        cout << "Depth map" << endl;
        string depth_map=string(argv[6]);
        int width_depth_map;
        std::vector<double> data;
        read_depth(depth_map,width_depth_map, data);
        cout << width_depth_map << endl;
        
        #warning      
        //cv::Mat depthMap(width_depth_map/2,width_depth_map,CV_64F);
        cv::Mat depthMap(width_depth_map/2,width_depth_map,CV_64F);
        memcpy(depthMap.data, data.data(), data.size()*sizeof(double));
        
        depthMap=1.2*depthMap;
        
        
        double min, max;
        cv::minMaxLoc(depthMap, &min, &max);
        
        cout << "Max" << max << endl;
        
        //cv::Mat adjMap;
        cv::convertScaleAbs(depthMap, adjMap, 255 / max);
        
        cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map",adjMap);
        cv::waitKey(20);
        
        stringstream ss;
        string depth_filename;
        ss.str("");
        ss.clear();
        ss << file_part0 << "depth_map.png";
        depth_filename=ss.str();
        
        std::vector<int> qualityType;
        //qualityType.push_back(IMWRITE_JPEG_QUALITY);
        //qualityType.push_back(85); //Jpeg quality

        cout << depth_filename << endl;    

        bool writing_success;
        writing_success=imwrite(depth_filename, adjMap, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
            return 1;
        }
        

        cv::resize(depthMap, scaledDepthMap, cv::Size(pano_width,pano_height),0,0,cv::INTER_NEAREST );
        cv::convertScaleAbs(scaledDepthMap, adjMap, 255 / max);
        
        cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map",adjMap);
        cv::waitKey(20);        
        
    
        //Read all depthmaps    
        string depthmap0=string(argv[7]);
        cameraindex = depthmap0.rfind("camera");
        string depthfile_part1 = depthmap0.substr(0, cameraindex+6); 
        first_camera= atoi(depthmap0.substr(cameraindex+6,1).c_str());
        //Find now the frame. The numbering of the frame is 5 digits.
        string depthfile_part2 = depthmap0.substr(cameraindex+7);
        last_camera=atoi(argv[6]);
        
        
        for(int cam = first_camera; cam <= last_camera; cam++) {
            ss.str("");
            string depthmap_path;
            depthmap_path.clear();
            ss << depthfile_part1 << cam << depthfile_part2;
            depthmap_path=ss.str();
            cout << "Depth map " << depthmap_path << endl;
            imread(depthmap_path, IMREAD_COLOR);
            int width_depth_map;
            read_depth(depthmap_path,width_depth_map, depthmapdata[cam]);
           
            //cv::Mat depthMap(width_depth_map/2,width_depth_map,CV_64F);
            individualDepthMap[cam]=cv::Mat::zeros(imageHeight,imageWidth,CV_64F);
            memcpy(individualDepthMap[cam].data, depthmapdata[cam].data(), depthmapdata[cam].size()*sizeof(double));
            individualDepthMap[cam]=1.2*individualDepthMap[cam];
        }   
    }

    //Read the images.

    Mat image[last_camera+1];
    string image_path;
    stringstream ss;

    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        image_path.clear();
        ss << file_part1 << cam << file_part2 << setfill('0')<< setw(5) << first_frame<< file_part3;
        image_path=ss.str();
        image[cam] = imread(image_path, IMREAD_COLOR);
        if(image[cam].data==NULL){
            cout << "Error reading image: "<< image_path << endl;
            return 1;
        }
    }   
    
    cout << "All images opened succesfully. About to start panoramic image" << endl;


    double R=calibration.getSphereRadius();
    int result;

    //Panoramic table
	cv::Mat panoramic[6];
    //Fundamental mat.
    for(int cam = 0; cam<= last_camera; cam++){
        panoramic[cam] =cv::Mat::zeros(pano_height,pano_width,CV_64FC3);
	}

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    #pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration) 
    for(y = 0; y< pano_height; y++){
        double dRow[last_camera];
        std::fill_n(dRow, numberCameras,-1);
        double dCol[last_camera];
        std::fill_n(dCol, numberCameras,-1);
        int x;
        for(x = 0; x <pano_width ; x++) {
            double globalX;
            double globalY;
            double globalZ;
                                               
            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=-(2*PI*x/(pano_width-1))+PI;// To rotate 180: -PI;
            double phi=PI*y/(pano_height-1);

            double dist=R;
            if(haveDepthMap){
                if(scaledDepthMap.at<double>(y,x)!=0){
                    dist=scaledDepthMap.at<double>(y,x);
                }
            }

            //From theta and phi find the 3D coordinates.
            globalZ=dist*cos(phi);
            globalX=dist*sin(phi)*cos(theta);
            globalY=dist*sin(phi)*sin(theta);

            float minDistanceCenter=pano_width*pano_width; // Max value possible.
            int cameraMinDistanceCenter=-1;
            int covered=0;
            int result_;

            //From the 3D coordinates, find in how many camera falls the point!
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                }
                else{
                    dRow[cam]=-1;
                    dCol[cam]=-1;
                }
            }
            if(covered){
                //cv::Vec3b intensity =image[cameraMinDistanceCenter].at<cv::Vec3b>(dRow[cameraMinDistanceCenter],dCol[cameraMinDistanceCenter]);
                panoramic[cameraMinDistanceCenter].ptr<double>(y,x)[0]=dRow[cameraMinDistanceCenter];
                panoramic[cameraMinDistanceCenter].ptr<double>(y,x)[1]=dCol[cameraMinDistanceCenter];
                panoramic[cameraMinDistanceCenter].ptr<double>(y,x)[2]=1.0;
            }
            
            
            /*
            if(covered>0){
                for(cam = first_camera; cam<= last_camera; cam++){
                    result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                    if (result_!=-1) {
                        cv::Vec3b intensity =image[cam].at<cv::Vec3b>(dRow[cam],dCol[cam]);
                        panoramic.at<cv::Vec3b>(y,x)=panoramic.at<cv::Vec3b>(y,x)+intensity*1.0/covered;
                    }
                }

            }

            
            bool covered_border=false;

            //From the 3D coordinates, find in how many camera falls the point!
            for(cam = first_camera; cam<= last_camera; cam++){
                result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1 && (dRow[cam]<50 || dRow[cam]>(imageHeight-50)|| dCol[cam]<50 || dCol[cam] > (imageWidth-50))) {
                    covered_border=true;
                }
                else{
                    dRow[cam]=-1;
                    dCol[cam]=-1;
                }
            }
            if(covered_border){
                panoramic.at<cv::Vec3b>(y,x)=cv::Vec3b(65,72,244);
            }
            * */
        
        
        
        }
        
        done[y]=true;
        
        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        //std::cout << numberDone << std::flush;
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;

        //done[y]=true;
        //std::cout << (int) ((float)(std::count(done, std::end(done), true))/(float)(pano_height)*100) << "%" << "\r" << std::flush;
        //std::cout << (int) ((float)(y*pano_width)/(float)(pano_height*pano_width)*100) << "%" << "\r" << std::flush;
                
    }

    FILE * pFile;
	string filename;

	ss.str("");
    ss << "pano_" << pano_width << "_radius"<< radius << ".txt";
	filename=ss.str();
    pFile = fopen (filename.c_str(),"w");

	fprintf(pFile, "cols %d rows %d\n", pano_width, pano_height);

	for(int cam = 0; cam!= 6; cam++){
		for(int y = 0; y!= pano_height; y++){
			for(int x = 0; x !=pano_width ; x++) {
                int R= (int) panoramic[cam].ptr<double>(y,x)[0];
                int C= (int) panoramic[cam].ptr<double>(y,x)[1];
                double alpha=panoramic[cam].ptr<double>(y,x)[2];
                fprintf (pFile, "%d,%d,%4.4lf\n", R, C, alpha);
			}
		}
	}

    fclose (pFile);
	cout << "Success!" << endl;
    cout << "Mesh file written in "<< filename << endl;

	cout << "Success!" << endl;

    return 0;
}
