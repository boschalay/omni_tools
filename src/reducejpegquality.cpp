#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>

using namespace cv; 
using namespace std;

int main(int argc, char *argv[])
{

	if (argc!=5) {
        std::cerr << "Usage: ./reducejpegquality <InputDir> <OuputDir> <Nframes-1> <Quality>"  << std::endl;
        std::cerr << "This will change the qualityy of the jpeg to the specified value. If want to convert all the images in the directory just enter a large numer. ex:99999" << std::endl;
        return 1;
	}

	string inputdir=string(argv[1]); //Relative path to the directory
	string::iterator it = inputdir.end() - 1;
	if (*it == '/'){
        inputdir.erase(it);
	}
	string outputdir=string(argv[2]); 
	it = outputdir.end() - 1;
	if (*it == '/'){
        outputdir.erase(it);
	}
	int nframes=atoi(argv[3]); //Nframes+1
	int quality=atoi(argv[4]); //Quality

    bool reading_error=false;
	int frame, cam,k ;
	char file[150];
	bool writing_success;
	for (frame=0;frame<nframes;frame++) {
		for (cam=0;cam<6;cam++) {
            reading_error=false;
			Mat imgMat[4]; //height, width
		 	for (k=0;k<4;k++) {
				sprintf (file, "%s/camera%i_frame%05d_%i.jpg",inputdir.c_str(),cam,frame,k);
				imgMat[k] = cv::imread(file, IMREAD_GRAYSCALE);
				if(imgMat[k].data==NULL){
					printf("Error reading image: %s!Check names and directory existence\n", file);
                    reading_error=true;
                    //return 1;
				}
                else{
                    //Save the image before exiting.
                    sprintf (file, "%s/camera%i_frame%05d_%i.jpg",outputdir.c_str(),cam,frame,k);
                    std::vector<int> qualityType;
                    qualityType.push_back(IMWRITE_JPEG_QUALITY);
                    qualityType.push_back(quality); //Jpeg quality

                    writing_success=imwrite(file, imgMat[k], qualityType);
                    if(!writing_success){
                        cout << "\nError writing image! Check ouput directory existence." << endl;
                        return 1;
                    }
                    else{
                        printf("Writing image: %s\n",file); 
                    }
                }
                
			}
		}
		cout << frame << "/" << nframes << "\r" << flush;
	}

    return 0;

}
