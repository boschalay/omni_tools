#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s depth_map_txt pano_contribution_map.png\n",argv[0]);
}

int main(int argc, char *argv[])
{

    if(argc <3)
    {
        printhelp(argv);
        return(1);
    }


    string file_path=argv[1];

    cv::Mat depthMap, panoramic;
    depthMap=read_depthmap(file_path);
    int width_depth_map, height_depth_map;
    width_depth_map=depthMap.cols;
    height_depth_map=depthMap.rows;

    //We resize it to make things faster. Anyway we are smoothing it...
    float ratio=(float)height_depth_map/(float)width_depth_map;
    width_depth_map=1000;
    height_depth_map=1000.0*ratio;
    cv::resize(panoramic, panoramic, cv::Size(width_depth_map, height_depth_map),0,0,cv::INTER_NEAREST );

    cv::Mat pano_contribution_map;
    string contribution_map_path=string(argv[2]);
    pano_contribution_map=cv::imread(contribution_map_path, IMREAD_GRAYSCALE);
    if(pano_contribution_map.data==NULL){
        cout << "Error reading image: "<< contribution_map_path << endl;
        return 1;
    }

    cv::resize(pano_contribution_map, pano_contribution_map, cv::Size(width_depth_map, height_depth_map));

    int pano_width=width_depth_map;
    int pano_height=height_depth_map;



    cv::Mat panoramic_gradient=cv::Mat::zeros(pano_height,pano_width,CV_8U);
    //For each pixel of the contribution map we open a window and see if there is any value different than it.
    for(int col=0;col<pano_width;col++){
        for(int row=0;row<pano_height;row++){

            int halfsize=5; //Should be even.
            cv::Point top_left=cv::Point(std::max(col-halfsize,0),std::max(row-halfsize,0));
            cv::Point bottom_right=cv::Point(std::min(col+halfsize+1,pano_width),std::min(row+halfsize+1,pano_height));
            cv::Mat image_roi=pano_contribution_map(cv::Rect(top_left,bottom_right));
            cv::Mat nonZeroCoordinates;
            cv::findNonZero(image_roi!=pano_contribution_map.at<unsigned char>(row,col), nonZeroCoordinates);
            if(nonZeroCoordinates.total()>0){
                panoramic_gradient.zzat<unsigned char>(row,col)=(unsigned char)255;
            }
        }
    }


    /*

    //Now calculate gradient image according to sobel to extract borders of image contribution.
    //originalGrayscaleImage_.convertTo(tmp_original,CV_32F);
    // Gradient X
    cv::Mat Dx;
    Sobel( pano_contribution_map, Dx, CV_32F, 1, 0, 7);
    cv::Mat Dy;
    Sobel( pano_contribution_map, Dy, CV_32F, 0, 1, 7);
    cv::addWeighted( cv::abs(Dx), 0.5, cv::abs(Dy), 0.5, 0, panoramic_gradient ); //Approximation
    panoramic_gradient.convertTo(panoramic_gradient, CV_8U);
    cv::threshold(panoramic_gradient,panoramic_gradient,0,255,cv::THRESH_BINARY); //Convert panoramic_gradient from 0 to 1
    */

    //Image with ones where FOV is covered always
    cv::Mat covered_fov;
    cv::threshold(pano_contribution_map,covered_fov,254,255,cv::THRESH_BINARY_INV);
    covered_fov.convertTo(covered_fov, CV_8U);
    cv::Mat not_covered_fov=cv::Mat(covered_fov.rows, covered_fov.cols, CV_8U, cv::Scalar(255));
    not_covered_fov=not_covered_fov-covered_fov;

    //We have the gradient image wihch containts the borders of the individual contributions
    //Compute a dot grid
    cv::Mat dot_grid=cv::Mat::zeros(panoramic_gradient.rows,panoramic_gradient.cols,CV_8U);
    int num_rows= 24; //Number of vertical samples
    int num_cols= num_rows*width_depth_map/height_depth_map;
    for (int i=0;i<num_cols;i++){
        for(int j=0;j<num_rows;j++){
            cv::Point center=cv::Point(pano_contribution_map.cols/(num_cols)*(0.5+i),pano_contribution_map.rows/(num_rows)*(0.5+j));
            //Radius should be proportional as well
            int k=15; //Relation btween radius and grid height.
            int radius=pano_contribution_map.rows/(num_rows)/k;
            //cv::circle(dot_grid, center, radius, 255, -1);
        }
    }

    dot_grid=dot_grid-not_covered_fov;
    //Sum borders + dot grid;

    cv::Mat panoramic_mask=dot_grid+panoramic_gradient;

    //Generate depth image with only borders and dots information


    cv::resize(panoramic_mask, panoramic_mask, panoramic.size(),0,0,cv::INTER_NEAREST);

    panoramic_mask.convertTo(panoramic_mask,CV_64F);
    panoramic=panoramic.mul(panoramic_mask/255);

    //===========SAVE MASK FOR LOG==================
    double min,max;
    cv::Mat adjMap,colorDepthMap;
    cv::minMaxLoc( panoramic, &min, &max);
    cv::convertScaleAbs( panoramic, adjMap, 255 / (max-min), -min*255/(max-min));


    cv::convertScaleAbs( panoramic, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::Mat mask;
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);


    stringstream ss;
    string pano_filename;
    ss.str("");
    string file_without_extension=file_path.substr(0,file_path.rfind("."));
    ss << file_without_extension << "_filtered.png";
    pano_filename.clear();
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    bool writing_success;
    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    //=====================SMOOTH PANORAMA

    cv::Mat valid_pixels;
    cv::threshold(panoramic,valid_pixels,0,255,cv::THRESH_BINARY);
    valid_pixels.convertTo(valid_pixels, CV_8U);

    //cv::Mat tobeInpainted = covered_fov-valid_pixels;

    //EQUI
    cv::Mat tobeInpainted = cv::Mat::ones(panoramic.rows,panoramic.cols,CV_8U)-valid_pixels;
    //dome
    //cv::Mat tobeInpainted = cv::Mat::zeros(panoramic.rows,panoramic.cols,CV_8U);
    //cv::circle(tobeInpainted, cv::Point(panoramic.cols/2, panoramic.rows/2), panoramic.rows/2, 255,-1);
    //tobeInpainted=tobeInpainted-valid_pixels;


    cv::Mat(dot_grid/255.0).convertTo(dot_grid,CV_64F);
    cv::Mat(panoramic_gradient/255.0*2.0).convertTo(panoramic_gradient,CV_64F);
    cv::Mat weight_values= dot_grid+panoramic_gradient;
    cv::threshold(weight_values,weight_values,50,-1,cv::THRESH_TRUNC);

    cv::resize(weight_values, weight_values, panoramic.size(),0,0,cv::INTER_NEAREST);

    cv::Mat valid_pixels_double;
    cv::Mat(valid_pixels/255*1.0).convertTo(valid_pixels_double, CV_64F);
    weight_values=weight_values.mul(valid_pixels_double);


    if(SHOW_DEBUG_IMAGES){
        cv::minMaxLoc( weight_values, &min, &max);
        //cv::Mat adjMap;
        cv::convertScaleAbs( weight_values, adjMap, 255 / (max-min), -min*255/(max-min));

        cv::convertScaleAbs( weight_values, colorDepthMap,255 / (max-min), -min*255/(max-min));
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
        cv::Mat mask;
        cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
        colorDepthMap.setTo(Scalar(0,0,0), mask);


        cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map Smoothed",colorDepthMap);
        cv::waitKey();
        }


    cv::Mat beforeBlur;
    cv::Mat filled=regionfill2(panoramic,covered_fov,tobeInpainted,width_depth_map,weight_values,beforeBlur);
    panoramic=filled;
    //For dome If phi> PI -> Black



    cv::minMaxLoc( beforeBlur, &min, &max);
    //cv::Mat ;
    cv::convertScaleAbs( beforeBlur, adjMap, 255 / (max-min), -min*255/(max-min));
    cv::convertScaleAbs( beforeBlur, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);


    ss.str("");
    ss << file_without_extension << "_filtered_before_blur.png";
    pano_filename.clear();
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    cv::minMaxLoc( panoramic, &min, &max);
    //cv::Mat adjMap;
    cv::convertScaleAbs( panoramic, adjMap, 255 / (max-min), -min*255/(max-min));
    cv::convertScaleAbs( panoramic, colorDepthMap, 255 / (max-min), -min*255/(max-min));
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    if(SHOW_DEBUG_IMAGES){

        cv::namedWindow("Depth Map Smoothed", WINDOW_NORMAL | WINDOW_KEEPRATIO);
        cv::imshow("Depth Map Smoothed",colorDepthMap);
        cv::waitKey(10);
    }

    ss.str("");
    ss << file_without_extension << "_smoothed.png";
    pano_filename.clear();
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    cout << "Saving "<< pano_filename << endl;

    ss.str("");
    ss << file_without_extension << "_smoothed.exr";
    string filename=ss.str();
    cv::Mat temp;
    panoramic.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);

    cout << "Mesh file written in "<< filename << endl;



    /*
    //===============================SAVING TXT===============================
    FILE * pFile;
    string filename;

    ss.str("");
     ss << file_without_extension << "_smoothed.txt";
    filename=ss.str();
    pFile = fopen (filename.c_str(),"w");

    for(int v = 0; v< width_depth_map/2; v++){
        int u;
        for(u = 0; u <width_depth_map ; u++) {
            fprintf (pFile, "%4.4lf ", panoramic.at<double>(v,u));
        }
        fprintf (pFile, "\n");
    }

    fclose (pFile);
    */

    return 0;
}
