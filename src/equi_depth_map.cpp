#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>
#include <random>

#include <pcl/range_image/range_image.h>
#include <pcl/range_image/range_image_spherical.h>
#include <pcl/visualization/range_image_visualizer.h>
 #include <pcl/visualization/cloud_viewer.h>

//LadybugCalibration class
#include "mcs_calibration.hpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width calibration_file individual_depth_map_txt last_camera <pano_contribution_map>\n",argv[0]);
}

bool read_depth( string depth_file, int& width, std::vector<float>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
    }
    return true;
}

bool read_depth_binary( string depth_file, int& width, std::vector<float>&  data)
{


    std::ifstream fin(depth_file.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cout << " Error, Couldn't find the file" << "\n";
        return false;
    }

    fin.seekg(0, std::ios::end);
    const size_t num_elements = fin.tellg() / sizeof(float);
    fin.seekg(0, std::ios::beg);

    float w,h;

    fin.read(reinterpret_cast<char*>(&w), sizeof(float));
    fin.read(reinterpret_cast<char*>(&h), sizeof(float));

    width=(int)round(w);

    cout << "Depth map Width:" << width << endl;
    cout << "Depth map Height:" << h << endl;

    data.resize(num_elements-2);
    fin.read(reinterpret_cast<char*>(&data[0]), (num_elements-2)*sizeof(float));

    return true;
}

int main(int argc, char *argv[])
{

    if(argc <5)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    int last_camera=atoi(argv[4]);
    string depthmap0=string(argv[3]);
    int cameraindex = depthmap0.rfind("camera");
    string depthfile_part0 = depthmap0.substr(0, cameraindex);
    string depthfile_part1 = depthmap0.substr(0, cameraindex+6);
    int first_camera= atoi(depthmap0.substr(cameraindex+6,1).c_str());
    //Find now the frame. The numbering of the frame is 5 digits.
    string depthfile_part2 = depthmap0.substr(cameraindex+7);
    string frame_number_string = depthmap0.substr(cameraindex+8,cameraindex+10);

    cv::Mat panoramic_gradient, pano_contribution_map;
    bool contribution_map=false;
    if(argc>5){
        contribution_map=true;

        string contribution_map_path=string(argv[5]);
        pano_contribution_map=cv::imread(contribution_map_path, IMREAD_GRAYSCALE);
        if(pano_contribution_map.data==NULL){
            cout << "Error reading image: "<< contribution_map_path << endl;
            return 1;
        }

        cv::resize(pano_contribution_map, pano_contribution_map, cv::Size(pano_width, pano_height));

        //Now calculate gradient image according to sobel to extract borders of image contribution.
        //originalGrayscaleImage_.convertTo(tmp_original,CV_32F);
        // Gradient X
        cv::Mat Dx;
        Sobel( pano_contribution_map, Dx, CV_32F, 1, 0, 5);
        cv::Mat Dy;
        Sobel( pano_contribution_map, Dy, CV_32F, 0, 1, 5);
        cv::addWeighted( cv::abs(Dx), 0.5, cv::abs(Dy), 0.5, 0, panoramic_gradient ); //Approximation
        panoramic_gradient.convertTo(panoramic_gradient, CV_8U);
        cv::threshold(panoramic_gradient,panoramic_gradient,0,255,cv::THRESH_BINARY); //Convert panoramic_gradient from 0 to 1
    }


    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[2]);
    calibration.loadCalibrationFile(calibration_file);

    /*
    cout << "Reducing calibration parameters by a 4 factor" << endl;
    //We don't neeed depth images at full resolution for our purposes, so we decrease them by 4 times.
    calibration.setImageWidth(calibration.getImageWidth()/4);
    calibration.setImageHeight(calibration.getImageHeight()/4);


    //Change parameters camera by camera.
    for(int i=0;i<calibration.getNumberCameras();i++){

        cv::Mat cameraMat;
        calibration.getCameraMatrix(i,cameraMat);
        //Divide f and cx,cy by 4.
        cameraMat=cameraMat/4;
        cameraMat.at<double>(2,2)=1.0;

        calibration.setCameraMatrix(i,cameraMat);
    }
    cout << "New calibration parameters set." << endl;
    */

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();

    //Read all depthmaps

    //Find now the frame. The numbering of the frame is 5 digits.
    std::vector<float> depthmapdata[last_camera+1];
    cv::Mat individualDepthMap[last_camera+1];
    cv::Mat pointCloud[last_camera+1];
    cv::Mat pointCloudGlobal[last_camera+1];
    cv::Mat equiIndividualSparseDepthMap[last_camera+1];
    cv::Mat equiIndividualDenseDepthMap[last_camera+1];
    cv::Mat equiIndividualMask[last_camera+1];
    cv::Mat equiIndividualNonOverlapping[last_camera+1];



    stringstream ss;
    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        string depthmap_path;
        depthmap_path.clear();
        ss << depthfile_part1 << cam << depthfile_part2;
        depthmap_path=ss.str();
        cout << "Depth map " << depthmap_path << endl;
        cout << "Converting indidual depth map to equirectangular depth map" << endl;
        int width_depth_map=2704;
        //read_depth(depthmap_path,width_depth_map, depthmapdata[cam]);

        //cv::Mat depthMap(width_depth_map/2,width_depth_map,CV_64F);
        individualDepthMap[cam]=cv::Mat::zeros(width_depth_map*3/4,width_depth_map,CV_32F);
        equiIndividualSparseDepthMap[cam]=cv::Mat::zeros(pano_height,pano_width,CV_64FC1);
        equiIndividualDenseDepthMap[cam]=cv::Mat::zeros(pano_height,pano_width,CV_64FC1);
        equiIndividualMask[cam]=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
        equiIndividualNonOverlapping[cam]=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
        pointCloud[cam]=cv::Mat::zeros(imageHeight,imageWidth,CV_64FC4);

        string file_extension=depthmap_path.substr(depthmap_path.rfind(".")+1);
        cout << "File extension " << file_extension << endl;

        if(file_extension=="bin"){
          read_depth_binary(depthmap_path,width_depth_map, depthmapdata[cam]);
          memcpy(individualDepthMap[cam].data, depthmapdata[cam].data(), depthmapdata[cam].size()*sizeof(float));
        }
        else if(file_extension=="exr"){
          cv::Mat exrMat=cv::imread(depthmap_path,CV_LOAD_IMAGE_ANYDEPTH);
          Mat bgr[3];   //destination array
          split(exrMat,bgr);//split source
          individualDepthMap[cam]=bgr[0].clone();
          width_depth_map=individualDepthMap[cam].cols;
        }
        else{
          cout << "Extension not recognised" << endl;
        }

        individualDepthMap[cam].convertTo(individualDepthMap[cam],CV_64F);
        cout << "Before resizing" << endl;
        //Resize depth images.
        cv::resize(individualDepthMap[cam], individualDepthMap[cam], cv::Size(imageWidth, imageHeight),0,0,cv::INTER_NEAREST);

        cout << "Depth map resized" << endl;

        //Compute 3D point from each depth map.

        double min, max;
        cv::minMaxLoc(individualDepthMap[cam], &min, &max);

        cout << "Max" << max << endl;
        if(SHOW_DEBUG_IMAGES){

            cv::Mat adjMap;
            cv::convertScaleAbs(individualDepthMap[cam], adjMap, 255 / max);

            cv::namedWindow("Individual Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Individual Depth Map",adjMap);
            cv::waitKey(50);
        }

        for(int row=0; row<imageHeight; row++){
            for(int col=0; col<imageWidth; col++){

                double localX, localY, localZ;
                calibration.RCtoCameraVector(row, col, cam, localX, localY, localZ);
                double k=individualDepthMap[cam].at<double>(row,col)/sqrt(pow(localX,2)+pow(localY,2)+pow(localZ,2));
                pointCloud[cam].at<cv::Vec4d>(row,col)=cv::Vec4d(k*localX,k*localY,k*localZ,1.0);
            }
        }
        pointCloud[cam]=pointCloud[cam].reshape(1,imageWidth*imageHeight).t();
        calibration.camera3dPointToMultiCam3dPoint(cam,pointCloud[cam],pointCloudGlobal[cam]);

        //Compute increment of angle per pixel in the horizon
        double delta_theta=2*PI/pano_width;

        for(int i=0;i<pointCloudGlobal[cam].cols;i++){
            if(pointCloud[cam].at<double>(2,i)!=0){
                double R=sqrt(pow(pointCloudGlobal[cam].at<double>(0,i),2)+pow(pointCloudGlobal[cam].at<double>(1,i),2)+pow(pointCloudGlobal[cam].at<double>(2,i),2));
                double theta=atan2(pointCloudGlobal[cam].at<double>(1,i),pointCloudGlobal[cam].at<double>(0,i));
                double phi=acos(pointCloudGlobal[cam].at<double>(2,i)/R);
                double u=(PI-theta)/(2*PI)*(pano_width);
                double v=phi/PI*(pano_height);

                //Compute angle between two pixels in the same row
                cv::Vec3d v1(sin(phi)*cos(theta),sin(phi)*sin(theta),cos(phi));
                cv::Vec3d v2(sin(phi)*cos(theta+delta_theta),sin(phi)*sin(theta+delta_theta),cos(phi));
                double angle_diff=acos(v1.dot(v2)/(cv::norm(v1)*cv::norm(v2)));

                double pixel_width=delta_theta/angle_diff;
                cv::Point2d top_left(u-pixel_width/2, v);
                cv::Point2d down_right(u+pixel_width/2, v);
                cv::Point2i top_left_i(std::max((int)round(top_left.x),0),std::max((int)round(top_left.y),0));
                cv::Point2i down_right_i(std::min((int)round(down_right.x),pano_width-1),std::min((int)round(down_right.y),pano_height-1));
                cv::Size rect_size=down_right_i-top_left_i;
                cv::Mat pRoi = equiIndividualDenseDepthMap[cam](cv::Rect(top_left_i,rect_size+cv::Size(1,1)));
                //In case the pixel had value 0 we replace.
                pRoi.setTo(R,pRoi == 0);
                //In case the pixels have larger depth value we replace.
                pRoi.setTo(R,pRoi > R);

                pRoi = equiIndividualMask[cam](cv::Rect(top_left_i,rect_size+cv::Size(1,1)));
                pRoi.setTo( (unsigned char) 1);

                equiIndividualSparseDepthMap[cam].at<double>(round(v),round(u))=R;

            }
        }


        cv::minMaxLoc( equiIndividualDenseDepthMap[cam], &min, &max);
        cv::Mat colorDepthMap,mask;
        cv::convertScaleAbs(  equiIndividualDenseDepthMap[cam], colorDepthMap, 255 / max);
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
        cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
        colorDepthMap.setTo(Scalar(0,0,0), mask);

        if(SHOW_DEBUG_IMAGES){

            cv::namedWindow("Individual Dense Depth Map", WINDOW_NORMAL);
            //cv::resizeWindow("Depth Map", pano_width, pano_height);
            cv::imshow("Individual Dense Depth Map",colorDepthMap);
            cv::waitKey(50);
        }

        string pano_filename;
        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_individual_" << frame_number_string << "_camera"<< cam <<".png";
        pano_filename.clear();
        pano_filename=ss.str();

        std::vector<int> qualityType;
        qualityType.push_back(IMWRITE_JPEG_QUALITY);
        qualityType.push_back(85); //Jpeg quality

        bool writing_success;
        /*writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
        }
        else{
          cout << "Written " << pano_filename << endl;
        }
        */

        cv::minMaxLoc( equiIndividualSparseDepthMap[cam], &min, &max);
        cv::convertScaleAbs(  equiIndividualSparseDepthMap[cam], colorDepthMap, 255 / max);
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
        cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
        colorDepthMap.setTo(Scalar(0,0,0), mask);

        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_individual_" << frame_number_string << "_camera"<< cam <<"_sparse.png";
        pano_filename.clear();
        pano_filename=ss.str();
        writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
        }
        else{
          cout << "Written " << pano_filename << endl;
        }

        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_individual_" << frame_number_string << "_camera"<< cam << "_sparse.exr";
        string filename=ss.str();
        cv::Mat temp;
        equiIndividualSparseDepthMap[cam].convertTo(temp,CV_32F);
        cv::imwrite(filename,temp);

        cout << "Mesh file written in "<< filename << endl;

    }


    //Panoramic image
    cv::Mat panoramic=cv::Mat::zeros(pano_height,pano_width,CV_64F);
    cv::Mat panoramicDense=cv::Mat::zeros(pano_height,pano_width,CV_64F);
    cv::Mat map=cv::Mat::zeros(pano_height,pano_width,CV_64F);
    int v;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(v = 0; v< pano_height; v++){
        int u;
        for(u = 0; u <pano_width ; u++) {
            int covered=0;
            double minDistance=1e9;
            int min_camera=-1;

            int coveredDense=0;
            double minDistanceDense=1e9;
            int min_cameraDense=-1;

            //Find in how many camera we have depth information
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                double depth_value=equiIndividualSparseDepthMap[cam].at<double>(v,u);
                double depth_valueDense=equiIndividualDenseDepthMap[cam].at<double>(v,u);
                if(depth_value!=0){
                    covered++;
                    if(depth_value<minDistance) {
                        minDistance=depth_value;
                        min_camera=cam;
                    }
                }
                if(depth_valueDense!=0){
                    coveredDense++;
                    if(depth_valueDense<minDistanceDense) {
                        minDistanceDense=depth_valueDense;
                        min_cameraDense=cam;
                    }
                }
            }
            if(covered>0){
                panoramic.at<double>(v,u)=equiIndividualSparseDepthMap[min_camera].at<double>(v,u);
                map.at<double>(v,u)=min_camera;
            }
            if(coveredDense>0){
                panoramicDense.at<double>(v,u)=equiIndividualDenseDepthMap[min_cameraDense].at<double>(v,u);
            }
        }
    }

    double min,max;
    cv::minMaxLoc( panoramic, &min, &max);
    cv::Mat colorDepthMap,mask;
    cv::convertScaleAbs(  panoramic, colorDepthMap, 255 / max);
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    string pano_filename;
    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_sparse.png";
    pano_filename.clear();
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    bool writing_success;
    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }

    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_sparse.exr";
    string filename=ss.str();

    cv::Mat temp;
    panoramic.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);
    cout << "Mesh file written in "<< filename << endl;

    cv::Mat colorMap;
    cv::minMaxLoc( map, &min, &max);
    cv::convertScaleAbs(  map, colorMap, 255 / max);
    applyColorMap(colorMap, colorMap, COLORMAP_JET);
    cv::inRange(colorMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorMap.setTo(Scalar(0,0,0), mask);
    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_sparse_map.png";
    pano_filename.clear();
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, colorMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }


    cv::minMaxLoc( panoramicDense, &min, &max);
    cv::convertScaleAbs(  panoramicDense, colorDepthMap, 255 / max);
    applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
    cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
    colorDepthMap.setTo(Scalar(0,0,0), mask);

    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_dense.png";
    pano_filename.clear();
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }

    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_dense.exr";
    filename=ss.str();

    panoramicDense.convertTo(temp,CV_32F);
    cv::imwrite(filename,temp);
    cout << "Mesh file written in "<< filename << endl;


    //If we have a contribution map we compose a full panoama.
    if(contribution_map){
        //Panoramic depth map
        cv::Mat panoramic=cv::Mat::zeros(pano_height,pano_width,CV_64F);
        cv::Mat panoramic_sparse=cv::Mat::zeros(pano_height,pano_width,CV_64F);


        bool done[pano_height];
        std::fill_n(done, pano_height, false);

        int v;
        //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
        for(v = 0; v< pano_height; v++){
            int u;
            for(u = 0; u <pano_width ; u++) {
                int covered=0;
                int covered_sparse=0;
                double minDistance=1e9;
                double minDistanceSparse=1e9;
                int min_camera=-1;
                int min_camera_sparse=-1;
                //Find in how many camera we have depth information
                int cam;
                for(cam = first_camera; cam<= last_camera; cam++){
                    if(pano_contribution_map.at<unsigned char>(v,u)==cam){
                        double depth_value=equiIndividualDenseDepthMap[cam].at<double>(v,u);
                        if(depth_value!=0){
                            covered++;
                            if(depth_value<minDistance) {
                                minDistance=depth_value;
                                min_camera=cam;
                            }
                        }
                        depth_value=equiIndividualSparseDepthMap[cam].at<double>(v,u);
                        if(depth_value!=0){
                            covered_sparse++;
                            if(depth_value<minDistanceSparse) {
                                minDistanceSparse=depth_value;
                                min_camera_sparse=cam;
                            }
                        }
                    }
                }
                if(covered>0){
                    panoramic.at<double>(v,u)=equiIndividualDenseDepthMap[min_camera].at<double>(v,u);
                }
                if(covered_sparse>0){
                    panoramic_sparse.at<double>(v,u)=equiIndividualSparseDepthMap[min_camera_sparse].at<double>(v,u);
                }
            }
            done[v]=true;

            int numberDone=0;
            for( int i = 0; i < pano_height; i++ ) {
                if( done[i]==true ) {
                    numberDone++;
                }
            }

            std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;
        }


        double min,max;
        cv::minMaxLoc( panoramic, &min, &max);
        cv::Mat colorDepthMap,mask;
        cv::convertScaleAbs(  panoramic, colorDepthMap, 255 / max);
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
        cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
        colorDepthMap.setTo(Scalar(0,0,0), mask);

        string pano_filename;
        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_dense.png";
        pano_filename.clear();
        pano_filename=ss.str();

        std::vector<int> qualityType;
        qualityType.push_back(IMWRITE_JPEG_QUALITY);
        qualityType.push_back(85); //Jpeg quality

        bool writing_success;
        writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
        }

        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_dense.exr";
        string filename=ss.str();

        cv::Mat temp;
        panoramic.convertTo(temp,CV_32F);
        cv::imwrite(filename,temp);
        cout << "Mesh file written in "<< filename << endl;


        //SPARSE
        //double min, max;
        cv::minMaxLoc( panoramic_sparse, &min, &max);
        //cv::Mat colorDepthMap,mask;
        cv::convertScaleAbs(  panoramic_sparse, colorDepthMap, 255 / max);
        applyColorMap(colorDepthMap, colorDepthMap, COLORMAP_JET);
        cv::inRange(colorDepthMap, Scalar(128,0,0), Scalar(128,0,0), mask);
        colorDepthMap.setTo(Scalar(0,0,0), mask);

        //string pano_filename;
        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_sparse.png";
        pano_filename.clear();
        pano_filename=ss.str();

        //bool writing_success;
        writing_success=imwrite(pano_filename, colorDepthMap, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
        }

        ss.str("");
        ss << depthfile_part0 << "equi_depth_map_" << frame_number_string << "_sparse.exr";
        filename=ss.str();

        //cv::Mat temp;
        panoramic_sparse.convertTo(temp,CV_32F);
        cv::imwrite(filename,temp);
        cout << "Mesh file written in "<< filename << endl;


    }

    //We need to generate a contribution map.
    cout << "Generating contribution map for sparse depthmap" << endl;
    cv::Mat first_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
    cv::Mat second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(y = 0; y< pano_height; y++){
        int x;
        for(x = 0; x <pano_width ; x++) {

          double theta=PI-x*2*PI/pano_width;
          double phi=PI*y/pano_height;

          float minDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.
          float secondMinDistanceCenter=static_cast<float>(pano_width) *static_cast<float>(pano_width); // Max value possible.

          int cameraMinDistanceCenter=-1;
          int secondCameraMinDistanceCenter=-1;
          int covered=0;
          double dRow[last_camera];
          std::fill_n(dRow, numberCameras,-1);
          double dCol[last_camera];
          std::fill_n(dCol, numberCameras,-1);

          for(int cam = first_camera; cam<= last_camera; cam++){
            double dist=0;
            if(equiIndividualSparseDepthMap[cam].at<double>(y,x)!=0){
                dist=equiIndividualSparseDepthMap[cam].at<double>(y,x);
                double globalZ=dist*cos(phi);
                double globalX=dist*sin(phi)*cos(theta);
                double globalY=dist*sin(phi)*sin(theta);
                int result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                if (result_!=-1) {
                    covered++;
                    float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                    if (distanceCenter<minDistanceCenter){
                        secondMinDistanceCenter=minDistanceCenter;
                        secondCameraMinDistanceCenter=cameraMinDistanceCenter;

                        minDistanceCenter=distanceCenter;
                        cameraMinDistanceCenter=cam;
                    }
                    if (distanceCenter<secondMinDistanceCenter && distanceCenter!= minDistanceCenter){
                        secondMinDistanceCenter=distanceCenter;
                        secondCameraMinDistanceCenter=cam;
                    }
                }
            }
          }
          if(covered>0){
            first_contribution_map.at<unsigned char>(y,x)=(unsigned char) cameraMinDistanceCenter;
            if(secondCameraMinDistanceCenter>-1){
              second_contribution_map.at<unsigned char>(y,x)=(unsigned char) secondCameraMinDistanceCenter;
            }
          }
      }
    }

    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_individual_" << frame_number_string << "_1st_contribution_map.png";
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, first_contribution_map, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    else{
      cout << "Image written in " << pano_filename << endl;
    }

    ss.str("");
    ss << depthfile_part0 << "equi_depth_map_individual_" << frame_number_string << "_2nd_contribution_map.png";
    pano_filename=ss.str();
    writing_success=imwrite(pano_filename, second_contribution_map, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    else{
      cout << "Image written in " << pano_filename << endl;
    }




    /*
    for(int cam = first_camera; cam <= last_camera; cam++) {
        equiIndividualNonOverlapping[cam]=equiIndividualMask[cam].clone();
        for(int cam2 = first_camera; cam2 <= last_camera; cam2++) {
            if(cam!=cam2){
                equiIndividualNonOverlapping[cam]=equiIndividualNonOverlapping[cam]-equiIndividualMask[cam2];
            }
            cv::namedWindow("Non overalpping mask", WINDOW_NORMAL);
            //cv::resizeWindow("Depth Map", pano_width, pano_height);
            cv::imshow("Non overalpping mask",equiIndividualNonOverlapping[cam]*255);
            cv::waitKey(100);
        }

        cv::namedWindow("Non overalpping mask", WINDOW_NORMAL);
        //cv::resizeWindow("Depth Map", pano_width, pano_height);
        cv::imshow("Non overalpping mask",equiIndividualNonOverlapping[cam]*255);
        cv::waitKey(100);

        stringstream ss;
        string pano_filename;
        ss.str("");
        pano_filename.clear();
        ss << "equi_depth_map_cam_"<< cam <<"_nonoverlapping.png";
        pano_filename=ss.str();

        std::vector<int> qualityType;
        qualityType.push_back(IMWRITE_JPEG_QUALITY);
        qualityType.push_back(85); //Jpeg quality

        bool writing_success;
        writing_success=imwrite(pano_filename, equiIndividualNonOverlapping[cam]*255, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
        }

    }
    */


        /*
        for(int cam = first_camera; cam <= last_camera; cam++) {
            //Extract individual contribution map
            cv::Mat equiIndividualContribution=cv::Mat::zeros(pano_height, pano_width, CV_8UC1);
            equiIndividualContribution=pano_contribution_map==cam;

            cv::namedWindow("Panoramic gradient", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow( "Panoramic gradient", panoramic_gradient );
            cv::waitKey();

            cv::namedWindow("Individual contribution", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow( "IndividualContribution", equiIndividualContribution );
            cv::waitKey(20);
            cv::Mat equiIndividualBorder=equiIndividualContribution.mul(panoramic_gradient);
            cv::namedWindow("Gradient", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow( "Gradient", equiIndividualBorder[cam] );
            cv::waitKey();


            ss.str("");
            pano_filename.clear();
            ss << "individual_contribution.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, equiIndividualContribution, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

            ss.str("");
            pano_filename.clear();
            ss << "individual_border.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, equiIndividualBorder, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }


            //Resize contribution map and border
            cv::resize(equiIndividualBorder, equiIndividualBorder, equiIndividualSparseDepthMap[cam].size(),0,0,cv::INTER_NEAREST);
            cv::resize(equiIndividualContribution, equiIndividualContribution, equiIndividualSparseDepthMap[cam].size(),0,0,cv::INTER_NEAREST);


            ss.str("");
            pano_filename.clear();
            ss << "individual_contribution.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, equiIndividualContribution, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

            ss.str("");
            pano_filename.clear();
            ss << "individual_border.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, equiIndividualBorder, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

            cv::Mat no_depth=equiIndividualDenseDepthMap[cam]==cv::Mat::zeros(pano_height,pano_width,CV_64FC1);
            no_depth.convertTo(no_depth, CV_8UC1);
            no_depth=no_depth*255;

            //cv::Mat inpainting_mask = equiIndividualSparseDepthMap[cam]== cv::Mat::zeros(pano_height,pano_width,CV_64FC1);
            cv::Mat valid_pixels=equiIndividualBorder-no_depth;
            cv::Mat inpainting_mask = equiIndividualContribution-valid_pixels;

            cv::namedWindow("Valid pixels", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Valid pixels",valid_pixels);
            cv::waitKey(20);

            ss.str("");
            pano_filename.clear();
            ss << "valid_pixels.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, valid_pixels, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

            cv::namedWindow("To interpolate", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("To interpolate",inpainting_mask);
            cv::waitKey(20);

            ss.str("");
            pano_filename.clear();
            ss << "toInterpolate.png";
            pano_filename=ss.str();

            writing_success=imwrite(pano_filename, inpainting_mask, qualityType);
            if(!writing_success){
                cout << "\nError writing image! Check output directory existence." << endl;
            }

            double min, max;
            cv::minMaxLoc( equiIndividualDenseDepthMap[cam], &min, &max);
            cv::Mat adjMap;
            cv::convertScaleAbs( equiIndividualDenseDepthMap[cam], adjMap, 255 / max);


            cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Depth Map",adjMap);
            cv::waitKey(20);


            //cout << "Region filling " << endl;
            //cv::Mat filled=regionfill(equiIndividualDenseDepthMap[cam],valid_pixels,inpainting_mask);
            //cv::minMaxLoc( filled, &min, &max);

            cv::convertScaleAbs( filled, adjMap, 255 / max);


            cout << "Here" << endl;

            cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            cv::imshow("Depth Map",adjMap);
            cv::waitKey();


            cv::Mat filled=regionfill(equiIndividualDenseDepthMap[cam],valid_pixels,inpainting_mask,pano_width);

            equiIndividualDenseDepthMap[cam]=filled;


            //Inpaint doesn't work for what i want...
            //inpainting_mask=cv::Mat::ones(pano_height,pano_width,CV_8UC1)*255-valid_pixels;

            //cv::namedWindow("To inpaint", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            //cv::imshow("To inpaint",inpainting_mask);
            //cv::waitKey();


            //cv::inpaint(adjMap,inpainting_mask,adjMap,5,cv::INPAINT_NS);
            //cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
            //cv::imshow("Depth Map",adjMap);
            //cv::waitKey();


        }
        //Panoramic image
        cv::Mat panoramic=cv::Mat::zeros(pano_height,pano_width,CV_64F);

        bool done[pano_height];
        std::fill_n(done, pano_height, false);

        int v;
        #pragma omp parallel for default(none) shared(panoramic,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
        for(v = 0; v< pano_height; v++){
            int u;
            for(u = 0; u <pano_width ; u++) {
                int covered=0;
                double minDistance=1e9;
                int min_camera=-1;
                //Find in how many camera we have depth information
                int cam;
                for(cam = first_camera; cam<= last_camera; cam++){
                    double depth_value=equiIndividualDenseDepthMap[cam].at<double>(v,u);
                    if(depth_value!=0){
                        covered++;
                        if(depth_value<minDistance) {
                            minDistance=depth_value;
                            min_camera=cam;
                        }
                    }
                }
                if(covered>0){
                    panoramic.at<double>(v,u)=equiIndividualDenseDepthMap[min_camera].at<double>(v,u);
                }
            }
            done[v]=true;

            int numberDone=0;
            for( int i = 0; i < pano_height; i++ ) {
                if( done[i]==true ) {
                    numberDone++;
                }
            }

            std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;
        }

    /*
    //CREATE A PANORAMA WITH COLORS REPRESENTING CONTRIBUTION OF EACH CAMERA

    //List of colors
    std::vector<cv::Vec3b> colors_list;
    colors_list.push_back(cv::Vec3b(90,90,235));
    colors_list.push_back(cv::Vec3b(75,190,45));
    colors_list.push_back(cv::Vec3b(75,165,245));
    colors_list.push_back(cv::Vec3b(160,95,175));
    colors_list.push_back(cv::Vec3b(200,140,87));
    colors_list.push_back(cv::Vec3b(55,185,205));

    cv::Mat equiColors=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);
    #pragma omp parallel for default(none) shared(equiColors,done,cout,pano_height, R, imageHeight,imageWidth,image, calibration)
    for(v = 0; v< pano_height; v++){
        int u;
        for(u = 0; u <pano_width ; u++) {
            bool covered_first_time=false;
            int covered=0;
            //Find in how many camera we have depth information
            int cam;
            for(cam = first_camera; cam<= last_camera; cam++){
                bool depth_value=equiIndividualMask[cam].at<unsigned char>(v,u)==(unsigned char) 1;
                if(depth_value){
                    covered++;
                }
            }
            if(covered>0){
                for(cam = first_camera; cam<= last_camera; cam++){
                    bool depth_value=equiIndividualMask[cam].at<unsigned char>(v,u)==(unsigned char) 1;
                    if (depth_value) {
                        if(!covered_first_time){
                            panoramic.at<cv::Vec3b>(v,u)= cv::Vec3b(0,0,0);
                            covered_first_time=true;
                        }
                        equiColors.at<cv::Vec3b>(v,u)=equiColors.at<cv::Vec3b>(v,u)+colors_list[cam]*1.0/covered;
                    }
                }

            }
        }
    }

    cv::namedWindow("Colors", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Colors",equiColors);
    cv::waitKey(20);

    ss.str("");
    pano_filename.clear();
    ss << "camera_contribution_equi_depth_map.png";
    pano_filename=ss.str();

    writing_success=imwrite(pano_filename, equiColors, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
    }
    */


    return 0;
}
