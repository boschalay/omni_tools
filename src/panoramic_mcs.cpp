#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp"
#include "depthmap_utils.cpp"

#define PI 3.14159265358979323846

#define SHOW_DEBUG_IMAGES 0

using namespace cv;
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec){
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl;
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s output_width image_colour_camera0 calibration_file radius last_camera <EquiDepthMap> <IndividualDepthMap>\n",argv[0]);
}

bool read_depth( string depth_file, int& width, std::vector<float>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
    }
    return true;
}

bool read_depth_binary( string depth_file, int& width, std::vector<float>&  data)
{


    std::ifstream fin(depth_file.c_str(), std::ios::binary);
    if(!fin)
    {
        std::cout << " Error, Couldn't find the file" << "\n";
        return false;
    }
    else{
        cout << "Depth map found" << endl;
    }

    fin.seekg(0, std::ios::end);
    const size_t num_elements = fin.tellg() / sizeof(float);
    fin.seekg(0, std::ios::beg);

    float w,h;

    fin.read(reinterpret_cast<char*>(&w), sizeof(float));
    fin.read(reinterpret_cast<char*>(&h), sizeof(float));

    width=(int)round(w);

    cout << "Depth map Width:" << width << endl;
    cout << "Depth map Height:" << h << endl;

    data.resize(num_elements-2);
    fin.read(reinterpret_cast<char*>(&data[0]), (num_elements-2)*sizeof(float));

    return true;
}

int main(int argc, char *argv[])
{

    if(argc < 6 || argc > 8)
    {
        printhelp(argv);
        return(1);
    }

    const int pano_width=atoi(argv[1]); //Width is adjustable
    int pano_height=pano_width/2;
    cout << "Panorama Width: "<< pano_width << ", Height: " << pano_height << endl;

    string image0=string(argv[2]);
    int cameraindex = image0.rfind("camera");
    string file_part0 = image0.substr(0, cameraindex);
    string file_part1 = image0.substr(0, cameraindex+6);
    int first_camera= atoi(image0.substr(cameraindex+6,1).c_str());
    cout << "First camera is " << first_camera << endl;
    //Find now the frame. The numbering of the frame is 5 digits.
    int indexframe = image0.rfind("frame");
    string file_part2 = image0.substr(cameraindex+7,indexframe-cameraindex-2);
    string first_frame_str = image0.substr(indexframe+5,5);
    int first_frame=atoi(first_frame_str.c_str());
    string file_part3 = image0.substr(indexframe+10);
    int last_camera=atoi(argv[5]);

    cout << "Last camera is: " << last_camera << endl;

    cout << "Arguments: " << argc << endl;
    cv::Mat scaledDepthMap, adjMap;
    bool haveDepthMap=false;
    bool haveIndividualDepthMap=false;


    MultiCamCalibration calibration; //Create calibration object.

    //-- Load calibation matrices.
    string calibration_file=string(argv[3]);
    calibration.loadCalibrationFile(calibration_file);

    const double radius=atof(argv[4]);
    calibration.setSphereRadius(radius);
    cout << "Sphere Radius set to: " << radius << endl;

    int imageWidth=calibration.getImageWidth();
    int imageHeight=calibration.getImageHeight();
    int numberCameras=calibration.getNumberCameras();


    std::vector<float> depthmapdata[last_camera+1];
    cv::Mat individualDepthMap[last_camera+1];

    if(argc>6){
        haveDepthMap=true;
        cout << "Depth map" << endl;
        string depth_map=string(argv[6]);

        string file_extension=depth_map.substr(depth_map.rfind(".")+1);

        cout << "File extension " << file_extension << endl;

        int width_depth_map;
        std::vector<float> depthmapdata_equi;
        cv::Mat depthMap;

        depthMap=read_depthmap(depth_map);
        double min, max;
        cv::minMaxLoc(depthMap, &min, &max);

        cout << "Max" << max << endl;

        cv::resize(depthMap, scaledDepthMap, cv::Size(pano_width,pano_height),0,0,cv::INTER_NEAREST);

        cv::convertScaleAbs(scaledDepthMap, adjMap, 255 / max);
        if(SHOW_DEBUG_IMAGES){

          cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
          cv::imshow("Depth Map",adjMap);
          cv::waitKey();
        }


        if(argc>7){
        haveIndividualDepthMap=true;
        //Read all depthmaps
        string depthmap0=string(argv[7]);
        cameraindex = depthmap0.rfind("camera");
        string depthfile_part1 = depthmap0.substr(0, cameraindex+6);
        //Find now the frame. The numbering of the frame is 5 digits.
        string depthfile_part2 = depthmap0.substr(cameraindex+7);
        stringstream ss;
            for(int cam = first_camera; cam <= last_camera; cam++) {
                ss.str("");
                string depthmap_path;
                depthmap_path.clear();
                ss << depthfile_part1 << cam << depthfile_part2;
                depthmap_path=ss.str();
                cout << "Depth map " << depthmap_path << endl;
                int width_depth_map;

                string file_extension=depthmap_path.substr(depthmap_path.rfind(".")+1);
                cout << "File extension " << file_extension << endl;

                individualDepthMap[cam]=cv::Mat::zeros(imageHeight,imageWidth,CV_32F);
                if(file_extension=="bin"){
                  read_depth_binary(depthmap_path,width_depth_map, depthmapdata[cam]);
                  memcpy(individualDepthMap[cam].data, depthmapdata[cam].data(), depthmapdata[cam].size()*sizeof(float));
                }
                else if(file_extension=="exr"){
                  cv::Mat exrMat=cv::imread(depthmap_path,CV_LOAD_IMAGE_ANYDEPTH);
                  Mat bgr[3];   //destination array
                  split(exrMat,bgr);//split source
                  individualDepthMap[cam]=bgr[0].clone();
                  width_depth_map=depthMap.cols;

                }

                individualDepthMap[cam].convertTo(individualDepthMap[cam],CV_64F);


                if(SHOW_DEBUG_IMAGES){
                  cv::minMaxLoc(individualDepthMap[cam], &min, &max);
                  cv::convertScaleAbs(individualDepthMap[cam], adjMap, 255 / max);
                  cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
                  cv::imshow("Depth Map",adjMap);
                  cv::waitKey();
                }

            }
        }
    }

    //Read the images.

    Mat image[last_camera+1];
    string image_path;
    stringstream ss;

    for(int cam = first_camera; cam <= last_camera; cam++) {
        ss.str("");
        image_path.clear();
        ss << file_part1 << cam << file_part2 << setfill('0')<< setw(5) << first_frame<< file_part3;
        image_path=ss.str();
        image[cam] = imread(image_path, IMREAD_COLOR);
        if(image[cam].data==NULL){
            cout << "Error reading image: "<< image_path << endl;
            return 1;
        }
    }

    cout << "All images opened succesfully. About to start panoramic image" << endl;


    double R=calibration.getSphereRadius();

    //Panoramic image
    cv::Mat panoramic, contribution_map, second_contribution_map;
    panoramic=cv::Mat::zeros(pano_height,pano_width,CV_8UC3);
    contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);
    second_contribution_map=cv::Mat::zeros(pano_height,pano_width,CV_8UC1);

    bool done[pano_height];
    std::fill_n(done, pano_height, false);

    int y;
    //#pragma omp parallel for default(none) shared(panoramic, contribution_map, second_contribution_map, cout, done, haveDepthMap, haveIndividualDepthMap, scaledDepthMap, individualDepthMap, pano_height, last_camera, R, numberCameras, first_camera, calibration, imageWidth, imageHeight, image)
    for(y = 0; y< pano_height; y++){
        double dRow[last_camera];
        std::fill_n(dRow, numberCameras,-1);
        double dCol[last_camera];
        std::fill_n(dCol, numberCameras,-1);
        int x;
        for(x = 0; x <pano_width ; x++) {
            double globalX;
            double globalY;
            double globalZ;

            //Find 3D position
            //Theta goes from -pi to pi
            //Phi goes from 0  to pi
            double theta=PI-(2*PI*x/pano_width);// To rotate 180: -PI;
            double phi=PI*y/pano_height;

            double dist=R;
            if(haveDepthMap){
                if(scaledDepthMap.at<double>(y,x)>0){
                    dist=scaledDepthMap.at<double>(y,x);
                }
                else{
                    dist=0;
                }
            }
            if(dist!=0){

                //From theta and phi find the 3D coordinates.
                globalZ=dist*cos(phi);
                globalX=dist*sin(phi)*cos(theta);
                globalY=dist*sin(phi)*sin(theta);

                float minDistanceCenter=pano_width*pano_width; // Max value possible.
                float secondMinDistanceCenter=pano_width*pano_width; // Max value possible.
                int cameraMinDistanceCenter=-1;
                int secondCameraMinDistanceCenter=-1;
                int covered=0;
                bool parallax=false;
                int result_;

                //From the 3D coordinates, find in how many camera falls the point!
                int cam;
                for(cam = first_camera; cam<= last_camera; cam++){
                    result_=calibration.MultiCamXYZtoRC(globalX, globalY, globalZ, cam, dRow[cam], dCol[cam]);
                    if (result_!=-1) {
                        //Check if distance of individual depth map matches.
                        cv::Mat PointGlobal(4,1,CV_64F);
                        PointGlobal.at<double>(0,0)=globalX;
                        PointGlobal.at<double>(1,0)=globalY;
                        PointGlobal.at<double>(2,0)=globalZ;
                        PointGlobal.at<double>(3,0)=1.0;
                        cv::Mat PointLocal;
                        calibration.multiCam3DPoint2camera3dPoint(PointGlobal, cam, PointLocal);
                        double distance=cv::norm(cv::Mat(PointLocal,cv::Rect(0,0,1,3)));

                        //This is to check if area is covered by a camera or there was something in the middle.
                        //We need to give some threshold because of approximations.
                        if(haveDepthMap && haveIndividualDepthMap && individualDepthMap[cam].at<double>(dRow[cam],dCol[cam])>0 && scaledDepthMap.at<double>(y,x)>0 && abs((individualDepthMap[cam].at<double>(dRow[cam],dCol[cam])-distance))>0.1*distance){
                            dRow[cam]=-1;
                            dCol[cam]=-1;
                            parallax=true;
                        }
                        else{
                            covered++;
                            float distanceCenter=sqrt(pow(dRow[cam]-imageHeight/2,2)+pow(dCol[cam]-imageWidth/2,2));
                            if (distanceCenter<minDistanceCenter){
                                secondMinDistanceCenter=minDistanceCenter;
                                secondCameraMinDistanceCenter=cameraMinDistanceCenter;

                                minDistanceCenter=distanceCenter;
                                cameraMinDistanceCenter=cam;
                            }
                            if (distanceCenter<secondMinDistanceCenter && distanceCenter!= minDistanceCenter){
                                secondMinDistanceCenter=distanceCenter;
                                secondCameraMinDistanceCenter=cam;
                            }

                        }
                    }
                    else{
                        dRow[cam]=-1;
                        dCol[cam]=-1;
                    }
                }
                if(covered>0){
                    cv::Vec3b intensity =image[cameraMinDistanceCenter].at<cv::Vec3b>(dRow[cameraMinDistanceCenter],dCol[cameraMinDistanceCenter]);
                    panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) intensity.val[0];
                    panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) intensity.val[1];
                    panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) intensity.val[2];
                    //panoramic.ptr<unsigned char>(y,x)[3]=(unsigned char) 255;
                    contribution_map.at<unsigned char>(y,x)=(unsigned char) cameraMinDistanceCenter;
                    if(secondCameraMinDistanceCenter>-1){
                      second_contribution_map.at<unsigned char>(y,x)=(unsigned char) secondCameraMinDistanceCenter;
                    }
                }
                else if(parallax){
                  panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) 0;
                  panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) 0;
                  panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) 0;
                }
                else{
                    //This might cause slight problems beacuse extrinsics between the cameras are not constant, so for some cases I put it balck and not red.
                    //We would need to generate the virtual individual depthmaps instead of registered pose
                    panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) 0;
                    panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) 0;
                    panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) 0;
                }
            }
            else{
                panoramic.ptr<unsigned char>(y,x)[0]=(unsigned char) 0;
                panoramic.ptr<unsigned char>(y,x)[1]=(unsigned char) 0;
                panoramic.ptr<unsigned char>(y,x)[2]=(unsigned char) 0;
            }
        }
        /*
        done[y]=true;

        int numberDone=0;
        for( int i = 0; i < pano_height; i++ ) {
            if( done[i]==true ) {
                numberDone++;
            }
        }
        std::cout << ((float)(numberDone)/(float)(pano_height))*100 << "%" << "\r" << std::flush;
        */
    }

    //Display
    /*cv::namedWindow("Display window", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow( "Display window", panoramic );
    cv::waitKey();
    */

    string pano_filename;
    ss.str("");
    pano_filename.clear();
    if(haveDepthMap && first_camera!=last_camera){
      ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << first_frame << ".png";
    }
    else if(haveDepthMap){
      ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_cam"<< first_camera <<".png";
    }
    else{
      ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_radius"<< R <<".png";
    }
    pano_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_PNG_COMPRESSION);
    qualityType.push_back(9); //Jpeg quality

    cout << pano_filename << endl;

    bool writing_success;
    writing_success=imwrite(pano_filename, panoramic, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    else{
      cout << "Image written in " << pano_filename << endl;
    }

    if(first_camera!=last_camera){
      ss.str("");
      ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_contribution_map.png";
      pano_filename=ss.str();
      writing_success=imwrite(pano_filename, contribution_map, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename << endl;
      }

      ss.str("");
      ss << file_part0 << "pano"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_2nd_contribution_map.png";
      pano_filename=ss.str();
      writing_success=imwrite(pano_filename, second_contribution_map, qualityType);
      if(!writing_success){
          cout << "\nError writing image! Check output directory existence." << endl;
          return 1;
      }
      else{
        cout << "Image written in " << pano_filename << endl;
      }
    }


    /*
    if(haveDepthMap){
        cv::Mat superImposed;
        cv::cvtColor(adjMap, adjMap, CV_GRAY2RGB);
        cv::addWeighted( panoramic, 0.3, adjMap, 0.7, 0.0, superImposed);
        ss.str("");
        if(haveDepthMap && first_camera!=last_camera){
          ss << file_part0 << "pano_superimposed"<< file_part2 << setfill('0')<< setw(5) << first_frame << ".png";
        }
        else if(haveDepthMap){
          ss << file_part0 << "pano_superimposed"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_cam"<< first_camera <<".png";
        }
        else{
          ss << file_part0 << "pano_superimposed"<< file_part2 << setfill('0')<< setw(5) << first_frame << "_radius"<< R <<".png";
        }
        pano_filename=ss.str();

        cout << pano_filename << endl;

        writing_success=imwrite(pano_filename, superImposed, qualityType);
        if(!writing_success){
            cout << "\nError writing image! Check output directory existence." << endl;
            return 1;
        }
        else{
          cout << "Image written in " << pano_filename << endl;
        }
    }
    */

    cout << "Success!" << endl;

    return 0;
}
