#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sstream>
#include <opencv2/core/core.hpp> 
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <omp.h> //Parallel
#include <fstream>

//LadybugCalibration class
#include "mcs_calibration.hpp" 

#define PI 3.14159265358979323846

using namespace cv; 
using namespace std;

bool isBiggerAngle(double alpha, double beta){
alpha=fmod(alpha,2*PI);
beta=fmod(beta,2*PI);
//Alpha and beta are 0<=anlge<=2*PI
bool bigger=false;
if((alpha-beta>0) && (alpha-beta)<PI){
        bigger=true;
}
else if((alpha-beta<0) && (alpha-beta)<-PI){
        bigger=true;
}
return bigger;
}

double degrees(double alpha){
        return alpha*180/PI;
}

void printMat(Mat mat, int prec)
{      
    for(int i=0; i<mat.size().height; i++)
    {
        cout << "[";
        for(int j=0; j<mat.size().width; j++)
        {
            cout << setprecision(prec) << mat.at<double>(i,j);
            if(j != mat.size().width-1)
                cout << ", ";
            else
                cout << "]" << endl; 
        }
    }
}
void printhelp(char *argv[]){
    fprintf(stderr,"Usage: %s image_colour DepthMap\n",argv[0]);
}

bool read_depth( string depth_file, int& width, int& height, std::vector<double>&  data)
{

    ifstream file(depth_file.c_str());
    string line;

    // get the full line, spaces and all
    bool first_line=true;
    height=0;
    while(getline(file,line)){
        std::istringstream iss(line);
        for(std::string s; iss >> line; ){
            data.push_back(atof(line.c_str()));
        }
        if(first_line){
            first_line=false;
            width=data.size();
        }
        height++;
    }
    return true;
}

int main(int argc, char *argv[])
{

    if(argc < 3)
    {
        printhelp(argv);
        return(1);
    }

    string image_path=string(argv[1]);
    cv::Mat originalImage = imread(image_path, IMREAD_COLOR);
    if(originalImage.data==NULL){
        cout << "Error reading image: "<< image_path << endl;
        return 1;
    }
    cout << "Original image read." << endl;
    
    cv::Mat scaledDepthMap, adjMap;
    cout << "Depth map" << endl;
    string depth_map=string(argv[2]);
    int width_depth_map,height_depth_map;
    std::vector<double> data;
    read_depth(depth_map,width_depth_map, height_depth_map, data);
    cout << width_depth_map << " , " << height_depth_map << endl;
        
    //cv::Mat depthMap(width_depth_map/2,width_depth_map,CV_64F);
    cv::Mat depthMap(height_depth_map,width_depth_map,CV_64F);
    memcpy(depthMap.data, data.data(), data.size()*sizeof(double));
        
    double min, max;
    cv::minMaxLoc(depthMap, &min, &max);
        
    cout << "Max" << max << endl;
        
    //cv::Mat adjMap;
    cv::convertScaleAbs(depthMap, adjMap, 255 / max);
    
    cv::namedWindow("Depth Map", WINDOW_NORMAL | WINDOW_KEEPRATIO);
    cv::imshow("Depth Map",adjMap);
    cv::waitKey();
    
    stringstream ss;
    ss.str("");
    ss << "depth_map.png";
    std::string out_filename=ss.str();

    std::vector<int> qualityType;
    qualityType.push_back(IMWRITE_JPEG_QUALITY);
    qualityType.push_back(85); //Jpeg quality

    bool writing_success=imwrite(out_filename, adjMap, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }


    cv::Mat superImposed;
    cv::cvtColor(adjMap, adjMap, CV_GRAY2RGB);
    cv::addWeighted( originalImage, 0.3, adjMap, 0.7, 0.0, superImposed);
    ss.str("");
    ss << "super_imposed.png";
    out_filename=ss.str();

    cout << out_filename << endl;    

    writing_success=imwrite(out_filename, superImposed, qualityType);
    if(!writing_success){
        cout << "\nError writing image! Check output directory existence." << endl;
        return 1;
    }
    
    
    cout << "Success!" << endl;

    return 0;
}
