#!/usr/bin/env python

import ipdb
import numpy as np
import cv2
from matplotlib import pyplot as plt

img1 = cv2.imread('/media/jep/DataSSD/ladybug_workspace/posters/girona_05_octave.png',0)          # queryImage
img2 = cv2.imread('/media/jep/DataSSD/ladybug_workspace/calib_config4_images/camera_extrinsics/camera5_frame00511.jpg',0) # trainImage

# Initiate SIFT detector
sift = cv2.xfeatures2d.SIFT_create()

# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)

# BFMatcher with default params
bf = cv2.BFMatcher()
matches = bf.knnMatch(des1,des2, k=2)

# Apply ratio test
good = []
for m,n in matches:
    if m.distance < 0.65*n.distance:
        good.append([m])

# cv2.drawMatchesKnn expects list of lists as matches.
img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2 or 1)
ipdb.set_trace()

plt.imshow(img3)
#plt.setp(axes, adjustable='box-forced')
#plt.tight_layout()
plt.savefig("plot3.pdf", bbox_inches="tight",format='pdf', transparent=False)
plt.show()
