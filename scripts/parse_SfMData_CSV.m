function [ sfm_data ] = parse_SfMData_CSV( file_csv_path )
    % Parse CSV file with ';' as delimiter
    data_mat = importdata(file_csv_path,';');

    sfm_data = {};
    for ii = 1:size(data_mat.data,1)
        % Image name
        sfm_data{ii}.img = data_mat.textdata{ii+1,1};
        % Camera and Frame ID
        cam_frame = sscanf(sfm_data{ii}.img,'camera%i_frame%f.jpg');
        sfm_data{ii}.cam_id = cam_frame(1);
        sfm_data{ii}.frame_id = cam_frame(2);
        % Rotation
        sfm_data{ii}.R = reshape(data_mat.data(ii,1:9),3,3)'; % we need to transpose as matlab is columnwise
        % Translation
        sfm_data{ii}.t = reshape(data_mat.data(ii,10:12),3,1);
    end

end

