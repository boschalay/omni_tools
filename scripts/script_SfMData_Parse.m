clc;
clear all;
close all;
%% Load data
%file_csv_path = '/home/jep/Downloads/boreas_orig_1fps_eq001_gsfm_constraints_single_camera_poses.txt';
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/gsfm_f_06_m_06_eq001_adjall_intr_soft_ext_poses.csv';
%1 Fairly consistent. 2 Not very consistent. 3 Consistent but very few
%frames registered %5 Not very consistent
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/gsfm_f_06_m_06_eq001_fix_intr_soft_ext_poses.csv';
%1 Not consistent. 2 Not very consistent. 3 Few frames not consistent. 4
%Consistent (1cm but doesn't look right.)
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/gsfm_f_06_m_08_eq001_adjall_intr_soft_ext_poses.csv';
% 1 Consistent. 2 Not  consistent . 3 Few frames not very consistent. 5 Not
% consistent
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/gsfm_f_06_m_08_eq001_fix_intr_soft_ext_poses.csv';
% 1 Not very consistent. 2 Bad. 3Not very consistent. 5 Not very consistent
% 1.2cm
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/isfm_f_06_m_08_eq001_fix_intr_soft_ext_poses.csv';
%1 bad. 2 bad. 3bad. 5bad
%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles/gsfm_f_06_m_06_eq001_adjall_intr_soft_ext_poses.csv';
%1 fairly consistent. 2 not very cons. 3 not very. 5 not very

%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles_25_01/gsfm_f_06_m_08_eq001_adjall_intr_hard_ext_poses.csv';
% MUch better. 3 doesnt have a lot registered


%file_csv_path = '/home/jep/Downloads/BoreasCSVFiles_25_01/isfm_f_06_m_08_eq001_adjall_intr_hard_ext_poses.csv';

%file_csv_path = '/home/jep/Downloads/gsfm_f_06_m_08_eq001_fix_new_intr_hard_ext_prunned_guided_BA_poses.csv';
file_csv_path = '/home/jep/wd_boreas_f23700_24300_10fps_eq001/gsfm_f_06_m_08_eq001_fix_new_intr_hard_ext/gsfm_f_06_m_08_eq001_fix_new_intr_hard_ext_prunned_guided_BA_poses.csv';
%file_csv_path = '/home/jep/Downloads/TulsAm_4110_4710_selected_10fps_gsfm_intr_free_constr_hard_poses.csv';
%

sfm_data = parse_SfMData_CSV_new(file_csv_path);
%sfm_data = parse_SfMData_CSV(file_csv_path);

% Construct the data about the cameras and positions in the table
sfm_data_table = zeros(0,14);
for ii = 1:size(sfm_data,2)
    sfm_data_table(ii,:) = [sfm_data{ii}.cam_id, sfm_data{ii}.frame_id, reshape(sfm_data{ii}.R,1,9), sfm_data{ii}.t'];
end

%% Process data
% Loop through the frames and find cameras corresponding to the same frame
% Camera 5 is the one easier to register, so we find the transformation
% between 5 and the others.


trans1toCenter=[];
trans2toCenter=[];
trans3toCenter=[];
trans5toCenter=[];

for frame_id = min(sfm_data_table(:,2)):max(sfm_data_table(:,2))
    % Get camera ids that reconstruct this frame
    cam_ids = sfm_data_table(sfm_data_table(:,2)==frame_id,1);
   
    % If no cameras reconstructed this frame -> skip frame
    if isempty(cam_ids)
        continue;
    end
    
    % Get poses of the cameras of this frame
    cam_pos = sfm_data_table(sfm_data_table(:,2)==frame_id,3:end);
    
    % We assume the only rigid transformation is from camera 4 to the
    % center.
    
    Rot4toCenter=[0.0 0.0 1.0; -1.0 0.0 0.0; -0.0 -1.0 0.0];
    trans4toCenter=[0.077; 0.0; 0.0];
    Trans4toCenter=[Rot4toCenter,trans4toCenter; 0 0 0 1];
    
    cam4_index=find(cam_ids==4);
    if ~ isempty(cam4_index) 
        for i =cam_ids.'
            if(i~=4)
                Rot4toW=reshape(sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==4),3:11),3,3);
                trans4toW=sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==4),12:end)';
                Trans4toW=[Rot4toW,trans4toW; 0 0 0 1];
                
                RotAtoW=reshape(sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==i),3:11),3,3);
                transAtoW=sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==i),12:end)';
                TransAtoW=[RotAtoW,transAtoW; 0 0 0 1];
            
                TransAtoCenter=Trans4toCenter*inv(Trans4toW)*TransAtoW;
                switch i
                    case 1
                        trans1toCenter=[trans1toCenter;[reshape(TransAtoCenter(1:3,1:3)',1,9), reshape(TransAtoCenter(1:3,4),1,3)], frame_id];
                    case 2
                        trans2toCenter=[trans2toCenter;[reshape(TransAtoCenter(1:3,1:3)',1,9), reshape(TransAtoCenter(1:3,4),1,3)], frame_id];
                    case 3
                        trans3toCenter=[trans3toCenter;[reshape(TransAtoCenter(1:3,1:3)',1,9), reshape(TransAtoCenter(1:3,4),1,3)], frame_id];
                    case 5
                        trans5toCenter=[trans5toCenter;[reshape(TransAtoCenter(1:3,1:3)',1,9), reshape(TransAtoCenter(1:3,4),1,3)], frame_id];
                end
            end
        end
    end
    
end 

%% FIND EXTRINSICS ACCORDING TO MEAN POSITION
% 
% for i= [1 2 3 5]
%    disp(['Transformation from camera ',num2str(i), ' to center']);
%     switch i
%         case 1
%                mean_diff=[trans1toCenter(:,10:12) - repmat(mean(trans1toCenter(:,10:12)),[size(trans1toCenter,1),1])];
%                norm_diff=sqrt(sum(mean_diff.^2,2));
%                [val, ind] = min(norm_diff);
%               
%                disp(['Number of frames with registartion used: ',num2str(length(trans1toCenter))])
%                res1=trans1toCenter(ind,:)';
%                rot1=wrev(rotm2eul(reshape(res1(1:9)',[3,3])'));
%                trans1=res1(10:12)';
%                extrinsics1=[rot1 trans1]
%         case 2
%                mean_diff=[trans2toCenter(:,10:12) - repmat(mean(trans2toCenter(:,10:12)),[size(trans2toCenter,1),1])];
%                norm_diff=sqrt(sum(mean_diff.^2,2));
%                [val, ind] = min(norm_diff);
%               
%                disp(['Number of frames with registartion used: ',num2str(length(trans2toCenter))])
%                res2=trans2toCenter(ind,:)';
%                rot2=wrev(rotm2eul(reshape(res2(1:9)',[3,3])'))
%                trans2=res2(10:12)';
%                extrinsics2=[rot2 trans2]
%         case 3
%                mean_diff=[trans3toCenter(:,10:12) - repmat(mean(trans3toCenter(:,10:12)),[size(trans3toCenter,1),1])];
%                norm_diff=sqrt(sum(mean_diff.^2,2));
%                [val, ind] = min(norm_diff);
%              
%                disp(['Number of frames with registartion used: ',num2str(length(trans3toCenter))])
%                res3=trans3toCenter(ind,:)';
%                rot3=wrev(rotm2eul(reshape(res3(1:9)',[3,3])'));
%                trans3=res3(10:12)';   
%                extrinsics3=[rot3 trans3]
%         case 5
%                mean_diff=[trans5toCenter(:,10:12) - repmat(mean(trans5toCenter(:,10:12)),[size(trans5toCenter,1),1])];
%                norm_diff=sqrt(sum(mean_diff.^2,2));
%                [val, ind] = min(norm_diff);
%                
%                disp(['Number of frames with registartion used: ',num2str(length(trans5toCenter))])
%                res5=trans5toCenter(ind,:)';
%                
%                rot5=wrev(rotm2eul(reshape(res5(1:9)',[3,3])'))
%                trans5=res5(10:12)';
%                extrinsics5=[rot5 trans5]
%                
%     end
% end

% FIND EXTRINSICS ACCORDING TO A frame

frame_number=23820;

for i= [1 2 3 5]
   disp(['Transformation from camera ',num2str(i), ' to center']);
    switch i
        case 1
               ind=find(trans1toCenter(:,13)==frame_number);
              
               res1=trans1toCenter(ind,:)';
               rot1=wrev(rotm2eul(reshape(res1(1:9)',[3,3])'));
               trans1=res1(10:12)';
               extrinsics1=[rot1 trans1]
        case 2
                ind=find(trans2toCenter(:,13)==frame_number);
              
               res2=trans2toCenter(ind,:)';
               rot2=wrev(rotm2eul(reshape(res2(1:9)',[3,3])'))
               trans2=res2(10:12)';
               extrinsics2=[rot2 trans2]
        case 3
               ind=find(trans3toCenter(:,13)==frame_number);
             
               res3=trans3toCenter(ind,:)';
               rot3=wrev(rotm2eul(reshape(res3(1:9)',[3,3])'));
               trans3=res3(10:12)';   
               extrinsics3=[rot3 trans3]
        case 5
               ind=find(trans5toCenter(:,13)==frame_number);
               
               res5=trans5toCenter(ind,:)';
               
               rot5=wrev(rotm2eul(reshape(res5(1:9)',[3,3])'))
               trans5=res5(10:12)';
               extrinsics5=[rot5 trans5]
               
    end
end
    
    
%% OLD    
    
%     TODO: What do we want to compute?
%     Take a camera, (reference) and compute average distance between the
%     others
%     cam5_index=find(cam_ids==5);
%     if ~ isempty(cam5_index) 
%         for i =cam_ids.'
%             cam_pos5 = sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==5),3:5);
%             switch i
%                 case 1
%                     cam_pos1 = sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==1),3:5);
%                     dis=cam_pos5-cam_pos1;
%                     trans5to1=[trans5to1; dis];
%                 case 2
%                     cam_pos2 = sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==2),3:5);
%                     dis=cam_pos5-cam_pos2;
%                     trans5to2=[trans5to2; dis];
%                 case 3
%                     cam_pos3 = sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==3),3:5);
%                     dis=cam_pos5-cam_pos3;
%                     trans5to3=[trans5to3; dis];
%                 case 4
%                     cam_pos4 = sfm_data_table((sfm_data_table(:,2)==frame_id) & (sfm_data_table(:,1)==4),3:5);
%                     dis=cam_pos5-cam_pos4;
%                     trans5to4=[trans5to4; dis];
%             end
%         end
%     end
%    
% 
% mean_trans=[mean(trans5to1); mean(trans5to2); mean(trans5to3); mean(trans5to4)];
% 
% for i= 1:4
%    disp(['Transformation camera 5 to ',num2str(i)]);
%    real_measure=0.109;
%     switch i
%         case 1
%                disp(['Number of frames with registartion of the two cameras :',num2str(length(trans5to1))])
%                disp(['Mean: ', num2str(mean(trans5to1))]);
%                disp(['Std dev: ', num2str(std(trans5to1))])
%                disp(['Scale: ', num2str(real_measure/mean(trans5to1))])
%         case 2
%             
%                disp(['Number of frames with registartion of the two cameras :',num2str(length(trans5to2))])
%                disp(['Mean: ', num2str(mean(trans5to2))]);
%                disp(['Std dev: ', num2str(std(trans5to2))])
%                disp(['Scale: ', num2str(real_measure/mean(trans5to2))])
%         case 3
%                disp(['Number of frames with registartion of the two cameras :',num2str(length(trans5to3))])
%                disp(['Mean: ', num2str(mean(trans5to3))]);
%                disp(['Std dev: ', num2str(std(trans5to3))])
%                disp(['Scale: ', num2str(real_measure/mean(trans5to3))])
%         case 4
%                disp(['Number of frames with registartion of the two cameras :',num2str(length(trans5to4))])
%                disp(['Mean: ', num2str(mean(trans5to4))]);
%                disp(['Std dev: ', num2str(std(trans5to4))])
%                disp(['Scale: ', num2str(real_measure/mean(trans5to4))])
%     end
% end



%% Plot positions of the cameras
figure;
hold on;
% Loop through the frames and find cameras corresponding to the same frame
for frame_id = min(sfm_data_table(:,2)):max(sfm_data_table(:,2))
    % Get camera ids that reconstruct this frame
    cam_ids = sfm_data_table(sfm_data_table(:,2)==frame_id,1);
    
    % If no cameras reconstructed this frame -> skip frame
    if isempty(cam_ids)
        continue;
    end
    
    % Get poses of the cameras of this frame
    cam_pos = sfm_data_table(sfm_data_table(:,2)==frame_id,12:14);
   
    % Display
    % Select color
    frame_color = rand(1,3);
    scatter3(cam_pos(:,1),cam_pos(:,2),cam_pos(:,3),20,repmat(frame_color,size(cam_ids,1),1),'filled');    
end
axis tight;
axis equal;