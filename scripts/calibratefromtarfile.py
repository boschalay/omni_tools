#!/usr/bin/env python

import numpy as np
import cv2
import glob
import os
import sys
import tarfile
import ipdb
import math
from matplotlib import pyplot as plt

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def lryaml(width, height, name, d, k, r, p):
    calmessage = (""
    + "image_width: " + str(width) + "\n"
    + "image_height: " + str(height) + "\n"
    + "camera_name: " + name + "\n"
    + "camera_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in k.reshape(1,9)[0]]) + "]\n"
    + "distortion_model: " + ("rational_polynomial" if d.size > 5 else "plumb_bob") + "\n"
    + "distortion_coefficients:\n"
    + "  rows: 1\n"
    + "  cols: 5\n"
    + "  data: [" + ", ".join(["%8f" % i for i in d.reshape(-1)]) + "]\n"
    + "rectification_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in r.reshape(1,9)[0]]) + "]\n"
    + "projection_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 4\n"
    + "  data: [" + ", ".join(["%8f" % i for i in p.reshape(1,12)[0]]) + "]\n"
    + "")
    return calmessage

class calibrate_from_tarfile():
    # mouse callback function
    def mouse_callback(self,event,x,y,flags,param):
        if event == cv2.EVENT_LBUTTONDOWN:
            col=x
            row=y
            self.cornersPosition.append([col,row])
            #cv2.circle(self.first_image, (col,row), 5, (0,0,255))
            cv2.rectangle(self.first_image_copy, (col-20,row-20),(col+20,row+20), (0,0,255),10)
            if len(self.cornersPosition) >=self.pattern_size[0]*self.pattern_size[1] :
                self.finished=True
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.finished = True

    def find_corners_normal_distortion(self):
        archive = tarfile.open(tarname, 'r')
        images_list=sorted(archive.getnames())
        for f in images_list:
            #if f.startswith('left') and (f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg') or f.endswith('JPG')):
            if (f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg') or f.endswith('JPG')):
                filedata = archive.extractfile(f).read()
                file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                h, w = img.shape[:2]
                image_w=w
                image_h=h
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

                corners2=np.array([])
                # Find the chess board corners

                # Scale the input image down to ~VGA size
                scale = math.sqrt( (w*h) / (640.*480.) )
                if scale > 1.0:
                    scrib = cv2.resize(img, (int(w / scale), int(h / scale)))
                else:
                    scrib = img
                # Due to rounding, actual horizontal/vertical scaling may differ slightly
                x_scale = float(w) / scrib.shape[1]
                y_scale = float(h) / scrib.shape[0]

                # Detect checkerboard
                found, downsampled_corners = cv2.findChessboardCorners(scrib, self.pattern_size, corners2, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

                # Scale corners back to full size image
                corners = None
                if found:
                    if scale > 1.0:
                        # Refine up-scaled corners in the original full-res image
                        # TODO Does this really make a difference in practice?
                        corners_unrefined = downsampled_corners.copy()
                        corners_unrefined[:, :, 0] *= x_scale
                        corners_unrefined[:, :, 1] *= y_scale
                        radius = int(math.ceil(scale))
                        cv2.cornerSubPix(gray, corners_unrefined, (radius,radius), (-1,-1),self.criteria)
                        corners = corners_unrefined
                    else:
                        corners = downsampled_corners

                # If found, add object points, image points (after refining them)
                if found:
                    #Refine corners
                    cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), self.criteria)
                    if self.visualize:
                        # Draw and display the corners
                        cv2.drawChessboardCorners(img, pattern_size, corners, found)
                        cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                        cv2.imshow(f,img)
                        cv2.moveWindow(f, 100, 100)
                        cv2.resizeWindow(f, 808, 616)
                        cv2.waitKey()
                        cv2.destroyWindow(f)
                #ipdb.set_trace()
                if not found:
                    print 'chessboard not found'
                    continue
                img_points.append(corners.reshape(-1, 2))
                obj_points.append(self.pattern_points)
        cv2.destroyAllWindows()

    def find_corners_fisheye(self):

        archive = tarfile.open(tarname, 'r')
        images_list=sorted(archive.getnames())
        first_image=images_list[0]
        filedata = archive.extractfile(first_image).read()
        file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
        first_image=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
        h, w = first_image.shape[:2]

        found_values=False
        while not found_values:
            focal_length = float(raw_input("Focal Length guess: "))
            d1= float(raw_input("Distortion Coeff1 guess: "))

            camera_matrix = np.eye(3)
            camera_matrix[0][0]=focal_length
            camera_matrix[1][1]=focal_length
            camera_matrix[0][2]=(first_image.shape[::-1][0]-1)/2
            camera_matrix[1][2]=(first_image.shape[::-1][1]-1)/2

            dist_coeffs = np.zeros(4)
            dist_coeffs[0]=d1
            mapx,mapy=cv2.fisheye.initUndistortRectifyMap(camera_matrix,dist_coeffs,np.eye(3),camera_matrix,first_image.shape[:-1],cv2.CV_32FC1)
            undist_image = cv2.remap(first_image,mapx,mapy,cv2.INTER_LINEAR)
            cv2.namedWindow("Undistorted Image",cv2.WINDOW_NORMAL)
            cv2.imshow("Undistorted Image",undist_image)
            #cv2.moveWindow("Undistorted Image", 100, 100)
            #cv2.resizeWindow("Undistorted Image", 808, 616)
            cv2.waitKey()

            found_values=query_yes_no("Was result OK?")

        
        print "Fisheye distortion model"
        print "Click manually corners in the first image"

        archive = tarfile.open(tarname, 'r')
        images_list=sorted(archive.getnames())
        first_image=images_list[0]
        filedata = archive.extractfile(first_image).read()
        file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
        self.first_image=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
        self.first_image_copy=self.first_image.copy()
        h, w = self.first_image.shape[:2]

        cv2.namedWindow('image',cv2.WINDOW_NORMAL) # Can be resized
        cv2.resizeWindow('image', w, h) #Reasonable size window
        #cv2.resizeWindow('image', 500, 500) #Reasonable size window
        cv2.setMouseCallback('image',self.mouse_callback) #Mouse callback
        self.finished=False
        self.cornersPosition=list()
        while(not self.finished):
                cv2.imshow('image',self.first_image_copy)
                k = cv2.waitKey(4) & 0xFF
                if k == 27:
                        breakim

        cv2.destroyAllWindows()
        self.cornersPositionArray=np.asarray(self.cornersPosition,np.float32).reshape(-1,1,2)
        
        #9x11 kodak outdoor
        #self.cornersPositionArray=np.asarray([861.7645874023438, 660.9724731445312, 1003.091064453125, 618.195556640625, 1165.35205078125, 593.497802734375, 1341.0950927734375, 596.0742797851562, 1519.0782470703125, 605.4767456054688, 1683.3319091796875, 645.4627685546875, 1826.1402587890625, 695.7969360351562, 1943.6683349609375, 751.3374633789062, 2038.1124267578125, 805.0972900390625, 805.595458984375, 771.70068359375, 950.2947387695312, 727.3895263671875, 1126.659912109375, 696.0535888671875, 1322.0096435546875, 690.0691528320312, 1520.49072265625, 709.4130859375, 1700.5853271484375, 749.9556274414062, 1852.13330078125, 802.456298828125, 1972.02734375, 856.6741333007812, 2067.86865234375, 909.924560546875, 751.7449951171875, 913.3694458007812, 901.1812744140625, 867.2924194335938, 1084.9207763671875, 840.7081909179688, 1310.5849609375, 838.393310546875, 1521.4735107421875, 853.090087890625, 1716.8165283203125, 891.649169921875, 1875.205078125, 939.1107788085938, 1998.66552734375, 988.322021484375, 2092.265625, 1033.7486572265625, 709.740966796875, 1087.7841796875, 857.9862060546875, 1052.88720703125, 1055.7120361328125, 1029.1119384765625, 1295.6966552734375, 1024.266845703125, 1520.4239501953125, 1046.617919921875, 1723.8746337890625, 1074.3927001953125, 1888.9208984375, 1107.2718505859375, 2013.3330078125, 1140.883544921875, 2106.074462890625, 1172.572509765625, 689.9978637695312, 1286.88037109375, 838.1195678710938, 1270.60009765625, 1044.4727783203125, 1260.2633056640625, 1288.4896240234375, 1264.36865234375, 1517.2779541015625, 1273.55712890625, 1718.7054443359375, 1282.2086181640625, 1887.9630126953125, 1294.5555419921875, 2015.49951171875, 1308.3214111328125, 2108.321533203125, 1321.97802734375, 698.6395874023438, 1494.8485107421875, 847.8134765625, 1498.1497802734375, 1053.8397216796875, 1498.4130859375, 1293.1112060546875, 1494.9222412109375, 1513.396484375, 1490.3856201171875, 1709.0841064453125, 1486.3399658203125, 1875.718505859375, 1481.9566650390625, 2004.1558837890625, 1477.4208984375, 2097.639892578125, 1472.738525390625, 734.3432006835938, 1687.791259765625, 881.9644165039062, 1706.2677001953125, 1077.1448974609375, 1714.9468994140625, 1299.0601806640625, 1707.4510498046875, 1510.3868408203125, 1691.531982421875, 1698.095458984375, 1675.1947021484375, 1857.4112548828125, 1656.8416748046875, 1982.4063720703125, 1636.4102783203125, 2076.6171875, 1616.4442138671875, 786.3436889648438, 1851.4317626953125, 930.7693481445312, 1878.0413818359375, 1110.0740966796875, 1892.30517578125, 1300.8349609375, 1888.7510986328125, 1506.2154541015625, 1870.3331298828125, 1683.8465576171875, 1841.2979736328125, 1833.1890869140625, 1810.2581787109375, 1953.5740966796875, 1777.5701904296875, 2047.3846435546875, 1745.8756103515625, 844.3902587890625, 1982.2174072265625, 982.9574584960938, 2010.6796875, 1146.910400390625, 2026.21875, 1321.4200439453125, 2022.276611328125, 1540.7908935546875, 1998.0902099609375, 1665.3719482421875, 1973.522216796875, 1805.0948486328125, 1936.4324951171875, 1920.651611328125, 1896.910888671875, 2013.405517578125, 1858.7049560546875, 900.9761352539062, 2083.632080078125, 1032.3519287109375, 2109.74560546875, 1175.3040771484375, 2120.225830078125, 1340.7657470703125, 2123.356689453125, 1470.3270263671875, 2082.583740234375, 1645.0899658203125, 2076.500732421875, 1776.32470703125, 2035.5902099609375, 1886.62744140625, 1994.565673828125, 1978.1729736328125, 1954.3504638671875, 954.3865356445312, 2163.624755859375, 1076.2452392578125, 2184.265380859375, 1214.0, 2188.0, 1348.0, 2191.0, 1495.3106689453125, 2173.1455078125, 1642.0013427734375, 2168.730712890625, 1749.631591796875, 2112.68359375, 1843.6256103515625, 2099.345458984375, 1942.58203125, 2032.601806640625]).reshape(-1,1,2)
        
        #print self.cornersPositionArray
        #Now we should refine the corners detection
        gray = cv2.cvtColor(self.first_image,cv2.COLOR_BGR2GRAY)
        #cv2.cornerSubPix(gray, self.cornersPositionArray, (40,40), (-1,-1),( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 300, 0.01 ))
        #print self.cornersPositionArray
        # Draw and display the corners

        cv2.drawChessboardCorners(self.first_image_copy, self.pattern_size, self.cornersPositionArray,True)
        cv2.namedWindow("Corners",cv2.WINDOW_NORMAL)
        cv2.imshow("Corners",self.first_image_copy)
        cv2.moveWindow("Corners", 100, 100)
        cv2.resizeWindow("Corners", 808, 616)
        cv2.waitKey()
        cv2.destroyWindow("Corners")

        #Now that we have the corners refined for first image we try to calibrate with fake values.

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((self.pattern_size[1]*self.pattern_size[0],3), np.float32)
        objp[:,:2] = np.mgrid[0:self.pattern_size[0],0:self.pattern_size[1]].T.reshape(-1,2)
        objpoints = [] # 3d point in real world space
        objpoints.append(objp)
        imgpoints=[] # 2d points in image plane.
        imgpoints.append(self.cornersPositionArray)

        obj_points = np.asarray([objpoints],dtype='float64').reshape(-1,1,self.pattern_size[0]*self.pattern_size[1],3)
        img_points = np.asarray([imgpoints],dtype='float64').reshape(-1,1,self.pattern_size[0]*self.pattern_size[1],2)

        rvecs=np.asarray([[[np.zeros(3).tolist() for i in xrange(obj_points.shape[0])]]],dtype='float64').reshape(-1,1,1,3)
        tvecs=np.asarray([[[np.zeros(3).tolist() for i in xrange(obj_points.shape[0])]]],dtype='float64').reshape(-1,1,1,3)

        camera_matrix = np.eye(3)
        dist_coeffs = np.zeros(4)

        camera_matrix = np.eye(3)
        camera_matrix[0][0]=1000
        camera_matrix[1][1]=1000
        camera_matrix[0][2]=(gray.shape[::-1][0]-1)/2
        camera_matrix[1][2]=(gray.shape[::-1][1]-1)/2

        calib_flags=cv2.fisheye.CALIB_FIX_SKEW + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_USE_INTRINSIC_GUESS
        ret, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(obj_points, img_points, gray.shape[::-1], camera_matrix, dist_coeffs,rvecs,tvecs,flags=calib_flags)
        print ret
        print mtx
        print dist

        newK=cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(mtx,dist,gray.shape[::-1],np.eye(3),0.0)
        #newK=cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(mtx,dist,gray.shape[::-1],np.eye(3),1.0)

        mapx,mapy=cv2.fisheye.initUndistortRectifyMap(mtx,dist,np.eye(3),newK,gray.shape[::-1],cv2.CV_32FC1)
        dst = cv2.remap(self.first_image,mapx,mapy,cv2.INTER_LINEAR)
        cv2.namedWindow("Undist",cv2.WINDOW_NORMAL)
        cv2.imshow("Undist",dst)
        cv2.moveWindow("Undist", 100, 100)
        cv2.resizeWindow("Undist", 808, 616)
        cv2.waitKey()
        
        #Now read all images

        archive = tarfile.open(tarname, 'r')
        images_list=sorted(archive.getnames())
        for f in images_list:
            #if f.startswith('left') and (f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg') or f.endswith('JPG')):
            if (f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg') or f.endswith('JPG')):
                print f
                filedata = archive.extractfile(f).read()
                file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                h, w = img.shape[:2]
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
                #img_rect = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)
                img_rect=img        
                gray_rect = cv2.cvtColor(img_rect,cv2.COLOR_BGR2GRAY)
                
                

                # Find the chess board corners

                # Scale the input image down to ~VGA size
                scale = w / 640. /4.0
                if scale > 1.0:
                    scrib = cv2.resize(img_rect, (int(w / scale), int(h / scale)))
                else:
                    scrib = img_rect
                # Due to rounding, actual horizontal/vertical scaling may differ slightly
                x_scale = float(w) / scrib.shape[1]
                y_scale = float(h) / scrib.shape[0]

                # Detect checkerboard
                found, downsampled_corners = cv2.findChessboardCorners(scrib, self.pattern_size, flags=cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)
                if found:
                    print "Chessboard found"
                    cv2.drawChessboardCorners(scrib, self.pattern_size, downsampled_corners, found)
                else:
                    print "Chessboard not found"
                cv2.namedWindow("test",cv2.WINDOW_NORMAL)
                cv2.imshow("test",scrib)
                cv2.waitKey()

                # Scale corners back to full size image
                corners = None
                if found:
                    if scale > 1.0:
                        # Refine up-scaled corners in the original full-res image
                        # TODO Does this really make a difference in practice?
                        corners_unrefined = downsampled_corners.copy()
                        corners_unrefined[:, :, 0] *= x_scale
                        corners_unrefined[:, :, 1] *= y_scale
                        radius = int(math.ceil(scale))
                        cv2.cornerSubPix(gray_rect, corners_unrefined, (radius,radius), (-1,-1),self.criteria)
                        corners = corners_unrefined
                        cv2.drawChessboardCorners(img_rect, self.pattern_size, corners, found)
                        cv2.namedWindow("test",cv2.WINDOW_NORMAL)
                        cv2.imshow("test",img_rect)
                        cv2.waitKey()

                    else:
                        corners = downsampled_corners

                # If found, add object points, image points (after refining them)
                if found:
                    #Refine corners
                    cv2.cornerSubPix(gray_rect, corners, (11, 11), (-1, -1), self.criteria)
                    cv2.namedWindow("test",cv2.WINDOW_NORMAL)
                    cv2.imshow("test",img_rect)
                    cv2.waitKey()
                    #Distort points
                    corners=np.vstack(((corners.reshape(-1,2)[:,0]-newK[0][2]*np.ones((len(corners))))/newK[0][0],(corners.reshape(-1,2)[:,1]-newK[1][2]*np.ones((len(corners))))/newK[1][1])).T
                    corners_dist=cv2.fisheye.distortPoints(corners.reshape(-1,1,2),mtx,dist)
                    cv2.cornerSubPix(gray, corners_dist.astype('float32'), (11, 11), (-1, -1), self.criteria)

                    if self.visualize:
                        # Draw and display the corners
                        cv2.drawChessboardCorners(img, self.pattern_size, corners_dist.astype('float32'), found)
                        cv2.namedWindow("Original",cv2.WINDOW_NORMAL)
                        cv2.imshow("Original",img)
                        cv2.moveWindow("Original", 100, 100)
                        cv2.resizeWindow("Original", 808, 616)
                        cv2.waitKey()
                        cv2.destroyWindow("Original")

                if not found:
                    print 'chessboard not found'
                    continue
                self.img_points.append(corners_dist.reshape(-1, 2))
                self.obj_points.append(self.pattern_points)

        cv2.destroyAllWindows()






    def cal_from_tarfile(self,tarname, size, calib_flags = 0, visualize = False, alpha=1.0, delay=5, fisheye = False):

        self.visualize=visualize

        # termination criteria
        self.criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        self.pattern_size = size
        self.pattern_points = np.zeros( (np.prod(self.pattern_size), 3), np.float32 )
        self.pattern_points[:,:2] = np.indices(self.pattern_size).T.reshape(-1, 2)#*0.056

        #print pattern_points

        # Arrays to store object points and image points from all the images.
        self.obj_points = []# 3d point in real world space. It will be a list of arrays
        self.img_points = []# 2d points in image plane. It will be a list of arrays

        # Size images
        image_w=640
        image_h=480

        h,w=image_h, image_w
        if not fisheye:
            self.find_corners_normal_distortion()
        else:
            self.find_corners_fisheye()


        if(len(self.obj_points)==0):
            print "No corners found in images. Can't proceed to calibration"
            return

        '''
        camera_matrix = np.eye(3)
        camera_matrix[0][0]=750 #750 -> 3.3 mm #670 -> 2.95mm
        camera_matrix[1][1]=750
        camera_matrix[0][2]=(image_w-1)/2
        camera_matrix[1][2]=(image_h-1)/2

        calib_flags+=cv2.CALIB_USE_INTRINSIC_GUESS

        if calib_flags & cv2.CALIB_RATIONAL_MODEL:
            dist_coeffs = np.zeros(8) # rational polynomial
        else:
            dist_coeffs = np.zeros(5) # plumb bob
        img_n = len(img_points)
        rvecs = [np.zeros(3) for i in xrange(img_n)]
        tvecs = [np.zeros(3) for i in xrange(img_n)]

        #dist_coeffs[0]=1 #WARNING: I set this because it makes better the calibration for 2.95 mm kind

        print camera_matrix

        # TODO: Termination criteria must depen on how strict we are...
        # termination criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10000, 0.001)

        ret, camera_matrix, dist_coeffs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), camera_matrix, dist_coeffs, rvecs, tvecs, calib_flags, criteria)
        print "Final reprojection error opencv: ", ret
        print camera_matrix
        print dist_coeffs

        if self.visualize:
            newcameramtx, roi=cv2.getOptimalNewCameraMatrix(camera_matrix,dist_coeffs,(w,h),alpha,(w,h))

            # undistort
            mapx,mapy = cv2.initUndistortRectifyMap(camera_matrix,dist_coeffs,None,newcameramtx,(w,h),5)

            for f in archive.getnames():
                if f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg'):
                    filedata = archive.extractfile(f).read()
                    file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                    img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                    dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)
                    cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                    cv2.imshow(f,dst)
                    cv2.moveWindow(f, 100, 100)
                    cv2.resizeWindow(f, 808, 616)
                    cv2.waitKey()
                    #cv2.imwrite(os.path.splitext(f)[0]+"_undistorted.png", dst)
                    cv2.destroyWindow(f)

            cv2.destroyAllWindows()

        #Compute mean of reprojection error
        tot_error=0
        total_points=0
        for i in xrange(len(obj_points)):
            reprojected_points, _ = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coeffs)
            reprojected_points=reprojected_points.reshape(-1,2)
            tot_error+=np.sum(np.abs(img_points[i]-reprojected_points)**2)
            total_points+=len(obj_points[i])

        rms=np.sqrt(tot_error/total_points)
        print "RMS of reprojection error: ", rms
        print "Total number of images used:", len(obj_points)
        print "Total of features used: ", total_points


        # R is identity matrix for monocular calibration
        R = np.eye(3, dtype=np.float64)
        P = np.zeros((3, 4), dtype=np.float64)

        #Alpha set as 0 (zoomed in)
        ncm, _ = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coeffs, (w,h), 0.0)
        for j in range(3):
            for i in range(3):
                P[j,i] = ncm[j, i]

        content=lryaml(w, h, "camera", dist_coeffs, camera_matrix, R, P)
        with open('camera_calibration.yaml','w') as f:
            f.write(content)


        #==========Show how features are distributed ==================
        ax=plt.subplot(111)
        for distorted_pts_image in img_points:
            for i in range(np.alen(distorted_pts_image)):
                    #Could be done better with random selection inside square. numpy digitize could be heplful
                    x0,y0 = (distorted_pts_image[i,0],distorted_pts_image[i,1])
                    ax.plot(x0, y0, '+', c='b')
        ax.set_xlim([0, image_w])
        ax.set_ylim([0, image_h])
        ax.invert_yaxis()
        ax.set_aspect('equal')
        plt.show()

        #==========Show how features are distributed with colormap of error. ==================
        all_points=np.array([[]]);
        for i in range(len(img_points)):
            distorted_pts_image=img_points[i] #Array original points
            reprojected_points, _ = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coeffs)
            reprojected_points=reprojected_points.reshape(-1,2) #Array reprojected points
            reprojected_error=distorted_pts_image-reprojected_points #Array reprojection errors.
            reprojected_error=np.sum(np.abs(reprojected_error)**2,axis=-1)**(1./2) #Array reprojection error norm
            points_and_error=np.hstack([distorted_pts_image,reprojected_error.reshape(-1,1)])
            all_points=np.vstack([all_points.reshape(-1,3),points_and_error])
        ax=plt.subplot(111)
        ax.set_xlim(0, image_w)
        ax.set_ylim(0, image_h)
        ax.invert_yaxis()
        ax.set_aspect('equal')
        sc= ax.scatter(all_points[:,0], all_points[:,1], c=all_points[:,2], vmin=0, vmax=max(all_points[:,2]), cmap='jet',marker='+', s=25, lw = 0.5)
        #np.max(all_points[:,2])
        plt.colorbar(sc)
        plt.show(sc)
        '''


if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser("%prog TARFILE [ opts ]")
    parser.add_option("-s", "--size", default="10x10", action="store", type="string", dest="size", help="specify chessboard size as NxM [default: 10x10]")
    parser.add_option("--fix-principal-point", action="store_true", default=False,
                     help="fix the principal point at the image center")
    parser.add_option("--fix-aspect-ratio", action="store_true", default=False,
                     help="enforce focal lengths (fx, fy) are equal")
    parser.add_option("--zero-tangent-dist", action="store_true", default=False,
                     help="set tangential distortion coefficients (p1, p2) to zero")
    parser.add_option("-k", "--k-coefficients", type="int", default=2, metavar="NUM_COEFFS",
                     help="number of radial distortion coefficients to use (up to 6, default %default)")
    parser.add_option("--fisheye", action="store_true", default=False, help="Use fisheye distortion model")
    parser.add_option("--visualize", action="store_true", default=False,
                     help="visualize rectified images after calibration")
    parser.add_option("-a", "--alpha", type="float", default=1.0, metavar="ALPHA",
                     help="zoom for visualization of rectifies images. Ranges from 0 (zoomed in, all pixels in calibrated image are valid) to 1 (zoomed out, all pixels in  original image are in calibrated image). default %default)")
    parser.add_option("-d", "--delay", type="int", default=50 , metavar="DELAY",
                     help="miliseconds of delay btween images (default %default)")

    options, args = parser.parse_args()

    size = tuple([int(c) for c in options.size.split('x')])

    if not args:
        parser.error("Must give tarfile name")
    if not os.path.exists(args[0]):
        parser.error("Tarfile %s does not exist. Please select valid tarfile" % args[0])

    tarname = args[0]

    num_ks = options.k_coefficients

    calib_flags = 0
    if options.fix_principal_point:
        calib_flags += cv2.CALIB_FIX_PRINCIPAL_POINT
    if options.fix_aspect_ratio:
        calib_flags += cv2.CALIB_FIX_ASPECT_RATIO
    if options.zero_tangent_dist:
        calib_flags += cv2.CALIB_ZERO_TANGENT_DIST
    if (num_ks > 3):
        calib_flags += cv2.CALIB_RATIONAL_MODEL
    if (num_ks < 6):
        calib_flags += cv2.CALIB_FIX_K6
    if (num_ks < 5):
        calib_flags += cv2.CALIB_FIX_K5
    if (num_ks < 4):
        calib_flags += cv2.CALIB_FIX_K4
    if (num_ks < 3):
        calib_flags += cv2.CALIB_FIX_K3
    if (num_ks < 2):
        calib_flags += cv2.CALIB_FIX_K2
    if (num_ks < 1):
        calib_flags += cv2.CALIB_FIX_K1

    #calib_flags +=cv2.CALIB_THIN_PRISM_MODEL

    obj=calibrate_from_tarfile()
    obj.cal_from_tarfile(tarname, size, calib_flags, options.visualize, options.alpha, options.delay, options.fisheye)
