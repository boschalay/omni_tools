#!/usr/bin/env python

import numpy as np
import cv2
import glob
import os
import tarfile 
import ipdb
from matplotlib import pyplot as plt
import math
import yaml


def lryaml(width, height, name, d, k, r, p):
    calmessage = (""
    + "image_width: " + str(width) + "\n"
    + "image_height: " + str(height) + "\n"
    + "camera_name: " + name + "\n"
    + "camera_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in k.reshape(1,9)[0]]) + "]\n"
    + "distortion_model: " + ("rational_polynomial" if d.size > 5 else "plumb_bob") + "\n"
    + "distortion_coefficients:\n"
    + "  rows: 1\n"
    + "  cols: 5\n"
    + "  data: [" + ", ".join(["%8f" % i for i in d.reshape(-1)]) + "]\n"
    + "rectification_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in r.reshape(1,9)[0]]) + "]\n"
    + "projection_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 4\n"
    + "  data: [" + ", ".join(["%8f" % i for i in p.reshape(1,12)[0]]) + "]\n"
    + "")
    return calmessage

def cal_from_tarfile(tarname, size, calib_flags = 0, visualize = False, alpha=1.0, square=0.108):

    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 1000, 1e-5)
    image_w=1
    image_h=1
    direction_left_pattern=""

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    pattern_size = size
    pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32 )
    pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)*square

    #print pattern_points

    # Arrays to store object points and image points from all the images.
    obj_points = []# 3d point in real world space. It will be a list of arrays
    limg_points = []# 2d points in image plane. It will be a list of arrays
    rimg_points = []# 2d points in image plane. It will be a list of arrays

    archive = tarfile.open(tarname, 'r')
    for f in archive.getnames():
        if f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg'):
            if f.startswith('left'):
                filedata = archive.extractfile(f).read()
                file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                image_h, image_w = img.shape[:2] 
                
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

                corners2=np.array([])
                # Find the chess board corners
                
                # Scale the input image down to ~VGA size
                scale = math.sqrt( (image_w*image_h) / (640.*480.) )
                if scale > 1.0:
                    scrib = cv2.resize(img, (int(image_w / scale), int(image_h / scale)))
                else:
                    scrib = img
                # Due to rounding, actual horizontal/vertical scaling may differ slightly
                x_scale = float(image_w) / scrib.shape[1]
                y_scale = float(image_h) / scrib.shape[0]

                # Detect checkerboard
                found, downsampled_corners = cv2.findChessboardCorners(scrib, pattern_size, corners2, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

                # Scale corners back to full size image
                corners = None
                if found:
                    if scale > 1.0:
                        # Refine up-scaled corners in the original full-res image
                        # TODO Does this really make a difference in practice?
                        corners_unrefined = downsampled_corners.copy()
                        corners_unrefined[:, :, 0] *= x_scale
                        corners_unrefined[:, :, 1] *= y_scale
                        radius = int(math.ceil(scale))
                        cv2.cornerSubPix(gray, corners_unrefined, (radius,radius), (-1,-1),
                                                      ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1 ))
                        corners = corners_unrefined
                    else:
                        corners = downsampled_corners
                    cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
                    #Remember the order of detection of the corner
                    #direction_left_pattern=np.sign(corners[-1]-corners[0]) 

                    #ipdb.set_trace()
                    if size[1]!=size[0]:
                        if corners[0, 0, 1] > corners[-1, 0, 1]:
                            corners = np.copy(np.flipud(corners))
                    else:
                        direction_corners=(corners[-1]-corners[0])>=np.array([[0.0,0.0]])
                    
                        if not np.all(direction_corners):
                            #ipdb.set_trace()
                            if not np.any(direction_corners):
                                print "Rotated 180"
                                corners = np.copy(np.flipud(corners))
                            elif direction_corners[0][0]:
                                print "Rotated 90"
                                corners=np.rot90(corners.reshape(size[1],size[0],2)).reshape(size[0]*size[1],1,2)
                            else:
                                print "Rotated -90"
                                corners=np.rot90(corners.reshape(size[1],size[0],2),3).reshape(size[0]*size[1],1,2)
                            
                    if visualize: 
                        # Draw and display the corners
                        cv2.drawChessboardCorners(img, pattern_size, corners, found)
                        cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                        cv2.imshow(f,img)
                        cv2.moveWindow(f, 100, 100)
                        cv2.resizeWindow(f, 808, 616)
                        cv2.waitKey()
                        cv2.destroyWindow(f)
                if not found:
                    print 'chessboard not found'
                    continue
                limg_points.append(corners.reshape(-1, 2))  
                obj_points.append(pattern_points)
                
                # read right image.
                f='right'+f[4:]
                filedata = archive.extractfile(f).read()
                file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                image_h, image_w = img.shape[:2] 
                gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

                corners2=np.array([])
                # Find the chess board corners
                
                # Scale the input image down to ~VGA size
                scale = math.sqrt( (image_w*image_h) / (640.*480.) )
                if scale > 1.0:
                    scrib = cv2.resize(img, (int(image_w / scale), int(image_h / scale)))
                else:
                    scrib = img
                # Due to rounding, actual horizontal/vertical scaling may differ slightly
                x_scale = float(image_w) / scrib.shape[1]
                y_scale = float(image_h) / scrib.shape[0]

                # Detect checkerboard
                found, downsampled_corners = cv2.findChessboardCorners(scrib, pattern_size, corners2, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE)

                # Scale corners back to full size image
                corners = None
                if found:
                    if scale > 1.0:
                        # Refine up-scaled corners in the original full-res image
                        # TODO Does this really make a difference in practice?
                        corners_unrefined = downsampled_corners.copy()
                        corners_unrefined[:, :, 0] *= x_scale
                        corners_unrefined[:, :, 1] *= y_scale
                        radius = int(math.ceil(scale))
                        cv2.cornerSubPix(gray, corners_unrefined, (radius,radius), (-1,-1),
                                                      ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.1 ))
                        corners = corners_unrefined
                    else:
                        corners = downsampled_corners
                    cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
                    '''
                    # Check if the corners are in the samer order than left image
                    direction_right_pattern=np.sign(corners[-1]-corners[0]) 
                    same_direction=(direction_right_pattern==direction_left_pattern).reshape(-1).tolist()
                    if not all(same_direction):
                        #print same_direction
                        if not same_direction[0] and same_direction[1]:
                            #X inverted
                            if direction_left_pattern[0][0]==direction_left_pattern[0][1]:
                                corners=np.rot90(corners.reshape(size[0],size[1],2),3).reshape(size[0]*size[1],1,2)
                            else:
                                corners=np.rot90(corners.reshape(size[0],size[1],2)).reshape(size[0]*size[1],1,2)
                        elif same_direction[0] and not same_direction[1]:
                            #Y inverted
                            if direction_left_pattern[0][0]==direction_left_pattern[0][1]:
                                corners=np.rot90(corners.reshape(size[0],size[1],2)).reshape(size[0]*size[1],1,2)
                            else:
                                corners=np.rot90(corners.reshape(size[0],size[1],2),3).reshape(size[0]*size[1],1,2) 
                        else:
                            print "Checkerboard was inverted, reversing order."
                            corners=corners[::-1]
                    '''
                    if size[1]!=size[0]:
                        if corners[0, 0, 1] > corners[-1, 0, 1]:
                            corners = np.copy(np.flipud(corners))
                    else:
                        direction_corners=(corners[-1]-corners[0])>=np.array([[0.0,0.0]])
                    
                        if not np.all(direction_corners):
                            #ipdb.set_trace()
                            if not np.any(direction_corners):
                                print "Rotated 180"
                                corners = np.copy(np.flipud(corners))
                            elif direction_corners[0][0]:
                                print "Rotated 90"
                                corners=np.rot90(corners.reshape(size[1],size[0],2)).reshape(size[0]*size[1],1,2)
                            else:
                                print "Rotated -90"
                                corners=np.rot90(corners.reshape(size[1],size[0],2),3).reshape(size[0]*size[1],1,2)
                    if visualize: 
                        # Draw and display the corners
                        cv2.drawChessboardCorners(img, pattern_size, corners, found)
                        cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                        cv2.imshow(f,img)
                        cv2.moveWindow(f, 100, 100)
                        cv2.resizeWindow(f, 808, 616)
                        cv2.waitKey()
                        cv2.destroyWindow(f)

                if not found:
                    print 'chessboard not found'
                    continue
                rimg_points.append(corners.reshape(-1, 2))             

    cv2.destroyAllWindows()

    if calib_flags & cv2.CALIB_RATIONAL_MODEL:
        dist_coeffs = np.zeros(8) # rational polynomial
    else:
        dist_coeffs = np.zeros(5) # plumb bob

    # TODO: Termination criteria must depen on how strict we are...
    # termination criteria
    term_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10000, 0.001)
    
    #Calibrate camera separately first:
    ret, lcamera_matrix, ldist_coeffs, lrvecs, ltvecs = cv2.calibrateCamera(obj_points, limg_points, (image_w, image_h), None, None, flags=calib_flags, criteria=term_criteria)
    print "LEFT Final reprojection error opencv: ", ret
    print lcamera_matrix
    print ldist_coeffs

    #Calibrate camera separately first:
    ret, rcamera_matrix, rdist_coeffs, rrvecs, rtvecs = cv2.calibrateCamera(obj_points, rimg_points, (image_w, image_h), None, None, flags=calib_flags, criteria=term_criteria)
    print "RIGHT Final reprojection error opencv: ", ret
    print rcamera_matrix
    print rdist_coeffs

    #Fix the previous values and only compute R and T
    ret, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F= cv2.stereoCalibrate(obj_points, limg_points, rimg_points, lcamera_matrix, ldist_coeffs, rcamera_matrix, rdist_coeffs, (image_w,image_h), flags=(calib_flags + cv2.CALIB_USE_INTRINSIC_GUESS + cv2.CALIB_FIX_INTRINSIC), criteria=term_criteria)
    print "Final reprojection error opencv: ", ret
    print "CaneraMatrix left", cameraMatrix1
    print "CaneraMatrix right", cameraMatrix2
    print "Distortion left", distCoeffs1
    print "Distortion right", distCoeffs2
    print "R", R
    print "T", T
    print "E", E
    print "F", F
    
    '''
    #Compute directly intrinsic and extrinsic
    ret, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T, E, F= cv2.stereoCalibrate(obj_points, limg_points, rimg_points, (image_w,image_h), flags=calib_flags, criteria=term_criteria)
    print "Final reprojection error opencv: ", ret
    print "CameraMatrix1", cameraMatrix1
    print "DistCoeffs1", distCoeffs1
    print "CameraMatrix2", cameraMatrix2
    print "DistCoeffs2", distCoeffs2
    print "R", R
    print "T", T
    '''

    R1, R2, P1, P2,Q,roi1,roi2=cv2.stereoRectify(cameraMatrix1,distCoeffs1,cameraMatrix2, distCoeffs2, (image_w,image_h),R,T,(image_w,image_h),alpha=alpha,flags=cv2.CALIB_ZERO_DISPARITY)

    print "P1:"
    print P1
    print "P2:"
    print P2

    #Compute maps
    lmapx,lmapy=cv2.initUndistortRectifyMap(cameraMatrix1, distCoeffs1, R1, P1, (image_w,image_h),cv2.CV_32FC1)
    rmapx,rmapy=cv2.initUndistortRectifyMap(cameraMatrix2, distCoeffs2, R2, P2, (image_w,image_h),cv2.CV_32FC1)

    if visualize:  
        for f in archive.getnames():
            if f.endswith('.pgm') or f.endswith('png') or f.endswith('jpg'):
                if f.startswith('left'):
                    filedata = archive.extractfile(f).read()
                    file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                    img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                    dst = cv2.remap(img,lmapx,lmapy,cv2.INTER_LINEAR)
                    cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                    cv2.imshow(f,dst)
                    cv2.moveWindow(f, 100, 100)
                    cv2.resizeWindow(f, 808, 616)
                    cv2.waitKey()
                    cv2.destroyWindow(f)
                    # read right image.
                    f='right'+f[4:]
                    filedata = archive.extractfile(f).read()
                    file_bytes = np.asarray(bytearray(filedata), dtype=np.uint8)
                    img=cv2.imdecode(file_bytes,cv2.IMREAD_COLOR)
                    dst = cv2.remap(img,lmapx,lmapy,cv2.INTER_LINEAR)
                    cv2.namedWindow(f,cv2.WINDOW_NORMAL)
                    cv2.imshow(f,dst)
                    cv2.moveWindow(f, 100, 100)
                    cv2.resizeWindow(f, 808, 616)
                    cv2.waitKey()
                    cv2.destroyWindow(f)
        cv2.destroyAllWindows()
        
    content1=lryaml(image_w, image_h, "Bumblebee", distCoeffs1, cameraMatrix1, R1, P1)
    with open('stereo_left.yaml','w') as f:
        f.write(content1)
    
    content2=lryaml(image_w, image_h, "Bumblebee", distCoeffs2, cameraMatrix2, R2, P2)
    with open('stereo_right.yaml','w') as f:
        f.write(content2)
    

if __name__ == '__main__':
    from optparse import OptionParser
    parser = OptionParser("%prog TARFILE [ opts ]")
    parser.add_option("-s", "--size", default="10x10", action="store", type="string", dest="size", help="specify chessboard size as NxM [default: 10x10]")
    parser.add_option("-q", "--square", type="float", default=0.108, metavar="SQUARE",
                     help="specify chessboard square size in meters [default: 0.108]")
    parser.add_option("--fix-principal-point", action="store_true", default=False,
                     help="fix the principal point at the image center")
    parser.add_option("--fix-aspect-ratio", action="store_true", default=False,
                     help="enforce focal lengths (fx, fy) are equal")
    parser.add_option("--zero-tangent-dist", action="store_true", default=False,
                     help="set tangential distortion coefficients (p1, p2) to zero")
    parser.add_option("-k", "--k-coefficients", type="int", default=2, metavar="NUM_COEFFS",
                     help="number of radial distortion coefficients to use (up to 6, default %default)")
    parser.add_option("--visualize", action="store_true", default=False,
                     help="visualize rectified images after calibration")
    parser.add_option("-a", "--alpha", type="float", default=1.0, metavar="ALPHA",
                     help="zoom for visualization of rectifies images. Ranges from 0 (zoomed in, all pixels in calibrated image are valid) to 1 (zoomed out, all pixels in  original image are in calibrated image). default %default)")

    options, args = parser.parse_args()
       
    size = tuple([int(c) for c in options.size.split('x')])

    if not args:
        parser.error("Must give tarfile name")
    if not os.path.exists(args[0]):
        parser.error("Tarfile %s does not exist. Please select valid tarfile" % args[0])

    tarname = args[0]

    num_ks = options.k_coefficients

    calib_flags = 0
    if options.fix_principal_point:  
        calib_flags += cv2.CALIB_FIX_PRINCIPAL_POINT
    if options.fix_aspect_ratio:
        calib_flags += cv2.CALIB_FIX_ASPECT_RATIO
    if options.zero_tangent_dist:
        calib_flags += cv2.CALIB_ZERO_TANGENT_DIST
    if (num_ks > 3):  
        calib_flags += cv2.CALIB_RATIONAL_MODEL
    if (num_ks < 6):
        calib_flags += cv2.CALIB_FIX_K6
    if (num_ks < 5):
        calib_flags += cv2.CALIB_FIX_K5
    if (num_ks < 4):
        calib_flags += cv2.CALIB_FIX_K4
    if (num_ks < 3):
        calib_flags += cv2.CALIB_FIX_K3
    if (num_ks < 2):
        calib_flags += cv2.CALIB_FIX_K2
    if (num_ks < 1):
        calib_flags += cv2.CALIB_FIX_K1
        
    #calib_flags +=cv2.CALIB_THIN_PRISM_MODEL

    cal_from_tarfile(tarname, size, calib_flags, options.visualize, options.alpha, options.square)
