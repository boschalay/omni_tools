#!/usr/bin/python

import numpy as np
import ipdb
import os
import csv
import glob
from optparse import OptionParser


if __name__ == '__main__':
    parser = OptionParser("%prog file.csv folder")
    options, args = parser.parse_args()

    if len(args) != 2:
        parser.error("Incorrect number of arguments.")

    if not os.path.exists(args[0]):
        parser.error("File %s does not exist. Please select valid one" % args[0])

    if not os.path.exists(args[1]):
        parser.error("Folder %s does not exist. Please select valid one" % args[1])

    #Create numpy matrix and save relevant info
    data=list()
    with open(args[0], 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i=0
        for row in spamreader:
            if i>0:
                time=np.float64(row[2])/10**9
                lat=np.float64(row[4])
                lon=np.float64(row[5])
                heading=np.float64(row[17])/np.pi*180+180 #offset
                data.append([time,lat,lon,heading])
            i=i+1
    data=np.mat(data,dtype='float64')
    #All data is stored now in a numpy matrix
    '''
    files=glob.glob('frame*.txt')   
    for txtfile in files:
        f = open(txtfile, 'r')
        timestamp=float(f.readline())
        f.close()
        #Look for the timestamp in the first column of the data
        ind=np.argmax(data[:,0]>timestamp)
        myfile=open(txtfile, "a")
        myfile.write(str(data[ind,1])+"\n")#lat
        myfile.write(str(data[ind,2])+"\n")#lon
        myfile.write(str(data[ind,3])+"\n")#heading
        myfile.close()
    '''
    files=sorted(glob.glob(args[1]+'/pano_*gradblend.jpg'))
    frame=0
    for pano in files:
        timestamp=1475587029+frame/29.97 #Basis is 4 oct 15:17:09
        frame=frame+1
        #Look for the timestamp in the first column of the data
        ind=np.argmax(data[:,0]>timestamp)
        lat=data[ind-1,1]
        lon=data[ind-1,2]
        heading=str(data[ind-1,3])

        cmd='exiv2 -M"set  Exif.GPSInfo.GPSLatitude '+str(int(lat*1000000))+'/1000000 0/1 0/1" -M"set Exif.GPSInfo.GPSLatitudeRef N" -M"set  Exif.GPSInfo.GPSLongitude '+str(int(lon*1000000))+'/1000000 0/1 0/1" -M"set Exif.GPSInfo.GPSLongitudeRef E" '

        cmd=cmd+'-M"set  Xmp.GPano.UsePanoramaViewer True" -M"set  Xmp.GPano.ProjectionType equirectangular" -M"set  Xmp.GPano.CroppedAreaImageHeightPixels 2000" -M"set  Xmp.GPano.CroppedAreaImageWidthPixels 4000" '

        cmd=cmd+'-M"set  Xmp.GPano.FullPanoHeightPixels 2000" -M"set  Xmp.GPano.FullPanoWidthPixels 4000" -M"set  Xmp.GPano.CroppedAreaTopPixels 0" -M"set  Xmp.GPano.CroppedAreaLeftPixels 0" '

        cmd=cmd+'-M"set  Xmp.GPano.FirstPhotoDate " -M"set  Xmp.GPano.LastPhotoDate " -M"set  Xmp.GPano.SourcePhotosCount 5" -M"set  Xmp.GPano.PoseHeadingDegrees '+heading+'" '

        cmd=cmd + pano
        #print cmd      
        os.system(cmd)
