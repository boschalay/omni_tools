#!/usr/bin/env python

import time
import cv2.aruco as A
import numpy as np
import glob
import cv2
import ipdb
from matplotlib import pyplot as plt
import sys

def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")

def totuple(a):
    try:
        return tuple(totuple(i) for i in a)
    except TypeError:
        return a

def lryaml(width, height, name, d, k, r, p):
    calmessage = (""
    + "image_width: " + str(width) + "\n"
    + "image_height: " + str(height) + "\n"
    + "camera_name: " + name + "\n"
    + "camera_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in k.reshape(1,9)[0]]) + "]\n"
    + "distortion_model: " + ("rational_polynomial" if d.size > 5 else "plumb_bob") + "\n"
    + "distortion_coefficients:\n"
    + "  rows: 1\n"
    + "  cols: 5\n"
    + "  data: [" + ", ".join(["%8f" % i for i in d.reshape(-1)]) + "]\n"
    + "rectification_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 3\n"
    + "  data: [" + ", ".join(["%8f" % i for i in r.reshape(1,9)[0]]) + "]\n"
    + "projection_matrix:\n"
    + "  rows: 3\n"
    + "  cols: 4\n"
    + "  data: [" + ", ".join(["%8f" % i for i in p.reshape(1,12)[0]]) + "]\n"
    + "")
    return calmessage

class calibration():
    def __init__(self):
        self.dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
        self.board = cv2.aruco.CharucoBoard_create(6,6,.1,.08,self.dictionary)
        self.refPt=np.array([0,0])
        self.finished=False
        
    def click(self,event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.refPt = np.array([x, y])
            self.finished=True
        
        
    def calibrate(self):

        print cv2.__version__

        pattern_size = (5,5)
        pattern_points = np.zeros( (np.prod(pattern_size), 3), np.float32 )
        pattern_points[:,:2] = np.indices(pattern_size).T.reshape(-1, 2)#*0.1

        allCorners = []
        allIds = []
        images = [cv2.imread(file) for file in np.sort(glob.glob(args[0]+"*.png"))]
        (image_h, image_w) = images[0].shape[:2]
        camera_matrix = np.eye(3)
        camera_matrix[0][0]=1000
        camera_matrix[1][1]=1000
        camera_matrix[0][2]=(images[0].shape[::-1][1]-1)/2
        camera_matrix[1][2]=(images[0].shape[::-1][2]-1)/2
        dist_coeffs = np.zeros(4)
        images_used=[]
        indeces_images_used=[]
        
        index=0
        #We need to find the corners
        if len(args) ==1:
            for image in images:
                imageCopy=image.copy()
                #We use full resoultion because it is when we obtain better results
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                corners, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray,self.dictionary)
                if len(corners)>0:
                    charucoretval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(corners,ids,gray,self.board )
                    if charucoCorners is not None and charucoIds is not None and len(charucoCorners)>3:
                        cv2.cornerSubPix(gray, charucoCorners, (15,15), (-1,-1),(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001))
                        
                        allCorners.append(charucoCorners)
                        allIds.append(charucoIds)
                        images_used.append(image.copy())
                        indeces_images_used.append(index)
                        
                    #cv2.aruco.drawDetectedMarkers(image,corners,ids)
                    #Draws markers not corners
                    #ipdb.set_trace()
                    cv2.aruco.drawDetectedCornersCharuco(imageCopy,charucoCorners, charucoIds)
                else:
                    charucoretval=0
                    
                print "Detected corners: " + str(charucoretval)
                cv2.namedWindow('frame',cv2.WINDOW_NORMAL)
                cv2.imshow('frame',imageCopy)
                cv2.resizeWindow('frame', 600,600)
                
                
                if cv2.waitKey() & 0xFF == ord('q'):
                    break
                
                index=index+1;

            cv2.destroyAllWindows()

            #So now, we will try to put the data in a format the calibration function can understand
            objectPoints= []
            for cornerSet in allIds:
                cornerSetCoordinates=np.array([]).reshape(0,3)
                for cornerId in cornerSet:
                    cornerSetCoordinates=np.vstack((cornerSetCoordinates,pattern_points[cornerId]))
                objectPoints.append(cornerSetCoordinates.astype('float64').reshape(1,-1,3))
                                      
            rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
            tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
            
            calib_flags=cv2.fisheye.CALIB_FIX_SKEW + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_USE_INTRINSIC_GUESS # + cv2.fisheye.CALIB_FIX_K4 + cv2.fisheye.CALIB_FIX_K3
            rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(objectPoints, allCorners, gray.shape[::-1], camera_matrix, dist_coeffs,rvecs,tvecs,flags=calib_flags)
            print "RMS: " + str(rms)

            #SHOW WHERE THE PROJECTED CORNERS ARE:
            
            allProjectedPoints=[]
            not_used=0
            for i in xrange(0,len(images)):
                print "Image: " + str(i) 
                image=images[i].copy()
                display_image=image.copy()
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                all_ok=False
                if i in indeces_images_used:
                    projected_points,_=cv2.fisheye.projectPoints(pattern_points.reshape(-1,1,3).astype('float32') ,rvecs[i-not_used],tvecs[i-not_used],mtx,dist)
                    allProjectedPoints.append(projected_points)
                    for projected_point in projected_points:
                        window_size=100
                        minx=int(max(0,projected_point[0][0]-window_size/2))
                        miny=int(max(0,projected_point[0][1]-window_size/2))
                        maxx=int(min(image_w,projected_point[0][0]+window_size/2))
                        maxy=int(min(image_h,projected_point[0][1]+window_size/2))
                        cv2.rectangle(display_image,(minx,miny),(maxx,maxy),(0,255,0),3)
                
                    cv2.namedWindow('Projected corners',cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('Projected corners', 800,800)
                    cv2.imshow('Projected corners',display_image)
                    cv2.waitKey()
                    #ipdb.set_trace()
                    rot_mat,_=cv2.Rodrigues(rvecs[i-not_used])
                    trans_mat=np.hstack((rot_mat,tvecs[i-not_used]))
                    trans_mat=np.vstack((trans_mat,np.array([0,0,0,1])))
                    pattern_point_ext=np.hstack((pattern_points,np.ones((25,1))))
                    np.dot(trans_mat,np.transpose(pattern_point_ext)).shape
                    resu=np.dot(trans_mat,np.transpose(pattern_point_ext))
                    print resu[:,4]
                    all_ok=query_yes_no("Are all corners ok?", "yes")
                else:
                    print "Corners not found automatically"
                    allProjectedPoints.append(np.zeros((pattern_points.shape[0],1,2)))
                    not_used=not_used+1
                if not all_ok:
                    print "Click all corners row by row."
                    display_image=image.copy()
                    manual_corners= np.array([]).reshape(0,2)
                    cv2.namedWindow('Select Points',cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('Select Points', 800,800)
                    cv2.setMouseCallback("Select Points", self.click)
                    for projected_point in projected_points:
                        self.finished=False
                        while not self.finished:
                            cv2.imshow('Select Points',display_image)
                            cv2.waitKey(10 ) 
                        display_image=cv2.rectangle(display_image,totuple(self.refPt-np.array([50,50])),totuple(self.refPt+np.array([50,50])) ,(0,0,255),3)
                        cv2.imshow('Select Points',display_image)
                        cv2.waitKey(300)
                        manual_corners=np.vstack((manual_corners,self.refPt))
                    cv2.destroyWindow('Select Points')
                    #Replace projected points by the manual ones.
                    allProjectedPoints[i]=manual_corners.reshape(-1,1,2)
            

            #REFINE CORNERS BY CLICKING EXACLTY WHERE THEY ARE
            allCorners = []
            objectPoints  = []
            for i in xrange(0,len(images)):
                print "Image: " + str(i) 
                image=images[i].copy()
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                projected_points=allProjectedPoints[i]
                corrected_projected_points=np.array([]).reshape(0,1,2)
                for projected_point in projected_points:
                    window_size=100
                    minx=int(max(0,projected_point[0][0]-window_size/2))
                    miny=int(max(0,projected_point[0][1]-window_size/2))
                    maxx=int(min(image_w,projected_point[0][0]+window_size/2))
                    maxy=int(min(image_h,projected_point[0][1]+window_size/2))
                    window=image[miny:maxy,minx:maxx].copy()
                    cv2.namedWindow('Window',cv2.WINDOW_NORMAL)
                    cv2.resizeWindow('Window', window_size*4,window_size*4)
                    cv2.setMouseCallback("Window", self.click)
                    self.finished=False
                    while not self.finished:
                        cv2.imshow('Window',window)
                        cv2.waitKey(10 )

                    #ipdb.set_trace()
                    #Print 15x15 window
                    #window=cv2.rectangle(window,totuple(np.array([minx,miny])+self.refPt-np.array([7,7])),totuple(np.array([minx,miny])+self.refPt+np.array([7,7])) ,(0,255,0),3)
                    window=cv2.rectangle(window,totuple(self.refPt-np.array([7,7])),totuple(self.refPt+np.array([7,7])) ,(0,0,255),1)
                    cv2.imshow('Window',window)
                    cv2.waitKey(300)
                    corrected_projected_points=np.vstack((corrected_projected_points,(np.array([minx,miny])+self.refPt).reshape(1,1,2)))
                corrected_projected_points=corrected_projected_points.reshape(-1,1,2).astype('float32')
                cv2.cornerSubPix(gray, corrected_projected_points, (15,15), (-1,-1),(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001))

                objectPoints.append(pattern_points.reshape(1,-1 ,3).astype('float32'))
                allCorners.append(corrected_projected_points.reshape(-1,1,2).astype('float32'))

                cv2.drawChessboardCorners(image, pattern_size, corrected_projected_points, True)   
                cv2.imwrite(args[0]+"image" + str(i)  +"_cornersdetected.jpg", image)
                
            #Save all corners for later.
            allCornersArray=np.asarray(allCorners)
            np.save(args[0]+"corners", allCornersArray)
            
        else:
            print "Warning: All images loaded must have assocatied corners."
            #Corners loaded from file
            allCornersArray=np.load(args[1])
            allCorners=[]
            objectPoints=[]
            for i in range(0,allCornersArray.shape[0]):
                allCorners.append(allCornersArray[i])
                objectPoints.append(pattern_points.reshape(1,-1 ,3).astype('float32'))
        
        
        #REAL CALIBRATION
        #ipdb.set_trace()
        #ipdb.set_trace()
        rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        calib_flags=cv2.fisheye.CALIB_FIX_SKEW + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC  + cv2.fisheye.CALIB_FIX_K3  + cv2.fisheye.CALIB_FIX_K4 
        rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(objectPoints, allCorners, (image_w,image_h), camera_matrix, dist_coeffs,rvecs,tvecs,flags=calib_flags)
        print "Images used: " + str(len(objectPoints))
        print "RMS: " + str(rms)
        
        '''
        #CALIBRATE NOW WITHOUT USING POINT BIGGER THAN 180
        for i in xrange(0,len(objectPoints)): 
            projected_points,_=cv2.fisheye.projectPoints(pattern_points.reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
            rot_mat,_=cv2.Rodrigues(rvecs[i])
            trans_mat=np.hstack((rot_mat,tvecs[i]))
            trans_mat=np.vstack((trans_mat,np.array([0,0,0,1])))
            pattern_point_ext=np.hstack((pattern_points,np.ones((25,1))))
            resu=np.dot(trans_mat,np.transpose(pattern_point_ext))
            bigger_180=np.arctan2(np.sqrt(resu[0,:]**2+np.sqrt(resu[1,:]**2)),resu[2,:])>90*np.pi/180.0
            #ipdb.set_trace()
            #Remove if they are bigger 180
            objectPoints[i]=np.delete(objectPoints[i],np.where(bigger_180),1)
            allCorners[i]=np.delete(allCorners[i],np.where(bigger_180),0)

        #ipdb.set_trace()
        #del(allCorners[1])
        #del(objectPoints[1])
        #del(images[1])


        camera_matrix = np.eye(3)
        camera_matrix[0][0]=1000
        camera_matrix[1][1]=1000
        camera_matrix[0][2]=(images[0].shape[::-1][1]-1)/2
        camera_matrix[1][2]=(images[0].shape[::-1][2]-1)/2
        dist_coeffs = np.zeros(4)

        calib_flags=cv2.fisheye.CALIB_FIX_SKEW + cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC + cv2.fisheye.CALIB_USE_INTRINSIC_GUESS + cv2.fisheye.CALIB_FIX_K3  + cv2.fisheye.CALIB_FIX_K4 #

        rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(objectPoints, allCorners, (image_w,image_h), camera_matrix, dist_coeffs,np.asarray(rvecs).reshape(-1,1,1,3),np.asarray(tvecs).reshape(-1,1,1,3),flags=calib_flags)
        print "RMS: " + str(rms)
        print mtx
        print dist_coeffs

        '''
        '''
        allCornersArray=np.load(args[1])
        allCorners=[]
        objectPoints=[]
        for i in range(0,allCornersArray.shape[0]):
            allCorners.append(allCornersArray[i])
            objectPoints.append(pattern_points.reshape(1,-1 ,3).astype('float32'))
        rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
        rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(objectPoints, allCorners, (image_w,image_h), camera_matrix, dist_coeffs,np.asarray(rvecs).reshape(-1,1,1,3),np.asarray(tvecs).reshape(-1,1,1,3),flags=calib_flags)
        print "RMS: " + str(rms)
        '''


        '''
        print "Plot of the model"
        for i in xrange(0,len(objectPoints)): 
            projected_points,_=cv2.fisheye.projectPoints(pattern_points.reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
            rot_mat,_=cv2.Rodrigues(rvecs[i])
            trans_mat=np.hstack((rot_mat,tvecs[i]))
            trans_mat=np.vstack((trans_mat,np.array([0,0,0,1])))
            pattern_point_ext=np.hstack((pattern_points,np.ones((25,1))))
            resu=np.dot(trans_mat,np.transpose(pattern_point_ext))
            angle=np.arctan2(np.sqrt(resu[0,:]**2+np.sqrt(resu[1,:]**2)),resu[2,:])
            r_theta=dist_coeffs[0]*angle+dist_coeffs[1]*angle**3+dist_coeffs[2]*angle**5+dist_coeffs[3]*angle**7
            ipdb.set_trace()
            #Remove if they are bigger 180
            objectPoints[i]=np.delete(objectPoints[i],np.where(bigger_180),1)
            allCorners[i]=np.delete(allCorners[i],np.where(bigger_180),0)
        '''




        print "Showing graphical results"

        #SHOW IMAGES WITH PROJECTED CORNERS
        
        cv2.namedWindow('Projected corners',cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Projected corners', 800,800)

        for i in xrange(0,len(objectPoints)): 
            print "Image: " + str(i) 
            image=images[i].copy()
            display_image=image.copy()
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            projected_points=allCorners[i]    #Detected corners
            for projected_point in projected_points:
                cv2.circle(display_image,(projected_point[0][0],projected_point[0][1]),8,(255,0,0),-1)


            projected_points,_=cv2.fisheye.projectPoints(objectPoints[i].reshape(-1,1,3).reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
            #projected_points=allCorners[i]    #Detected corners
            for projected_point in projected_points:
                cv2.circle(display_image,(projected_point[0][0],projected_point[0][1]),8,(0,0,255),-1)
            cv2.imshow('Projected corners',display_image)
            cv2.waitKey()


        #SHOW UNDISTORTED IMAGES
        newK=cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(mtx,dist,(image_w,image_h),np.eye(3),0.0)
        mapx,mapy=cv2.fisheye.initUndistortRectifyMap(mtx,dist,np.eye(3),newK,(image_w,image_h),cv2.CV_32FC1)
        for image in images:
            dst = cv2.remap(image,mapx,mapy,cv2.INTER_LINEAR)
            cv2.namedWindow("Undist",cv2.WINDOW_NORMAL)
            cv2.imshow("Undist",dst)
            cv2.resizeWindow("Undist", 600, 600)
            cv2.waitKey()
        cv2.destroyAllWindows()

        # R is identity matrix for monocular calibration
        R = np.eye(3, dtype=np.float64)
        P = np.zeros((3, 4), dtype=np.float64)

        #Alpha set as 0 (zoomed in)
        for j in range(3):
            for i in range(3):
                P[j,i] = newK[j, i]

        content=lryaml(image_w, image_h, "kodak_360sp", dist, mtx, R, P)
        with open(args[0]+'camera_calibration.yaml','w') as f:
            f.write(content)


        #CALCULATE INDIVIDUAL ERROR FOR CORNERS
        all_errors=np.array([]).reshape(-1);
        for i in range(len(allCorners)):      
            distorted_pts_image=allCorners[i].reshape(-1,2) #Array original points
            reprojected_points, _=cv2.fisheye.projectPoints(objectPoints[i].reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
            reprojected_points=reprojected_points.reshape(-1,2) #Array reprojected points
            reprojected_error=distorted_pts_image-reprojected_points #Array reprojection errors.
            reprojected_error=np.sum(np.abs(reprojected_error)**2,axis=-1)**(1./2) #Array reprojection error norm
            all_errors=np.hstack((all_errors,reprojected_error.reshape(-1)))
    
    
        #ipdb.set_trace()
        #SHOW CORNERS WITH MAXIUM ERROR
        L=np.argsort(-all_errors, axis=0)
        for k in range(0,3):
            image_max_error=0
            corners_max_error=0
            i=0
            j=0
            while i + objectPoints[j].shape[1] < L[k]:
                i=i+objectPoints[j].shape[1]
                j=j+1
            image_max_error=j
            corner_max_error=L[k]-i    
            
            
            #image_max_error=L[k]/25;
            #corner_max_error=L[k]%25;
            print "Image " + str(image_max_error)
            print "Corner " + str(corner_max_error)
            print "Error " + str(all_errors[L[k]])

            reprojected_points, _=cv2.fisheye.projectPoints(objectPoints[image_max_error].reshape(-1,1,3).astype('float32') ,rvecs[image_max_error],tvecs[image_max_error],mtx,dist)
            
            window_size=100
            minx=int(max(0,allCorners[image_max_error][corner_max_error][0][0]-window_size/2))
            miny=int(max(0,allCorners[image_max_error][corner_max_error][0][1]-window_size/2))
            maxx=int(min(image_w,allCorners[image_max_error][corner_max_error][0][0]+window_size/2))
            maxy=int(min(image_h,allCorners[image_max_error][corner_max_error][0][1]+window_size/2))
            window=images[image_max_error][miny:maxy,minx:maxx].copy()
            window=cv2.circle(window,(window_size/2,window_size/2),3,(0,0,255))  
            difference=(allCorners[image_max_error][corner_max_error][0]-reprojected_points[corner_max_error]).astype('int')
            window=cv2.circle(window,(difference[0][0]+window_size/2,difference[0][1]+window_size/2),3,(255,0,0))
            #window=cv2.rectangle(window,(window_size/2-7,window_size/2-7),(window_size/2+7,window_size/2+7) ,(0,0,255),1)
            cv2.namedWindow('Window',cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Window', window_size*4,window_size*4)
            cv2.setMouseCallback("Window", self.click)
            self.finished=False
            cv2.imshow('Window',window)
            cv2.waitKey()

        '''
        #Try now to reproject points according to calibration obtained and see where they fall
        for j in range(0,3):
            allCorners = []
            objectPoints  = []
            goodCorners=[]
            goodObjectPoints=[]
            for i in xrange(0,len(images_used)):
                image=images_used[i].copy()
                image2=images_used[i].copy()
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                image_points,jacobian=cv2.fisheye.projectPoints(pattern_points.reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
                #ipdb.set_trace()
                image_points=image_points.reshape(-1,1,2).astype('float32')
                image_points_original=image_points.copy()
                cv2.cornerSubPix(gray, image_points, (15,15), (-1,-1),(cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001))
                
                reprojection_error=(image_points_original-image_points).reshape(-1,2)
                reprojection_error=np.sum(np.abs(reprojection_error)**2,axis=-1)**(1./2)
                max_reprojection_error=np.max(reprojection_error)
                     
                include=True
                if max_reprojection_error > 10:
                    print "There is a maximum reprojection error for this image of " + str(max_reprojection_error) + " for the current calibration values."
                    include=query_yes_no("Do you want to include this image?", "no")
                
                if include:
                    goodObjectPoints.append(pattern_points.reshape(1,-1 ,3).astype('float32'))
                    goodCorners.append(image_points.reshape(-1,1,2).astype('float32'))

                objectPoints.append(pattern_points.reshape(1,-1 ,3).astype('float32'))
                allCorners.append(image_points.reshape(-1,1,2).astype('float32'))
           
                if j==2:
                    cv2.drawChessboardCorners(image2, pattern_size, image_points_original, True)  
                    cv2.imwrite(args[0]+"image" + str(i)  +"_cornersreporjected.jpg", image2 )
                    cv2.drawChessboardCorners(image, pattern_size, image_points, True)   
                    cv2.imwrite(args[0]+"image" + str(i)  +"_cornersdetected.jpg", image)
                
                #cv2.aruco.drawDetectedCornersCharuco(image,image_points, charucoIds)

                #cv2.namedWindow('frame',cv2.WINDOW_NORMAL)
                #cv2.imshow('frame',image)
                #cv2.resizeWindow('frame', 600,600)
                #cv2.waitKey()

            rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(goodObjectPoints))]]],dtype='float64').reshape(-1,1,1,3)
            tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(goodObjectPoints))]]],dtype='float64').reshape(-1,1,1,3) 
            rms, mtx, dist, rvecs, tvecs = cv2.fisheye.calibrate(goodObjectPoints, goodCorners, gray.shape[::-1], camera_matrix, dist_coeffs,rvecs,tvecs,flags=calib_flags)
            print "Images used: " + str(len(goodObjectPoints))
            print "RMS: " + str(rms)

            rvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3)
            tvecs=np.asarray([[[np.zeros(3).tolist() for i in range(0,len(objectPoints))]]],dtype='float64').reshape(-1,1,1,3) 
            _, _, _, rvecs, tvecs = cv2.fisheye.calibrate(objectPoints, allCorners, gray.shape[::-1], camera_matrix, dist_coeffs,rvecs,tvecs,flags=calib_flags)
        '''

       
        #==========Show how features are distributed ==================
        ax=plt.subplot(111)
        for distorted_pts_image in allCorners:
            for i in range(np.alen(distorted_pts_image)):
                #ipdb.set_trace()
                distorted_pts_image=distorted_pts_image.reshape(-1,2)
                #Could be done better with random selection inside square. numpy digitize could be heplful
                x0,y0 = (distorted_pts_image[i,0],distorted_pts_image[i,1])
                ax.plot(x0, y0, '+', c='b')
        ax.set_xlim([0, image_w])
        ax.set_ylim([0, image_h])
        ax.invert_yaxis()
        ax.set_aspect('equal')
        plt.show()

        #==========Show how features are distributed with colormap of error. ==================
        all_points=np.array([[]]);
        for i in range(len(allCorners)):      
            distorted_pts_image=allCorners[i].reshape(-1,2) #Array original points
            reprojected_points, _=cv2.fisheye.projectPoints(objectPoints[i].reshape(-1,1,3).astype('float32') ,rvecs[i],tvecs[i],mtx,dist)
            reprojected_points=reprojected_points.reshape(-1,2) #Array reprojected points
            reprojected_error=distorted_pts_image-reprojected_points #Array reprojection errors.
            reprojected_error=np.sum(np.abs(reprojected_error)**2,axis=-1)**(1./2) #Array reprojection error norm
            points_and_error=np.hstack([distorted_pts_image,reprojected_error.reshape(-1,1)])
            all_points=np.vstack([all_points.reshape(-1,3),points_and_error])
            
            
        ax=plt.subplot(111)
        ax.set_xlim(0, image_w)
        ax.set_ylim(0, image_h)
        ax.invert_yaxis()
        ax.set_aspect('equal')
        sc= ax.scatter(all_points[:,0], all_points[:,1], c=all_points[:,2], vmin=0, vmax=max(all_points[:,2]), cmap='jet',marker='+', s=25, lw = 0.5)
        #np.max(all_points[:,2])
        plt.colorbar(sc)
        plt.show(sc)
        

if __name__ == '__main__':

    from optparse import OptionParser
    parser = OptionParser("%prog directory")
    options, args = parser.parse_args()

    obj=calibration()
    obj.calibrate()
