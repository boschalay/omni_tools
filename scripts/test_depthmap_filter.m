% test of depmat filtering to minimize distortions of objects in multicam systems

clear all;
close all;
clc;

parfor frame = 23700:24299
    if mod(frame,3)==0

    %path='/media/jep/DataSSD/Boreas16-6-16/frame23700_24300_selected_graphcut/depthmap_equi2/camera5_frame23820.jpg_equi_smoothed.exr.txt'
    path=['/media/jep/DataSSD/Boreas16-6-16/frame23700_24300_selected_graphcut/depthmaps/equi_depth_map_frame',num2str(frame),'_sparse_smoothed.exr.txt']';

    depthmap_ori = dlmread(path);
    kernelSide = 11;    % should be an odd number
    decayPower = 0.000000000025;     % use 1 to go with the distance and 2 to go with the square of the distance
    distVec1D = [-floor(kernelSide/2):floor(kernelSide/2)];
    distVec1DRep = repmat(distVec1D,[length(distVec1D) 1]);
    distMap = sqrt(distVec1DRep.^2 + (distVec1DRep').^2);
    %weightMap = (1./(distMap+1)).^decayPower
    weightMap = 1;
    %weightMap = normal(weightMap);

    depthmap_ori(depthmap_ori == 0) = inf;

    depthmap_ori_index = uint8(depthmap_ori/10 * 255);
    %imshow(depthmap_ori_index,(jet(255)));

    % depthmapOriPadded = ones(size(depthmap_ori)+floor(kernelSide/2))*inf;
    % depthmapOriPadded(floor(kernelSide/2):)

    depthmapOriPadded = depthmap_ori;
    depthmapOriPadded = [ones(size(depthmap_ori,1),floor(kernelSide/2))*inf depthmapOriPadded ones(size(depthmap_ori,1),floor(kernelSide/2))*inf];
    depthmapOriPadded = [ones(floor(kernelSide/2),size(depthmapOriPadded,2))*inf; depthmapOriPadded; ones(floor(kernelSide/2),size(depthmapOriPadded,2))*inf];

    depthmap_filtered = zeros(size(depthmap_ori));
    for uIdx = 1:size(depthmap_ori,2)
        for vIdx = 1:size(depthmap_ori,1)
            % get the patch from the padded depthmap
            patch = depthmapOriPadded(vIdx:vIdx+kernelSide-1,uIdx:uIdx+kernelSide-1);
            filterValue = min(min(patch./weightMap));
            depthmap_filtered(vIdx,uIdx) = filterValue;
        end;
        uIdx;
    end;

    depthmap_filtered(depthmap_ori == inf) = 0;

    depthmap_filtered = imgaussfilt(depthmap_filtered,5);


    %figure;
    depthmap_filtered_index = uint8(depthmap_filtered/10 * 255);
    %imshow(depthmap_filtered_index,(jet(255)));


    depthmap_diff = depthmap_filtered-depthmap_ori;
    depthmap_diff(~isfinite(depthmap_diff)) = 0;
    figure;
    indexImg = uint8(normal(depthmap_diff) * 255);
    %imshow(indexImg,(jet(255)));

    rgbImage = ind2rgb(uint8(depthmap_filtered/max(max(depthmap_filtered))*255), jet(256));
    rgbImage= bsxfun(@times, rgbImage, cast(depthmap_filtered~=0,class(rgbImage)));
    imwrite(rgbImage, ['/media/jep/DataSSD/Boreas16-6-16/frame23700_24300_selected_graphcut/depthmaps/equi_depth_map_frame',num2str(frame),'_sparse_smoothed_filtered.png'],'PNG');

    dlmwrite(['/media/jep/DataSSD/Boreas16-6-16/frame23700_24300_selected_graphcut/depthmaps/equi_depth_map_frame',num2str(frame),'_sparse_smoothed_filtered.txt'],depthmap_filtered,' ');
    end
end